﻿namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    partial class Frm_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.显示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_StatusStripDisplay1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_StatusStripDisplay2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_OpenSendArrayWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_ResetSTM32 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_ReserAPP = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_AboutUs = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.透明度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity10Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity20Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity30Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity40Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity50Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity60Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity70Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity80Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity90Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsm_Opacity100Percent1 = new System.Windows.Forms.ToolStripMenuItem();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel9 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_TransferOrderSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel11 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_TransferSucessedSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel13 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_TransferFailedSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_RecvSucessedSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_Clear = new System.Windows.Forms.ToolStripSplitButton();
            this.ts_ClearRecv = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_ClearTransfer = new System.Windows.Forms.ToolStripMenuItem();
            this.ts_Version = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txb_MainSendData0 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK13 = new System.Windows.Forms.TextBox();
            this.txb_FilterID13 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK12 = new System.Windows.Forms.TextBox();
            this.txb_FilterID12 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK11 = new System.Windows.Forms.TextBox();
            this.txb_FilterID11 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK10 = new System.Windows.Forms.TextBox();
            this.txb_FilterID10 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK9 = new System.Windows.Forms.TextBox();
            this.txb_FilterID9 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK8 = new System.Windows.Forms.TextBox();
            this.txb_FilterID8 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK7 = new System.Windows.Forms.TextBox();
            this.txb_FilterID7 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK6 = new System.Windows.Forms.TextBox();
            this.txb_FilterID6 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK5 = new System.Windows.Forms.TextBox();
            this.txb_FilterID5 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK4 = new System.Windows.Forms.TextBox();
            this.txb_FilterID4 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK3 = new System.Windows.Forms.TextBox();
            this.txb_FilterID3 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK1 = new System.Windows.Forms.TextBox();
            this.txb_FilterID1 = new System.Windows.Forms.TextBox();
            this.txb_FilterMASK0 = new System.Windows.Forms.TextBox();
            this.txb_FilterID0 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime4 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID4 = new System.Windows.Forms.TextBox();
            this.txb_MainSendData4 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime5 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID5 = new System.Windows.Forms.TextBox();
            this.txb_MainSendData5 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime3 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID3 = new System.Windows.Forms.TextBox();
            this.txb_MainSendData3 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime1 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID1 = new System.Windows.Forms.TextBox();
            this.txb_MainSendData1 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime2 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID2 = new System.Windows.Forms.TextBox();
            this.txb_MainSendData2 = new System.Windows.Forms.TextBox();
            this.txb_MainSendCycleTime0 = new System.Windows.Forms.TextBox();
            this.txb_MainSendID0 = new System.Windows.Forms.TextBox();
            this.btn_SaveExcel = new System.Windows.Forms.Button();
            this.txb_FilterMASK2 = new System.Windows.Forms.TextBox();
            this.txb_FilterID2 = new System.Windows.Forms.TextBox();
            this.btn_Adapter = new System.Windows.Forms.Button();
            this.btn_LeftShrink = new System.Windows.Forms.Button();
            this.btn_UpDownShrink = new System.Windows.Forms.Button();
            this.btn_UpDownExtend = new System.Windows.Forms.Button();
            this.btn_LeftExtend = new System.Windows.Forms.Button();
            this.btnPin = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ts_WorkMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_OffLineFlag = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_Baudrate = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel19 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_AutoRetransferEnable = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel16 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_FilterEnabledSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel18 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_TransferErrorSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_RecvErrorSum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel14 = new System.Windows.Forms.ToolStripStatusLabel();
            this.ts_LastErrorCode = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pl_BasicFunction = new System.Windows.Forms.Panel();
            this.gb_CanFunction = new System.Windows.Forms.GroupBox();
            this.txb_SamleValue = new System.Windows.Forms.TextBox();
            this.label164 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.chb_CustomEnable = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chb_AtuoRetransfer = new System.Windows.Forms.CheckBox();
            this.btn_BasicSet = new System.Windows.Forms.Button();
            this.cmb_Baudrate = new System.Windows.Forms.ComboBox();
            this.cmb_tBS2 = new System.Windows.Forms.ComboBox();
            this.txb_CalculateCANBaudrate = new System.Windows.Forms.TextBox();
            this.txb_BRPValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmb_tBS1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmb_WorkFunSet = new System.Windows.Forms.ComboBox();
            this.gb_SerialPortSet = new System.Windows.Forms.GroupBox();
            this.lablel_SerialPortStatus = new System.Windows.Forms.Label();
            this.btn_SerialPortRefresh = new System.Windows.Forms.Button();
            this.btn_SerialPortOpen = new System.Windows.Forms.Button();
            this.cmb_SerialPortIndex = new System.Windows.Forms.ComboBox();
            this.label162 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pl_FilterSet = new System.Windows.Forms.Panel();
            this.btn_FilterCal = new System.Windows.Forms.Button();
            this.btn_CancelFilterAll = new System.Windows.Forms.Button();
            this.btn_SetFilterAll = new System.Windows.Forms.Button();
            this.btn_CancelFilter13 = new System.Windows.Forms.Button();
            this.btn_SetFilter13 = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.btn_CancelFilter12 = new System.Windows.Forms.Button();
            this.btn_SetFilter12 = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.btn_CancelFilter11 = new System.Windows.Forms.Button();
            this.btn_SetFilter11 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.btn_CancelFilter10 = new System.Windows.Forms.Button();
            this.btn_SetFilter10 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.btn_CancelFilter9 = new System.Windows.Forms.Button();
            this.btn_SetFilter9 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.btn_CancelFilter8 = new System.Windows.Forms.Button();
            this.btn_SetFilter8 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.btn_CancelFilter7 = new System.Windows.Forms.Button();
            this.btn_SetFilter7 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btn_CancelFilter6 = new System.Windows.Forms.Button();
            this.btn_SetFilter6 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.btn_CancelFilter5 = new System.Windows.Forms.Button();
            this.btn_SetFilter5 = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.btn_CancelFilter4 = new System.Windows.Forms.Button();
            this.btn_SetFilter4 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.btn_CancelFilter3 = new System.Windows.Forms.Button();
            this.btn_SetFilter3 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_CancelFilter2 = new System.Windows.Forms.Button();
            this.btn_SetFilter2 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btn_CancelFilter1 = new System.Windows.Forms.Button();
            this.btn_SetFilter1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btn_CancelFilter0 = new System.Windows.Forms.Button();
            this.btn_SetFilter0 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pl_MainSend = new System.Windows.Forms.Panel();
            this.btn_MainCycleSendCancle4 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType4 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType4 = new System.Windows.Forms.ComboBox();
            this.label66 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle4 = new System.Windows.Forms.Button();
            this.btn_MainSend4 = new System.Windows.Forms.Button();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.btn_MainCycleSendCancle5 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType5 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType5 = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle5 = new System.Windows.Forms.Button();
            this.btn_MainSend5 = new System.Windows.Forms.Button();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.btn_MainCycleSendCancle3 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType3 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType3 = new System.Windows.Forms.ComboBox();
            this.label76 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle3 = new System.Windows.Forms.Button();
            this.btn_MainSend3 = new System.Windows.Forms.Button();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.btn_MainCycleSendCancle1 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType1 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType1 = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle1 = new System.Windows.Forms.Button();
            this.btn_MainSend1 = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.btn_MainCycleSendCancle2 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType2 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType2 = new System.Windows.Forms.ComboBox();
            this.label50 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle2 = new System.Windows.Forms.Button();
            this.btn_MainSend2 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.btn_MainCycleSendCancle0 = new System.Windows.Forms.Button();
            this.cmb_MainSendDataType0 = new System.Windows.Forms.ComboBox();
            this.cmb_MainSendIDType0 = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.btn_MainSendCycle0 = new System.Windows.Forms.Button();
            this.btn_MainSend0 = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransferDirection = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FrameID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txb_DataShow = new System.Windows.Forms.TextBox();
            this.chb_NotUpdateRecvToDataGridView = new System.Windows.Forms.CheckBox();
            this.btn_SaveTxt = new System.Windows.Forms.Button();
            this.chb_NotUpdateDataGrid = new System.Windows.Forms.CheckBox();
            this.btn_ClearData = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pl_SendBatch = new System.Windows.Forms.Panel();
            this.chb_All = new System.Windows.Forms.CheckBox();
            this.btn_CycleSendCancle = new System.Windows.Forms.Button();
            this.txb_SendCycleTime = new System.Windows.Forms.TextBox();
            this.lab_cycleTimeInterval = new System.Windows.Forms.Label();
            this.btn_SendCycle = new System.Windows.Forms.Button();
            this.txb_Specification1 = new System.Windows.Forms.TextBox();
            this.txb_Specification2 = new System.Windows.Forms.TextBox();
            this.txb_Specification3 = new System.Windows.Forms.TextBox();
            this.txb_Specification4 = new System.Windows.Forms.TextBox();
            this.txb_Specification5 = new System.Windows.Forms.TextBox();
            this.txb_Specification6 = new System.Windows.Forms.TextBox();
            this.txb_Specification7 = new System.Windows.Forms.TextBox();
            this.txb_Specification8 = new System.Windows.Forms.TextBox();
            this.txb_Specification9 = new System.Windows.Forms.TextBox();
            this.txb_Specification10 = new System.Windows.Forms.TextBox();
            this.txb_Specification11 = new System.Windows.Forms.TextBox();
            this.txb_Specification12 = new System.Windows.Forms.TextBox();
            this.txb_Specification13 = new System.Windows.Forms.TextBox();
            this.txb_Specification14 = new System.Windows.Forms.TextBox();
            this.txb_Specification15 = new System.Windows.Forms.TextBox();
            this.txb_Specification16 = new System.Windows.Forms.TextBox();
            this.txb_Specification17 = new System.Windows.Forms.TextBox();
            this.txb_Specification18 = new System.Windows.Forms.TextBox();
            this.txb_Specification19 = new System.Windows.Forms.TextBox();
            this.txb_Specification20 = new System.Windows.Forms.TextBox();
            this.txb_SendID20 = new System.Windows.Forms.TextBox();
            this.txb_SendData20 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType20 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType20 = new System.Windows.Forms.ComboBox();
            this.btn_Send20 = new System.Windows.Forms.Button();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.chb_Send20 = new System.Windows.Forms.CheckBox();
            this.txb_SendID19 = new System.Windows.Forms.TextBox();
            this.txb_SendData19 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType19 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType19 = new System.Windows.Forms.ComboBox();
            this.btn_Send19 = new System.Windows.Forms.Button();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.chb_Send19 = new System.Windows.Forms.CheckBox();
            this.txb_SendID18 = new System.Windows.Forms.TextBox();
            this.txb_SendData18 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType18 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType18 = new System.Windows.Forms.ComboBox();
            this.btn_Send18 = new System.Windows.Forms.Button();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.chb_Send18 = new System.Windows.Forms.CheckBox();
            this.txb_SendID17 = new System.Windows.Forms.TextBox();
            this.txb_SendData17 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType17 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType17 = new System.Windows.Forms.ComboBox();
            this.btn_Send17 = new System.Windows.Forms.Button();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.chb_Send17 = new System.Windows.Forms.CheckBox();
            this.txb_SendID16 = new System.Windows.Forms.TextBox();
            this.txb_SendData16 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType16 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType16 = new System.Windows.Forms.ComboBox();
            this.btn_Send16 = new System.Windows.Forms.Button();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.chb_Send16 = new System.Windows.Forms.CheckBox();
            this.txb_SendID15 = new System.Windows.Forms.TextBox();
            this.txb_SendData15 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType15 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType15 = new System.Windows.Forms.ComboBox();
            this.btn_Send15 = new System.Windows.Forms.Button();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.chb_Send15 = new System.Windows.Forms.CheckBox();
            this.txb_SendID14 = new System.Windows.Forms.TextBox();
            this.txb_SendData14 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType14 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType14 = new System.Windows.Forms.ComboBox();
            this.btn_Send14 = new System.Windows.Forms.Button();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.chb_Send14 = new System.Windows.Forms.CheckBox();
            this.txb_SendID13 = new System.Windows.Forms.TextBox();
            this.txb_SendData13 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType13 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType13 = new System.Windows.Forms.ComboBox();
            this.btn_Send13 = new System.Windows.Forms.Button();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.chb_Send13 = new System.Windows.Forms.CheckBox();
            this.txb_SendID12 = new System.Windows.Forms.TextBox();
            this.txb_SendData12 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType12 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType12 = new System.Windows.Forms.ComboBox();
            this.btn_Send12 = new System.Windows.Forms.Button();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.chb_Send12 = new System.Windows.Forms.CheckBox();
            this.txb_SendID11 = new System.Windows.Forms.TextBox();
            this.txb_SendData11 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType11 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType11 = new System.Windows.Forms.ComboBox();
            this.btn_Send11 = new System.Windows.Forms.Button();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.chb_Send11 = new System.Windows.Forms.CheckBox();
            this.txb_SendID10 = new System.Windows.Forms.TextBox();
            this.txb_SendData10 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType10 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType10 = new System.Windows.Forms.ComboBox();
            this.btn_Send10 = new System.Windows.Forms.Button();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.chb_Send10 = new System.Windows.Forms.CheckBox();
            this.txb_SendID9 = new System.Windows.Forms.TextBox();
            this.txb_SendData9 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType9 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType9 = new System.Windows.Forms.ComboBox();
            this.btn_Send9 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.chb_Send9 = new System.Windows.Forms.CheckBox();
            this.txb_SendID8 = new System.Windows.Forms.TextBox();
            this.txb_SendData8 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType8 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType8 = new System.Windows.Forms.ComboBox();
            this.btn_Send8 = new System.Windows.Forms.Button();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.chb_Send8 = new System.Windows.Forms.CheckBox();
            this.txb_SendID7 = new System.Windows.Forms.TextBox();
            this.txb_SendData7 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType7 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType7 = new System.Windows.Forms.ComboBox();
            this.btn_Send7 = new System.Windows.Forms.Button();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.chb_Send7 = new System.Windows.Forms.CheckBox();
            this.txb_SendID6 = new System.Windows.Forms.TextBox();
            this.txb_SendData6 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType6 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType6 = new System.Windows.Forms.ComboBox();
            this.btn_Send6 = new System.Windows.Forms.Button();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.chb_Send6 = new System.Windows.Forms.CheckBox();
            this.txb_SendID5 = new System.Windows.Forms.TextBox();
            this.txb_SendData5 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType5 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType5 = new System.Windows.Forms.ComboBox();
            this.btn_Send5 = new System.Windows.Forms.Button();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.chb_Send5 = new System.Windows.Forms.CheckBox();
            this.txb_SendID4 = new System.Windows.Forms.TextBox();
            this.txb_SendData4 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType4 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType4 = new System.Windows.Forms.ComboBox();
            this.btn_Send4 = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.chb_Send4 = new System.Windows.Forms.CheckBox();
            this.txb_SendID3 = new System.Windows.Forms.TextBox();
            this.txb_SendData3 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType3 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType3 = new System.Windows.Forms.ComboBox();
            this.btn_Send3 = new System.Windows.Forms.Button();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.chb_Send3 = new System.Windows.Forms.CheckBox();
            this.txb_SendID2 = new System.Windows.Forms.TextBox();
            this.txb_SendData2 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType2 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType2 = new System.Windows.Forms.ComboBox();
            this.btn_Send2 = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.chb_Send2 = new System.Windows.Forms.CheckBox();
            this.txb_SendID1 = new System.Windows.Forms.TextBox();
            this.txb_SendData1 = new System.Windows.Forms.TextBox();
            this.chb_SendDataType1 = new System.Windows.Forms.ComboBox();
            this.cmb_SendIDType1 = new System.Windows.Forms.ComboBox();
            this.btn_Send1 = new System.Windows.Forms.Button();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.chb_Send1 = new System.Windows.Forms.CheckBox();
            this.tmr_MainSend0 = new System.Windows.Forms.Timer(this.components);
            this.tmr_MainSend1 = new System.Windows.Forms.Timer(this.components);
            this.tmr_MainSend2 = new System.Windows.Forms.Timer(this.components);
            this.tmr_MainSend3 = new System.Windows.Forms.Timer(this.components);
            this.tmr_MainSend4 = new System.Windows.Forms.Timer(this.components);
            this.tmr_MainSend5 = new System.Windows.Forms.Timer(this.components);
            this.tmr_Send = new System.Windows.Forms.Timer(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tmr_StrartRefreshCom = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pl_BasicFunction.SuspendLayout();
            this.gb_CanFunction.SuspendLayout();
            this.gb_SerialPortSet.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.pl_FilterSet.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.pl_MainSend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.pl_SendBatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示ToolStripMenuItem,
            this.tsm_OpenSendArrayWindow,
            this.tsm_ResetSTM32,
            this.tsm_ReserAPP,
            this.tsm_Help,
            this.tsm_AboutUs});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1370, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 显示ToolStripMenuItem
            // 
            this.显示ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_StatusStripDisplay1,
            this.tsm_StatusStripDisplay2});
            this.显示ToolStripMenuItem.Name = "显示ToolStripMenuItem";
            this.显示ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.显示ToolStripMenuItem.Text = "显示";
            // 
            // tsm_StatusStripDisplay1
            // 
            this.tsm_StatusStripDisplay1.Checked = true;
            this.tsm_StatusStripDisplay1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsm_StatusStripDisplay1.Name = "tsm_StatusStripDisplay1";
            this.tsm_StatusStripDisplay1.Size = new System.Drawing.Size(119, 22);
            this.tsm_StatusStripDisplay1.Text = "状态栏1";
            this.tsm_StatusStripDisplay1.Click += new System.EventHandler(this.tsm_StatusStripDisplay1_Click);
            // 
            // tsm_StatusStripDisplay2
            // 
            this.tsm_StatusStripDisplay2.Checked = true;
            this.tsm_StatusStripDisplay2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsm_StatusStripDisplay2.Name = "tsm_StatusStripDisplay2";
            this.tsm_StatusStripDisplay2.Size = new System.Drawing.Size(119, 22);
            this.tsm_StatusStripDisplay2.Text = "状态栏2";
            this.tsm_StatusStripDisplay2.Click += new System.EventHandler(this.tsm_StatusStripDisplay2_Click);
            // 
            // tsm_OpenSendArrayWindow
            // 
            this.tsm_OpenSendArrayWindow.Name = "tsm_OpenSendArrayWindow";
            this.tsm_OpenSendArrayWindow.Size = new System.Drawing.Size(116, 21);
            this.tsm_OpenSendArrayWindow.Text = "打开批量发送窗口";
            this.tsm_OpenSendArrayWindow.Click += new System.EventHandler(this.tsm_OpenSendArrayWindow_Click);
            // 
            // tsm_ResetSTM32
            // 
            this.tsm_ResetSTM32.Name = "tsm_ResetSTM32";
            this.tsm_ResetSTM32.Size = new System.Drawing.Size(104, 21);
            this.tsm_ResetSTM32.Text = "软件复位下位机";
            this.tsm_ResetSTM32.Click += new System.EventHandler(this.tsm_ResetSTM32_Click);
            // 
            // tsm_ReserAPP
            // 
            this.tsm_ReserAPP.Name = "tsm_ReserAPP";
            this.tsm_ReserAPP.Size = new System.Drawing.Size(172, 21);
            this.tsm_ReserAPP.Text = "恢复软件最初配置(重启生效)";
            this.tsm_ReserAPP.Click += new System.EventHandler(this.tsm_ReserAPP_Click);
            // 
            // tsm_Help
            // 
            this.tsm_Help.Name = "tsm_Help";
            this.tsm_Help.Size = new System.Drawing.Size(44, 21);
            this.tsm_Help.Text = "帮助";
            this.tsm_Help.Click += new System.EventHandler(this.tsm_Help_Click);
            // 
            // tsm_AboutUs
            // 
            this.tsm_AboutUs.Name = "tsm_AboutUs";
            this.tsm_AboutUs.Size = new System.Drawing.Size(68, 21);
            this.tsm_AboutUs.Text = "关于我们";
            this.tsm_AboutUs.Click += new System.EventHandler(this.tsm_AboutUs_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.透明度ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 26);
            // 
            // 透明度ToolStripMenuItem
            // 
            this.透明度ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsm_Opacity10Percent1,
            this.tsm_Opacity20Percent1,
            this.tsm_Opacity30Percent1,
            this.tsm_Opacity40Percent1,
            this.tsm_Opacity50Percent1,
            this.tsm_Opacity60Percent1,
            this.tsm_Opacity70Percent1,
            this.tsm_Opacity80Percent1,
            this.tsm_Opacity90Percent1,
            this.tsm_Opacity100Percent1});
            this.透明度ToolStripMenuItem.Name = "透明度ToolStripMenuItem";
            this.透明度ToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.透明度ToolStripMenuItem.Text = "透明度";
            // 
            // tsm_Opacity10Percent1
            // 
            this.tsm_Opacity10Percent1.Name = "tsm_Opacity10Percent1";
            this.tsm_Opacity10Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity10Percent1.Text = "10%";
            this.tsm_Opacity10Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity20Percent1
            // 
            this.tsm_Opacity20Percent1.Name = "tsm_Opacity20Percent1";
            this.tsm_Opacity20Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity20Percent1.Text = "20%";
            this.tsm_Opacity20Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity30Percent1
            // 
            this.tsm_Opacity30Percent1.Name = "tsm_Opacity30Percent1";
            this.tsm_Opacity30Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity30Percent1.Text = "30%";
            this.tsm_Opacity30Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity40Percent1
            // 
            this.tsm_Opacity40Percent1.Name = "tsm_Opacity40Percent1";
            this.tsm_Opacity40Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity40Percent1.Text = "40%";
            this.tsm_Opacity40Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity50Percent1
            // 
            this.tsm_Opacity50Percent1.Name = "tsm_Opacity50Percent1";
            this.tsm_Opacity50Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity50Percent1.Text = "50%";
            this.tsm_Opacity50Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity60Percent1
            // 
            this.tsm_Opacity60Percent1.Name = "tsm_Opacity60Percent1";
            this.tsm_Opacity60Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity60Percent1.Text = "60%";
            this.tsm_Opacity60Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity70Percent1
            // 
            this.tsm_Opacity70Percent1.Name = "tsm_Opacity70Percent1";
            this.tsm_Opacity70Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity70Percent1.Text = "70%";
            this.tsm_Opacity70Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity80Percent1
            // 
            this.tsm_Opacity80Percent1.Name = "tsm_Opacity80Percent1";
            this.tsm_Opacity80Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity80Percent1.Text = "80%";
            this.tsm_Opacity80Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity90Percent1
            // 
            this.tsm_Opacity90Percent1.Name = "tsm_Opacity90Percent1";
            this.tsm_Opacity90Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity90Percent1.Text = "90%";
            this.tsm_Opacity90Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // tsm_Opacity100Percent1
            // 
            this.tsm_Opacity100Percent1.Checked = true;
            this.tsm_Opacity100Percent1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsm_Opacity100Percent1.Name = "tsm_Opacity100Percent1";
            this.tsm_Opacity100Percent1.Size = new System.Drawing.Size(108, 22);
            this.tsm_Opacity100Percent1.Text = "100%";
            this.tsm_Opacity100Percent1.Click += new System.EventHandler(this.MainFromMenuClick);
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 460800;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel9,
            this.ts_TransferOrderSum,
            this.toolStripStatusLabel11,
            this.ts_TransferSucessedSum,
            this.toolStripStatusLabel13,
            this.ts_TransferFailedSum,
            this.toolStripStatusLabel5,
            this.ts_RecvSucessedSum,
            this.ts_Clear,
            this.ts_Version});
            this.statusStrip2.Location = new System.Drawing.Point(0, 659);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(1370, 23);
            this.statusStrip2.TabIndex = 2;
            this.statusStrip2.Text = "statusStrip1";
            // 
            // toolStripStatusLabel9
            // 
            this.toolStripStatusLabel9.AutoSize = false;
            this.toolStripStatusLabel9.Name = "toolStripStatusLabel9";
            this.toolStripStatusLabel9.Size = new System.Drawing.Size(59, 18);
            this.toolStripStatusLabel9.Text = "发送命令:";
            // 
            // ts_TransferOrderSum
            // 
            this.ts_TransferOrderSum.AutoSize = false;
            this.ts_TransferOrderSum.Name = "ts_TransferOrderSum";
            this.ts_TransferOrderSum.Size = new System.Drawing.Size(78, 18);
            this.ts_TransferOrderSum.Text = "----------";
            // 
            // toolStripStatusLabel11
            // 
            this.toolStripStatusLabel11.AutoSize = false;
            this.toolStripStatusLabel11.Name = "toolStripStatusLabel11";
            this.toolStripStatusLabel11.Size = new System.Drawing.Size(59, 18);
            this.toolStripStatusLabel11.Text = "发送成功:";
            // 
            // ts_TransferSucessedSum
            // 
            this.ts_TransferSucessedSum.AutoSize = false;
            this.ts_TransferSucessedSum.Name = "ts_TransferSucessedSum";
            this.ts_TransferSucessedSum.Size = new System.Drawing.Size(85, 18);
            this.ts_TransferSucessedSum.Text = "----------";
            // 
            // toolStripStatusLabel13
            // 
            this.toolStripStatusLabel13.AutoSize = false;
            this.toolStripStatusLabel13.Name = "toolStripStatusLabel13";
            this.toolStripStatusLabel13.Size = new System.Drawing.Size(59, 18);
            this.toolStripStatusLabel13.Text = "发送失败:";
            // 
            // ts_TransferFailedSum
            // 
            this.ts_TransferFailedSum.AutoSize = false;
            this.ts_TransferFailedSum.Name = "ts_TransferFailedSum";
            this.ts_TransferFailedSum.Size = new System.Drawing.Size(78, 18);
            this.ts_TransferFailedSum.Text = "----------";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.AutoSize = false;
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(59, 18);
            this.toolStripStatusLabel5.Text = "接收成功:";
            // 
            // ts_RecvSucessedSum
            // 
            this.ts_RecvSucessedSum.AutoSize = false;
            this.ts_RecvSucessedSum.Name = "ts_RecvSucessedSum";
            this.ts_RecvSucessedSum.Size = new System.Drawing.Size(78, 18);
            this.ts_RecvSucessedSum.Text = "----------";
            // 
            // ts_Clear
            // 
            this.ts_Clear.AutoSize = false;
            this.ts_Clear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ts_Clear.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ts_ClearRecv,
            this.ts_ClearTransfer});
            this.ts_Clear.Image = ((System.Drawing.Image)(resources.GetObject("ts_Clear.Image")));
            this.ts_Clear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ts_Clear.Name = "ts_Clear";
            this.ts_Clear.Size = new System.Drawing.Size(90, 21);
            this.ts_Clear.Text = "清零计数值";
            this.ts_Clear.ButtonClick += new System.EventHandler(this.ts_Clear_ButtonClick);
            // 
            // ts_ClearRecv
            // 
            this.ts_ClearRecv.Name = "ts_ClearRecv";
            this.ts_ClearRecv.Size = new System.Drawing.Size(124, 22);
            this.ts_ClearRecv.Text = "清零接收";
            this.ts_ClearRecv.Click += new System.EventHandler(this.ts_ClearRecv_Click);
            // 
            // ts_ClearTransfer
            // 
            this.ts_ClearTransfer.Name = "ts_ClearTransfer";
            this.ts_ClearTransfer.Size = new System.Drawing.Size(124, 22);
            this.ts_ClearTransfer.Text = "清零发送";
            this.ts_ClearTransfer.Click += new System.EventHandler(this.ts_ClearTransfer_Click);
            // 
            // ts_Version
            // 
            this.ts_Version.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.ts_Version.Name = "ts_Version";
            this.ts_Version.Size = new System.Drawing.Size(121, 18);
            this.ts_Version.Text = "          版本号：V6.0";
            // 
            // txb_MainSendData0
            // 
            this.txb_MainSendData0.Location = new System.Drawing.Point(342, 1);
            this.txb_MainSendData0.MaxLength = 23;
            this.txb_MainSendData0.Multiline = true;
            this.txb_MainSendData0.Name = "txb_MainSendData0";
            this.txb_MainSendData0.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData0.TabIndex = 87;
            this.txb_MainSendData0.Tag = "0";
            this.txb_MainSendData0.Text = "01 23 45 67 89 AB CD EF";
            this.toolTip1.SetToolTip(this.txb_MainSendData0, "16进制 Data0 Data1 ......");
            this.txb_MainSendData0.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_FilterMASK13
            // 
            this.txb_FilterMASK13.Location = new System.Drawing.Point(533, 138);
            this.txb_FilterMASK13.MaxLength = 8;
            this.txb_FilterMASK13.Multiline = true;
            this.txb_FilterMASK13.Name = "txb_FilterMASK13";
            this.txb_FilterMASK13.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK13.TabIndex = 192;
            this.txb_FilterMASK13.Tag = "13";
            this.txb_FilterMASK13.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK13, "16进制的MASK号");
            this.txb_FilterMASK13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID13
            // 
            this.txb_FilterID13.Location = new System.Drawing.Point(433, 138);
            this.txb_FilterID13.MaxLength = 8;
            this.txb_FilterID13.Multiline = true;
            this.txb_FilterID13.Name = "txb_FilterID13";
            this.txb_FilterID13.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID13.TabIndex = 189;
            this.txb_FilterID13.Tag = "13";
            this.txb_FilterID13.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID13, "16进制的ID号");
            this.txb_FilterID13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK12
            // 
            this.txb_FilterMASK12.Location = new System.Drawing.Point(187, 138);
            this.txb_FilterMASK12.MaxLength = 8;
            this.txb_FilterMASK12.Multiline = true;
            this.txb_FilterMASK12.Name = "txb_FilterMASK12";
            this.txb_FilterMASK12.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK12.TabIndex = 185;
            this.txb_FilterMASK12.Tag = "12";
            this.txb_FilterMASK12.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK12, "16进制的MASK号");
            this.txb_FilterMASK12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID12
            // 
            this.txb_FilterID12.Location = new System.Drawing.Point(87, 138);
            this.txb_FilterID12.MaxLength = 8;
            this.txb_FilterID12.Multiline = true;
            this.txb_FilterID12.Name = "txb_FilterID12";
            this.txb_FilterID12.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID12.TabIndex = 182;
            this.txb_FilterID12.Tag = "12";
            this.txb_FilterID12.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID12, "16进制的ID号");
            this.txb_FilterID12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK11
            // 
            this.txb_FilterMASK11.Location = new System.Drawing.Point(533, 115);
            this.txb_FilterMASK11.MaxLength = 8;
            this.txb_FilterMASK11.Multiline = true;
            this.txb_FilterMASK11.Name = "txb_FilterMASK11";
            this.txb_FilterMASK11.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK11.TabIndex = 178;
            this.txb_FilterMASK11.Tag = "11";
            this.txb_FilterMASK11.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK11, "16进制的MASK号");
            this.txb_FilterMASK11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID11
            // 
            this.txb_FilterID11.Location = new System.Drawing.Point(433, 115);
            this.txb_FilterID11.MaxLength = 8;
            this.txb_FilterID11.Multiline = true;
            this.txb_FilterID11.Name = "txb_FilterID11";
            this.txb_FilterID11.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID11.TabIndex = 175;
            this.txb_FilterID11.Tag = "11";
            this.txb_FilterID11.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID11, "16进制的ID号");
            this.txb_FilterID11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK10
            // 
            this.txb_FilterMASK10.Location = new System.Drawing.Point(187, 115);
            this.txb_FilterMASK10.MaxLength = 8;
            this.txb_FilterMASK10.Multiline = true;
            this.txb_FilterMASK10.Name = "txb_FilterMASK10";
            this.txb_FilterMASK10.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK10.TabIndex = 171;
            this.txb_FilterMASK10.Tag = "10";
            this.txb_FilterMASK10.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK10, "16进制的MASK号");
            this.txb_FilterMASK10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID10
            // 
            this.txb_FilterID10.Location = new System.Drawing.Point(87, 115);
            this.txb_FilterID10.MaxLength = 8;
            this.txb_FilterID10.Multiline = true;
            this.txb_FilterID10.Name = "txb_FilterID10";
            this.txb_FilterID10.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID10.TabIndex = 168;
            this.txb_FilterID10.Tag = "10";
            this.txb_FilterID10.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID10, "16进制的ID号");
            this.txb_FilterID10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK9
            // 
            this.txb_FilterMASK9.Location = new System.Drawing.Point(533, 92);
            this.txb_FilterMASK9.MaxLength = 8;
            this.txb_FilterMASK9.Multiline = true;
            this.txb_FilterMASK9.Name = "txb_FilterMASK9";
            this.txb_FilterMASK9.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK9.TabIndex = 164;
            this.txb_FilterMASK9.Tag = "9";
            this.txb_FilterMASK9.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK9, "16进制的MASK号");
            this.txb_FilterMASK9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID9
            // 
            this.txb_FilterID9.Location = new System.Drawing.Point(433, 92);
            this.txb_FilterID9.MaxLength = 8;
            this.txb_FilterID9.Multiline = true;
            this.txb_FilterID9.Name = "txb_FilterID9";
            this.txb_FilterID9.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID9.TabIndex = 161;
            this.txb_FilterID9.Tag = "9";
            this.txb_FilterID9.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID9, "16进制的ID号");
            this.txb_FilterID9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK8
            // 
            this.txb_FilterMASK8.Location = new System.Drawing.Point(187, 92);
            this.txb_FilterMASK8.MaxLength = 8;
            this.txb_FilterMASK8.Multiline = true;
            this.txb_FilterMASK8.Name = "txb_FilterMASK8";
            this.txb_FilterMASK8.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK8.TabIndex = 157;
            this.txb_FilterMASK8.Tag = "8";
            this.txb_FilterMASK8.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK8, "16进制的MASK号");
            this.txb_FilterMASK8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID8
            // 
            this.txb_FilterID8.Location = new System.Drawing.Point(87, 92);
            this.txb_FilterID8.MaxLength = 8;
            this.txb_FilterID8.Multiline = true;
            this.txb_FilterID8.Name = "txb_FilterID8";
            this.txb_FilterID8.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID8.TabIndex = 154;
            this.txb_FilterID8.Tag = "8";
            this.txb_FilterID8.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID8, "16进制的ID号");
            this.txb_FilterID8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK7
            // 
            this.txb_FilterMASK7.Location = new System.Drawing.Point(533, 71);
            this.txb_FilterMASK7.MaxLength = 8;
            this.txb_FilterMASK7.Multiline = true;
            this.txb_FilterMASK7.Name = "txb_FilterMASK7";
            this.txb_FilterMASK7.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK7.TabIndex = 150;
            this.txb_FilterMASK7.Tag = "7";
            this.txb_FilterMASK7.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK7, "16进制的MASK号");
            this.txb_FilterMASK7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID7
            // 
            this.txb_FilterID7.Location = new System.Drawing.Point(433, 71);
            this.txb_FilterID7.MaxLength = 8;
            this.txb_FilterID7.Multiline = true;
            this.txb_FilterID7.Name = "txb_FilterID7";
            this.txb_FilterID7.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID7.TabIndex = 147;
            this.txb_FilterID7.Tag = "7";
            this.txb_FilterID7.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID7, "16进制的ID号");
            this.txb_FilterID7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK6
            // 
            this.txb_FilterMASK6.Location = new System.Drawing.Point(187, 71);
            this.txb_FilterMASK6.MaxLength = 8;
            this.txb_FilterMASK6.Multiline = true;
            this.txb_FilterMASK6.Name = "txb_FilterMASK6";
            this.txb_FilterMASK6.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK6.TabIndex = 143;
            this.txb_FilterMASK6.Tag = "6";
            this.txb_FilterMASK6.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK6, "16进制的MASK号");
            this.txb_FilterMASK6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID6
            // 
            this.txb_FilterID6.Location = new System.Drawing.Point(87, 71);
            this.txb_FilterID6.MaxLength = 8;
            this.txb_FilterID6.Multiline = true;
            this.txb_FilterID6.Name = "txb_FilterID6";
            this.txb_FilterID6.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID6.TabIndex = 140;
            this.txb_FilterID6.Tag = "6";
            this.txb_FilterID6.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID6, "16进制的ID号");
            this.txb_FilterID6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK5
            // 
            this.txb_FilterMASK5.Location = new System.Drawing.Point(533, 48);
            this.txb_FilterMASK5.MaxLength = 8;
            this.txb_FilterMASK5.Multiline = true;
            this.txb_FilterMASK5.Name = "txb_FilterMASK5";
            this.txb_FilterMASK5.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK5.TabIndex = 136;
            this.txb_FilterMASK5.Tag = "5";
            this.txb_FilterMASK5.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK5, "16进制的MASK号");
            this.txb_FilterMASK5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID5
            // 
            this.txb_FilterID5.Location = new System.Drawing.Point(433, 48);
            this.txb_FilterID5.MaxLength = 8;
            this.txb_FilterID5.Multiline = true;
            this.txb_FilterID5.Name = "txb_FilterID5";
            this.txb_FilterID5.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID5.TabIndex = 133;
            this.txb_FilterID5.Tag = "5";
            this.txb_FilterID5.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID5, "16进制的ID号");
            this.txb_FilterID5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK4
            // 
            this.txb_FilterMASK4.Location = new System.Drawing.Point(187, 48);
            this.txb_FilterMASK4.MaxLength = 8;
            this.txb_FilterMASK4.Multiline = true;
            this.txb_FilterMASK4.Name = "txb_FilterMASK4";
            this.txb_FilterMASK4.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK4.TabIndex = 129;
            this.txb_FilterMASK4.Tag = "4";
            this.txb_FilterMASK4.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK4, "16进制的MASK号");
            this.txb_FilterMASK4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID4
            // 
            this.txb_FilterID4.Location = new System.Drawing.Point(87, 48);
            this.txb_FilterID4.MaxLength = 8;
            this.txb_FilterID4.Multiline = true;
            this.txb_FilterID4.Name = "txb_FilterID4";
            this.txb_FilterID4.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID4.TabIndex = 126;
            this.txb_FilterID4.Tag = "4";
            this.txb_FilterID4.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID4, "16进制的ID号");
            this.txb_FilterID4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK3
            // 
            this.txb_FilterMASK3.Location = new System.Drawing.Point(533, 26);
            this.txb_FilterMASK3.MaxLength = 8;
            this.txb_FilterMASK3.Multiline = true;
            this.txb_FilterMASK3.Name = "txb_FilterMASK3";
            this.txb_FilterMASK3.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK3.TabIndex = 122;
            this.txb_FilterMASK3.Tag = "3";
            this.txb_FilterMASK3.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK3, "16进制的MASK号");
            this.txb_FilterMASK3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID3
            // 
            this.txb_FilterID3.Location = new System.Drawing.Point(433, 26);
            this.txb_FilterID3.MaxLength = 8;
            this.txb_FilterID3.Multiline = true;
            this.txb_FilterID3.Name = "txb_FilterID3";
            this.txb_FilterID3.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID3.TabIndex = 119;
            this.txb_FilterID3.Tag = "3";
            this.txb_FilterID3.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID3, "16进制的ID号");
            this.txb_FilterID3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK1
            // 
            this.txb_FilterMASK1.Location = new System.Drawing.Point(533, 3);
            this.txb_FilterMASK1.MaxLength = 8;
            this.txb_FilterMASK1.Multiline = true;
            this.txb_FilterMASK1.Name = "txb_FilterMASK1";
            this.txb_FilterMASK1.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK1.TabIndex = 108;
            this.txb_FilterMASK1.Tag = "1";
            this.txb_FilterMASK1.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK1, "16进制的MASK号");
            this.txb_FilterMASK1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID1
            // 
            this.txb_FilterID1.Location = new System.Drawing.Point(433, 3);
            this.txb_FilterID1.MaxLength = 8;
            this.txb_FilterID1.Multiline = true;
            this.txb_FilterID1.Name = "txb_FilterID1";
            this.txb_FilterID1.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID1.TabIndex = 105;
            this.txb_FilterID1.Tag = "1";
            this.txb_FilterID1.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID1, "16进制的ID号");
            this.txb_FilterID1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterMASK0
            // 
            this.txb_FilterMASK0.Location = new System.Drawing.Point(187, 3);
            this.txb_FilterMASK0.MaxLength = 8;
            this.txb_FilterMASK0.Multiline = true;
            this.txb_FilterMASK0.Name = "txb_FilterMASK0";
            this.txb_FilterMASK0.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK0.TabIndex = 101;
            this.txb_FilterMASK0.Tag = "0";
            this.txb_FilterMASK0.Text = "00000000";
            this.toolTip1.SetToolTip(this.txb_FilterMASK0, "16进制的MASK号");
            this.txb_FilterMASK0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID0
            // 
            this.txb_FilterID0.Location = new System.Drawing.Point(87, 3);
            this.txb_FilterID0.MaxLength = 8;
            this.txb_FilterID0.Multiline = true;
            this.txb_FilterID0.Name = "txb_FilterID0";
            this.txb_FilterID0.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID0.TabIndex = 98;
            this.txb_FilterID0.Tag = "0";
            this.txb_FilterID0.Text = "00000000";
            this.toolTip1.SetToolTip(this.txb_FilterID0, "16进制的ID号");
            this.txb_FilterID0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendCycleTime4
            // 
            this.txb_MainSendCycleTime4.Location = new System.Drawing.Point(587, 112);
            this.txb_MainSendCycleTime4.MaxLength = 8;
            this.txb_MainSendCycleTime4.Multiline = true;
            this.txb_MainSendCycleTime4.Name = "txb_MainSendCycleTime4";
            this.txb_MainSendCycleTime4.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime4.TabIndex = 154;
            this.txb_MainSendCycleTime4.Tag = "4";
            this.txb_MainSendCycleTime4.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime4, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID4
            // 
            this.txb_MainSendID4.Location = new System.Drawing.Point(252, 113);
            this.txb_MainSendID4.MaxLength = 8;
            this.txb_MainSendID4.Multiline = true;
            this.txb_MainSendID4.Name = "txb_MainSendID4";
            this.txb_MainSendID4.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID4.TabIndex = 153;
            this.txb_MainSendID4.Tag = "4";
            this.txb_MainSendID4.Text = "00000005";
            this.toolTip1.SetToolTip(this.txb_MainSendID4, "16进制的ID");
            this.txb_MainSendID4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendData4
            // 
            this.txb_MainSendData4.Location = new System.Drawing.Point(342, 113);
            this.txb_MainSendData4.MaxLength = 23;
            this.txb_MainSendData4.Multiline = true;
            this.txb_MainSendData4.Name = "txb_MainSendData4";
            this.txb_MainSendData4.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData4.TabIndex = 152;
            this.txb_MainSendData4.Tag = "4";
            this.txb_MainSendData4.Text = "FE DC BA 98 76 54 32 10";
            this.toolTip1.SetToolTip(this.txb_MainSendData4, "16进制 Data0 Data1 ......");
            this.txb_MainSendData4.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_MainSendCycleTime5
            // 
            this.txb_MainSendCycleTime5.Location = new System.Drawing.Point(587, 140);
            this.txb_MainSendCycleTime5.MaxLength = 8;
            this.txb_MainSendCycleTime5.Multiline = true;
            this.txb_MainSendCycleTime5.Name = "txb_MainSendCycleTime5";
            this.txb_MainSendCycleTime5.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime5.TabIndex = 141;
            this.txb_MainSendCycleTime5.Tag = "5";
            this.txb_MainSendCycleTime5.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime5, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID5
            // 
            this.txb_MainSendID5.Location = new System.Drawing.Point(252, 141);
            this.txb_MainSendID5.MaxLength = 8;
            this.txb_MainSendID5.Multiline = true;
            this.txb_MainSendID5.Name = "txb_MainSendID5";
            this.txb_MainSendID5.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID5.TabIndex = 140;
            this.txb_MainSendID5.Tag = "5";
            this.txb_MainSendID5.Text = "00000006";
            this.toolTip1.SetToolTip(this.txb_MainSendID5, "16进制的ID");
            this.txb_MainSendID5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendData5
            // 
            this.txb_MainSendData5.Location = new System.Drawing.Point(342, 141);
            this.txb_MainSendData5.MaxLength = 23;
            this.txb_MainSendData5.Multiline = true;
            this.txb_MainSendData5.Name = "txb_MainSendData5";
            this.txb_MainSendData5.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData5.TabIndex = 139;
            this.txb_MainSendData5.Tag = "5";
            this.txb_MainSendData5.Text = "02 46 8A CE 13 57 9B DF";
            this.toolTip1.SetToolTip(this.txb_MainSendData5, "16进制 Data0 Data1 ......");
            this.txb_MainSendData5.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_MainSendCycleTime3
            // 
            this.txb_MainSendCycleTime3.Location = new System.Drawing.Point(587, 84);
            this.txb_MainSendCycleTime3.MaxLength = 8;
            this.txb_MainSendCycleTime3.Multiline = true;
            this.txb_MainSendCycleTime3.Name = "txb_MainSendCycleTime3";
            this.txb_MainSendCycleTime3.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime3.TabIndex = 128;
            this.txb_MainSendCycleTime3.Tag = "3";
            this.txb_MainSendCycleTime3.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime3, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID3
            // 
            this.txb_MainSendID3.Location = new System.Drawing.Point(252, 85);
            this.txb_MainSendID3.MaxLength = 8;
            this.txb_MainSendID3.Multiline = true;
            this.txb_MainSendID3.Name = "txb_MainSendID3";
            this.txb_MainSendID3.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID3.TabIndex = 127;
            this.txb_MainSendID3.Tag = "3";
            this.txb_MainSendID3.Text = "00000004";
            this.toolTip1.SetToolTip(this.txb_MainSendID3, "16进制的ID");
            this.txb_MainSendID3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendData3
            // 
            this.txb_MainSendData3.Location = new System.Drawing.Point(342, 85);
            this.txb_MainSendData3.MaxLength = 23;
            this.txb_MainSendData3.Multiline = true;
            this.txb_MainSendData3.Name = "txb_MainSendData3";
            this.txb_MainSendData3.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData3.TabIndex = 126;
            this.txb_MainSendData3.Tag = "3";
            this.txb_MainSendData3.Text = "00 22 44 66 88 AA CC EE";
            this.toolTip1.SetToolTip(this.txb_MainSendData3, "16进制 Data0 Data1 ......");
            this.txb_MainSendData3.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_MainSendCycleTime1
            // 
            this.txb_MainSendCycleTime1.Location = new System.Drawing.Point(587, 28);
            this.txb_MainSendCycleTime1.MaxLength = 8;
            this.txb_MainSendCycleTime1.Multiline = true;
            this.txb_MainSendCycleTime1.Name = "txb_MainSendCycleTime1";
            this.txb_MainSendCycleTime1.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime1.TabIndex = 115;
            this.txb_MainSendCycleTime1.Tag = "1";
            this.txb_MainSendCycleTime1.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime1, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID1
            // 
            this.txb_MainSendID1.Location = new System.Drawing.Point(252, 29);
            this.txb_MainSendID1.MaxLength = 8;
            this.txb_MainSendID1.Multiline = true;
            this.txb_MainSendID1.Name = "txb_MainSendID1";
            this.txb_MainSendID1.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID1.TabIndex = 114;
            this.txb_MainSendID1.Tag = "1";
            this.txb_MainSendID1.Text = "00000002";
            this.toolTip1.SetToolTip(this.txb_MainSendID1, "16进制的ID");
            this.txb_MainSendID1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendData1
            // 
            this.txb_MainSendData1.Location = new System.Drawing.Point(342, 29);
            this.txb_MainSendData1.MaxLength = 23;
            this.txb_MainSendData1.Multiline = true;
            this.txb_MainSendData1.Name = "txb_MainSendData1";
            this.txb_MainSendData1.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData1.TabIndex = 113;
            this.txb_MainSendData1.Tag = "1";
            this.txb_MainSendData1.Text = "00 11 22 33 44 55 66 77";
            this.toolTip1.SetToolTip(this.txb_MainSendData1, "16进制 Data0 Data1 ......");
            this.txb_MainSendData1.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_MainSendCycleTime2
            // 
            this.txb_MainSendCycleTime2.Location = new System.Drawing.Point(587, 56);
            this.txb_MainSendCycleTime2.MaxLength = 8;
            this.txb_MainSendCycleTime2.Multiline = true;
            this.txb_MainSendCycleTime2.Name = "txb_MainSendCycleTime2";
            this.txb_MainSendCycleTime2.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime2.TabIndex = 102;
            this.txb_MainSendCycleTime2.Tag = "2";
            this.txb_MainSendCycleTime2.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime2, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID2
            // 
            this.txb_MainSendID2.Location = new System.Drawing.Point(252, 57);
            this.txb_MainSendID2.MaxLength = 8;
            this.txb_MainSendID2.Multiline = true;
            this.txb_MainSendID2.Name = "txb_MainSendID2";
            this.txb_MainSendID2.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID2.TabIndex = 101;
            this.txb_MainSendID2.Tag = "2";
            this.txb_MainSendID2.Text = "00000003";
            this.toolTip1.SetToolTip(this.txb_MainSendID2, "16进制的ID");
            this.txb_MainSendID2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_MainSendData2
            // 
            this.txb_MainSendData2.Location = new System.Drawing.Point(342, 57);
            this.txb_MainSendData2.MaxLength = 23;
            this.txb_MainSendData2.Multiline = true;
            this.txb_MainSendData2.Name = "txb_MainSendData2";
            this.txb_MainSendData2.Size = new System.Drawing.Size(144, 18);
            this.txb_MainSendData2.TabIndex = 100;
            this.txb_MainSendData2.Tag = "2";
            this.txb_MainSendData2.Text = "11 33 55 77 99 BB DD FF";
            this.toolTip1.SetToolTip(this.txb_MainSendData2, "16进制 Data0 Data1 ......");
            this.txb_MainSendData2.TextChanged += new System.EventHandler(this.txb_MainSendData0_TextChanged);
            this.txb_MainSendData2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendData0_KeyPress);
            // 
            // txb_MainSendCycleTime0
            // 
            this.txb_MainSendCycleTime0.Location = new System.Drawing.Point(587, 0);
            this.txb_MainSendCycleTime0.MaxLength = 8;
            this.txb_MainSendCycleTime0.Multiline = true;
            this.txb_MainSendCycleTime0.Name = "txb_MainSendCycleTime0";
            this.txb_MainSendCycleTime0.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendCycleTime0.TabIndex = 89;
            this.txb_MainSendCycleTime0.Tag = "0";
            this.txb_MainSendCycleTime0.Text = "1000";
            this.toolTip1.SetToolTip(this.txb_MainSendCycleTime0, "10进制,最小可设置100ms");
            this.txb_MainSendCycleTime0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // txb_MainSendID0
            // 
            this.txb_MainSendID0.Location = new System.Drawing.Point(252, 1);
            this.txb_MainSendID0.MaxLength = 8;
            this.txb_MainSendID0.Multiline = true;
            this.txb_MainSendID0.Name = "txb_MainSendID0";
            this.txb_MainSendID0.Size = new System.Drawing.Size(55, 18);
            this.txb_MainSendID0.TabIndex = 88;
            this.txb_MainSendID0.Tag = "0";
            this.txb_MainSendID0.Text = "00000001";
            this.toolTip1.SetToolTip(this.txb_MainSendID0, "16进制的ID");
            this.txb_MainSendID0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // btn_SaveExcel
            // 
            this.btn_SaveExcel.Location = new System.Drawing.Point(256, 1);
            this.btn_SaveExcel.Name = "btn_SaveExcel";
            this.btn_SaveExcel.Size = new System.Drawing.Size(79, 20);
            this.btn_SaveExcel.TabIndex = 4;
            this.btn_SaveExcel.Text = "保存为Excel";
            this.toolTip1.SetToolTip(this.btn_SaveExcel, "保存为Office2007及以上版本的Excel");
            this.btn_SaveExcel.UseVisualStyleBackColor = true;
            this.btn_SaveExcel.Visible = false;
            this.btn_SaveExcel.Click += new System.EventHandler(this.btn_SaveExcel_Click);
            // 
            // txb_FilterMASK2
            // 
            this.txb_FilterMASK2.Location = new System.Drawing.Point(187, 26);
            this.txb_FilterMASK2.MaxLength = 8;
            this.txb_FilterMASK2.Multiline = true;
            this.txb_FilterMASK2.Name = "txb_FilterMASK2";
            this.txb_FilterMASK2.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterMASK2.TabIndex = 115;
            this.txb_FilterMASK2.Tag = "2";
            this.txb_FilterMASK2.Text = "FFFFFFFF";
            this.toolTip1.SetToolTip(this.txb_FilterMASK2, "16进制的MASK号");
            this.txb_FilterMASK2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // txb_FilterID2
            // 
            this.txb_FilterID2.Location = new System.Drawing.Point(87, 26);
            this.txb_FilterID2.MaxLength = 8;
            this.txb_FilterID2.Multiline = true;
            this.txb_FilterID2.Name = "txb_FilterID2";
            this.txb_FilterID2.Size = new System.Drawing.Size(55, 18);
            this.txb_FilterID2.TabIndex = 112;
            this.txb_FilterID2.Tag = 0D;
            this.txb_FilterID2.Text = "12345678";
            this.toolTip1.SetToolTip(this.txb_FilterID2, "16进制的ID号");
            this.txb_FilterID2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_FilterID0_KeyPress);
            // 
            // btn_Adapter
            // 
            this.btn_Adapter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Adapter.BackColor = System.Drawing.SystemColors.Control;
            this.btn_Adapter.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Adapt;
            this.btn_Adapter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Adapter.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_Adapter.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_Adapter.Location = new System.Drawing.Point(1191, -1);
            this.btn_Adapter.Name = "btn_Adapter";
            this.btn_Adapter.Size = new System.Drawing.Size(24, 26);
            this.btn_Adapter.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btn_Adapter, "适应窗口(Adapt)");
            this.btn_Adapter.UseVisualStyleBackColor = false;
            this.btn_Adapter.Click += new System.EventHandler(this.btn_Adapter_Click);
            // 
            // btn_LeftShrink
            // 
            this.btn_LeftShrink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_LeftShrink.BackColor = System.Drawing.SystemColors.Control;
            this.btn_LeftShrink.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.LeftExtend;
            this.btn_LeftShrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_LeftShrink.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_LeftShrink.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_LeftShrink.Location = new System.Drawing.Point(1284, -1);
            this.btn_LeftShrink.Name = "btn_LeftShrink";
            this.btn_LeftShrink.Size = new System.Drawing.Size(24, 26);
            this.btn_LeftShrink.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btn_LeftShrink, "关闭批量发送");
            this.btn_LeftShrink.UseVisualStyleBackColor = false;
            this.btn_LeftShrink.Click += new System.EventHandler(this.btn_LeftShrink_Click);
            // 
            // btn_UpDownShrink
            // 
            this.btn_UpDownShrink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_UpDownShrink.BackColor = System.Drawing.SystemColors.Control;
            this.btn_UpDownShrink.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Shrink;
            this.btn_UpDownShrink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_UpDownShrink.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_UpDownShrink.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_UpDownShrink.Location = new System.Drawing.Point(1253, -1);
            this.btn_UpDownShrink.Name = "btn_UpDownShrink";
            this.btn_UpDownShrink.Size = new System.Drawing.Size(24, 26);
            this.btn_UpDownShrink.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btn_UpDownShrink, "上下收缩");
            this.btn_UpDownShrink.UseVisualStyleBackColor = false;
            this.btn_UpDownShrink.Click += new System.EventHandler(this.btn_UpDownShrink_Click);
            // 
            // btn_UpDownExtend
            // 
            this.btn_UpDownExtend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_UpDownExtend.BackColor = System.Drawing.SystemColors.Control;
            this.btn_UpDownExtend.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.UpDownExtend;
            this.btn_UpDownExtend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_UpDownExtend.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_UpDownExtend.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_UpDownExtend.Location = new System.Drawing.Point(1222, -1);
            this.btn_UpDownExtend.Name = "btn_UpDownExtend";
            this.btn_UpDownExtend.Size = new System.Drawing.Size(24, 26);
            this.btn_UpDownExtend.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btn_UpDownExtend, "上下扩展");
            this.btn_UpDownExtend.UseVisualStyleBackColor = false;
            this.btn_UpDownExtend.Click += new System.EventHandler(this.btn_UpDownExtend_Click);
            // 
            // btn_LeftExtend
            // 
            this.btn_LeftExtend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_LeftExtend.BackColor = System.Drawing.SystemColors.Control;
            this.btn_LeftExtend.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.RightExtend;
            this.btn_LeftExtend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_LeftExtend.Cursor = System.Windows.Forms.Cursors.Default;
            this.btn_LeftExtend.ForeColor = System.Drawing.SystemColors.Control;
            this.btn_LeftExtend.Location = new System.Drawing.Point(1315, -1);
            this.btn_LeftExtend.Name = "btn_LeftExtend";
            this.btn_LeftExtend.Size = new System.Drawing.Size(24, 26);
            this.btn_LeftExtend.TabIndex = 6;
            this.toolTip1.SetToolTip(this.btn_LeftExtend, "打开批量发送");
            this.btn_LeftExtend.UseVisualStyleBackColor = false;
            this.btn_LeftExtend.Click += new System.EventHandler(this.btn_LeftExtend_Click);
            // 
            // btnPin
            // 
            this.btnPin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPin.BackColor = System.Drawing.SystemColors.Control;
            this.btnPin.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Pin0;
            this.btnPin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPin.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnPin.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPin.Location = new System.Drawing.Point(1346, -1);
            this.btnPin.Name = "btnPin";
            this.btnPin.Size = new System.Drawing.Size(24, 26);
            this.btnPin.TabIndex = 3;
            this.toolTip1.SetToolTip(this.btnPin, "前端显示");
            this.btnPin.UseVisualStyleBackColor = false;
            this.btnPin.Click += new System.EventHandler(this.btnPin_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ts_WorkMode,
            this.ts_OffLineFlag,
            this.ts_Baudrate,
            this.toolStripStatusLabel19,
            this.ts_AutoRetransferEnable,
            this.toolStripStatusLabel16,
            this.ts_FilterEnabledSum,
            this.toolStripStatusLabel18,
            this.ts_TransferErrorSum,
            this.toolStripStatusLabel2,
            this.ts_RecvErrorSum,
            this.toolStripStatusLabel14,
            this.ts_LastErrorCode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 637);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1370, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip2";
            // 
            // ts_WorkMode
            // 
            this.ts_WorkMode.AutoSize = false;
            this.ts_WorkMode.Name = "ts_WorkMode";
            this.ts_WorkMode.Size = new System.Drawing.Size(56, 17);
            this.ts_WorkMode.Text = "--------";
            // 
            // ts_OffLineFlag
            // 
            this.ts_OffLineFlag.AutoSize = false;
            this.ts_OffLineFlag.Name = "ts_OffLineFlag";
            this.ts_OffLineFlag.Size = new System.Drawing.Size(32, 17);
            this.ts_OffLineFlag.Text = "----";
            // 
            // ts_Baudrate
            // 
            this.ts_Baudrate.AutoSize = false;
            this.ts_Baudrate.Name = "ts_Baudrate";
            this.ts_Baudrate.Size = new System.Drawing.Size(79, 17);
            this.ts_Baudrate.Text = "-------bps";
            // 
            // toolStripStatusLabel19
            // 
            this.toolStripStatusLabel19.AutoSize = false;
            this.toolStripStatusLabel19.Name = "toolStripStatusLabel19";
            this.toolStripStatusLabel19.Size = new System.Drawing.Size(59, 17);
            this.toolStripStatusLabel19.Text = "自动重发:";
            // 
            // ts_AutoRetransferEnable
            // 
            this.ts_AutoRetransferEnable.AutoSize = false;
            this.ts_AutoRetransferEnable.Name = "ts_AutoRetransferEnable";
            this.ts_AutoRetransferEnable.Size = new System.Drawing.Size(20, 17);
            this.ts_AutoRetransferEnable.Text = "--";
            // 
            // toolStripStatusLabel16
            // 
            this.toolStripStatusLabel16.AutoSize = false;
            this.toolStripStatusLabel16.Name = "toolStripStatusLabel16";
            this.toolStripStatusLabel16.Size = new System.Drawing.Size(83, 17);
            this.toolStripStatusLabel16.Text = "滤波器使能数:";
            // 
            // ts_FilterEnabledSum
            // 
            this.ts_FilterEnabledSum.AutoSize = false;
            this.ts_FilterEnabledSum.Name = "ts_FilterEnabledSum";
            this.ts_FilterEnabledSum.Size = new System.Drawing.Size(22, 17);
            this.ts_FilterEnabledSum.Text = "--";
            // 
            // toolStripStatusLabel18
            // 
            this.toolStripStatusLabel18.AutoSize = false;
            this.toolStripStatusLabel18.Name = "toolStripStatusLabel18";
            this.toolStripStatusLabel18.Size = new System.Drawing.Size(91, 17);
            this.toolStripStatusLabel18.Text = "发送错误数(内):";
            // 
            // ts_TransferErrorSum
            // 
            this.ts_TransferErrorSum.AutoSize = false;
            this.ts_TransferErrorSum.Name = "ts_TransferErrorSum";
            this.ts_TransferErrorSum.Size = new System.Drawing.Size(29, 17);
            this.ts_TransferErrorSum.Text = "---";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.AutoSize = false;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(91, 17);
            this.toolStripStatusLabel2.Text = "接收错误数(内):";
            // 
            // ts_RecvErrorSum
            // 
            this.ts_RecvErrorSum.AutoSize = false;
            this.ts_RecvErrorSum.Name = "ts_RecvErrorSum";
            this.ts_RecvErrorSum.Size = new System.Drawing.Size(29, 17);
            this.ts_RecvErrorSum.Text = "---";
            // 
            // toolStripStatusLabel14
            // 
            this.toolStripStatusLabel14.AutoSize = false;
            this.toolStripStatusLabel14.Name = "toolStripStatusLabel14";
            this.toolStripStatusLabel14.Size = new System.Drawing.Size(92, 17);
            this.toolStripStatusLabel14.Text = "最近一次错误：";
            // 
            // ts_LastErrorCode
            // 
            this.ts_LastErrorCode.AutoSize = false;
            this.ts_LastErrorCode.Name = "ts_LastErrorCode";
            this.ts_LastErrorCode.Size = new System.Drawing.Size(80, 17);
            this.ts_LastErrorCode.Text = "--------";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Panel2.Controls.Add(this.txb_DataShow);
            this.splitContainer1.Panel2.Controls.Add(this.chb_NotUpdateRecvToDataGridView);
            this.splitContainer1.Panel2.Controls.Add(this.btn_SaveExcel);
            this.splitContainer1.Panel2.Controls.Add(this.btn_SaveTxt);
            this.splitContainer1.Panel2.Controls.Add(this.chb_NotUpdateDataGrid);
            this.splitContainer1.Panel2.Controls.Add(this.btn_ClearData);
            this.splitContainer1.Size = new System.Drawing.Size(784, 612);
            this.splitContainer1.SplitterDistance = 194;
            this.splitContainer1.TabIndex = 5;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(782, 192);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pl_BasicFunction);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(774, 166);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "常规功能设置";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pl_BasicFunction
            // 
            this.pl_BasicFunction.AutoScroll = true;
            this.pl_BasicFunction.Controls.Add(this.gb_CanFunction);
            this.pl_BasicFunction.Controls.Add(this.gb_SerialPortSet);
            this.pl_BasicFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl_BasicFunction.Location = new System.Drawing.Point(3, 3);
            this.pl_BasicFunction.Name = "pl_BasicFunction";
            this.pl_BasicFunction.Size = new System.Drawing.Size(768, 160);
            this.pl_BasicFunction.TabIndex = 22;
            // 
            // gb_CanFunction
            // 
            this.gb_CanFunction.Controls.Add(this.txb_SamleValue);
            this.gb_CanFunction.Controls.Add(this.label164);
            this.gb_CanFunction.Controls.Add(this.label163);
            this.gb_CanFunction.Controls.Add(this.label161);
            this.gb_CanFunction.Controls.Add(this.label8);
            this.gb_CanFunction.Controls.Add(this.chb_CustomEnable);
            this.gb_CanFunction.Controls.Add(this.label7);
            this.gb_CanFunction.Controls.Add(this.chb_AtuoRetransfer);
            this.gb_CanFunction.Controls.Add(this.btn_BasicSet);
            this.gb_CanFunction.Controls.Add(this.cmb_Baudrate);
            this.gb_CanFunction.Controls.Add(this.cmb_tBS2);
            this.gb_CanFunction.Controls.Add(this.txb_CalculateCANBaudrate);
            this.gb_CanFunction.Controls.Add(this.txb_BRPValue);
            this.gb_CanFunction.Controls.Add(this.label6);
            this.gb_CanFunction.Controls.Add(this.label5);
            this.gb_CanFunction.Controls.Add(this.label1);
            this.gb_CanFunction.Controls.Add(this.label4);
            this.gb_CanFunction.Controls.Add(this.cmb_tBS1);
            this.gb_CanFunction.Controls.Add(this.label3);
            this.gb_CanFunction.Controls.Add(this.label2);
            this.gb_CanFunction.Controls.Add(this.cmb_WorkFunSet);
            this.gb_CanFunction.Location = new System.Drawing.Point(6, 54);
            this.gb_CanFunction.Name = "gb_CanFunction";
            this.gb_CanFunction.Size = new System.Drawing.Size(759, 106);
            this.gb_CanFunction.TabIndex = 23;
            this.gb_CanFunction.TabStop = false;
            this.gb_CanFunction.Text = "CAN基本设置";
            // 
            // txb_SamleValue
            // 
            this.txb_SamleValue.Enabled = false;
            this.txb_SamleValue.Location = new System.Drawing.Point(661, 47);
            this.txb_SamleValue.MaxLength = 4;
            this.txb_SamleValue.Multiline = true;
            this.txb_SamleValue.Name = "txb_SamleValue";
            this.txb_SamleValue.ReadOnly = true;
            this.txb_SamleValue.Size = new System.Drawing.Size(41, 18);
            this.txb_SamleValue.TabIndex = 26;
            this.txb_SamleValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(613, 50);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(53, 12);
            this.label164.TabIndex = 25;
            this.label164.Text = "采样点：";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(495, 87);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(245, 12);
            this.label163.TabIndex = 24;
            this.label163.Text = "推荐值：>800K:75% >500K:80% <=500K:87.5%";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(506, 69);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(233, 12);
            this.label161.TabIndex = 23;
            this.label161.Text = "采样点计算方法：(1+tBS1)/(1+tBS1+tBS2)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(258, 69);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "（不建议）";
            // 
            // chb_CustomEnable
            // 
            this.chb_CustomEnable.AutoSize = true;
            this.chb_CustomEnable.Location = new System.Drawing.Point(330, 19);
            this.chb_CustomEnable.Name = "chb_CustomEnable";
            this.chb_CustomEnable.Size = new System.Drawing.Size(96, 16);
            this.chb_CustomEnable.TabIndex = 7;
            this.chb_CustomEnable.Text = "自定义波特率";
            this.chb_CustomEnable.UseVisualStyleBackColor = true;
            this.chb_CustomEnable.CheckedChanged += new System.EventHandler(this.chb_CustomEnable_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(432, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "波特率计算方法：36000000/[(BRP+1)(tBS1+tBS2+3)]";
            // 
            // chb_AtuoRetransfer
            // 
            this.chb_AtuoRetransfer.AutoSize = true;
            this.chb_AtuoRetransfer.Location = new System.Drawing.Point(248, 50);
            this.chb_AtuoRetransfer.Name = "chb_AtuoRetransfer";
            this.chb_AtuoRetransfer.Size = new System.Drawing.Size(72, 16);
            this.chb_AtuoRetransfer.TabIndex = 4;
            this.chb_AtuoRetransfer.Text = "自动重发";
            this.chb_AtuoRetransfer.UseVisualStyleBackColor = true;
            // 
            // btn_BasicSet
            // 
            this.btn_BasicSet.Location = new System.Drawing.Point(8, 41);
            this.btn_BasicSet.Name = "btn_BasicSet";
            this.btn_BasicSet.Size = new System.Drawing.Size(226, 58);
            this.btn_BasicSet.TabIndex = 14;
            this.btn_BasicSet.Text = "设置";
            this.btn_BasicSet.UseVisualStyleBackColor = true;
            this.btn_BasicSet.Click += new System.EventHandler(this.btn_BasicSet_Click);
            // 
            // cmb_Baudrate
            // 
            this.cmb_Baudrate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Baudrate.FormattingEnabled = true;
            this.cmb_Baudrate.Items.AddRange(new object[] {
            "20k",
            "50k",
            "100k",
            "125k",
            "200k",
            "250k",
            "400k",
            "500k",
            "600k",
            "750k",
            "800k",
            "1M"});
            this.cmb_Baudrate.Location = new System.Drawing.Point(248, 17);
            this.cmb_Baudrate.Name = "cmb_Baudrate";
            this.cmb_Baudrate.Size = new System.Drawing.Size(55, 20);
            this.cmb_Baudrate.TabIndex = 6;
            // 
            // cmb_tBS2
            // 
            this.cmb_tBS2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tBS2.Enabled = false;
            this.cmb_tBS2.FormattingEnabled = true;
            this.cmb_tBS2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.cmb_tBS2.Location = new System.Drawing.Point(445, 47);
            this.cmb_tBS2.Name = "cmb_tBS2";
            this.cmb_tBS2.Size = new System.Drawing.Size(37, 20);
            this.cmb_tBS2.TabIndex = 21;
            this.cmb_tBS2.TextChanged += new System.EventHandler(this.CalculatorBuadrate);
            // 
            // txb_CalculateCANBaudrate
            // 
            this.txb_CalculateCANBaudrate.Location = new System.Drawing.Point(395, 73);
            this.txb_CalculateCANBaudrate.Multiline = true;
            this.txb_CalculateCANBaudrate.Name = "txb_CalculateCANBaudrate";
            this.txb_CalculateCANBaudrate.ReadOnly = true;
            this.txb_CalculateCANBaudrate.Size = new System.Drawing.Size(87, 18);
            this.txb_CalculateCANBaudrate.TabIndex = 16;
            this.txb_CalculateCANBaudrate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_BRPValue
            // 
            this.txb_BRPValue.Enabled = false;
            this.txb_BRPValue.Location = new System.Drawing.Point(566, 47);
            this.txb_BRPValue.MaxLength = 4;
            this.txb_BRPValue.Multiline = true;
            this.txb_BRPValue.Name = "txb_BRPValue";
            this.txb_BRPValue.Size = new System.Drawing.Size(41, 18);
            this.txb_BRPValue.TabIndex = 13;
            this.txb_BRPValue.Text = "1023";
            this.txb_BRPValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_BRPValue.TextChanged += new System.EventHandler(this.CalculatorBuadrate);
            this.txb_BRPValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_MainSendCycleTime0_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(328, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 15;
            this.label6.Text = "CAN波特率：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(488, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "BRP(0-1023)：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "模式选择：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(411, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "tBS2：";
            // 
            // cmb_tBS1
            // 
            this.cmb_tBS1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_tBS1.Enabled = false;
            this.cmb_tBS1.FormattingEnabled = true;
            this.cmb_tBS1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15"});
            this.cmb_tBS1.Location = new System.Drawing.Point(364, 47);
            this.cmb_tBS1.Name = "cmb_tBS1";
            this.cmb_tBS1.Size = new System.Drawing.Size(38, 20);
            this.cmb_tBS1.TabIndex = 20;
            this.cmb_tBS1.TextChanged += new System.EventHandler(this.CalculatorBuadrate);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "tBS1：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "波特率：";
            // 
            // cmb_WorkFunSet
            // 
            this.cmb_WorkFunSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_WorkFunSet.FormattingEnabled = true;
            this.cmb_WorkFunSet.Items.AddRange(new object[] {
            "正常模式",
            "环回模式",
            "静默模式",
            "环回静默模式"});
            this.cmb_WorkFunSet.Location = new System.Drawing.Point(90, 17);
            this.cmb_WorkFunSet.Name = "cmb_WorkFunSet";
            this.cmb_WorkFunSet.Size = new System.Drawing.Size(97, 20);
            this.cmb_WorkFunSet.TabIndex = 1;
            // 
            // gb_SerialPortSet
            // 
            this.gb_SerialPortSet.Controls.Add(this.lablel_SerialPortStatus);
            this.gb_SerialPortSet.Controls.Add(this.btn_SerialPortRefresh);
            this.gb_SerialPortSet.Controls.Add(this.btn_SerialPortOpen);
            this.gb_SerialPortSet.Controls.Add(this.cmb_SerialPortIndex);
            this.gb_SerialPortSet.Controls.Add(this.label162);
            this.gb_SerialPortSet.Location = new System.Drawing.Point(6, 4);
            this.gb_SerialPortSet.Name = "gb_SerialPortSet";
            this.gb_SerialPortSet.Size = new System.Drawing.Size(759, 48);
            this.gb_SerialPortSet.TabIndex = 22;
            this.gb_SerialPortSet.TabStop = false;
            this.gb_SerialPortSet.Text = "通信串口设置";
            // 
            // lablel_SerialPortStatus
            // 
            this.lablel_SerialPortStatus.AutoSize = true;
            this.lablel_SerialPortStatus.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lablel_SerialPortStatus.Location = new System.Drawing.Point(460, 21);
            this.lablel_SerialPortStatus.Name = "lablel_SerialPortStatus";
            this.lablel_SerialPortStatus.Size = new System.Drawing.Size(111, 13);
            this.lablel_SerialPortStatus.TabIndex = 4;
            this.lablel_SerialPortStatus.Text = "串口状态：已关闭";
            // 
            // btn_SerialPortRefresh
            // 
            this.btn_SerialPortRefresh.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_SerialPortRefresh.Location = new System.Drawing.Point(379, 16);
            this.btn_SerialPortRefresh.Name = "btn_SerialPortRefresh";
            this.btn_SerialPortRefresh.Size = new System.Drawing.Size(75, 23);
            this.btn_SerialPortRefresh.TabIndex = 3;
            this.btn_SerialPortRefresh.Tag = "Fresh";
            this.btn_SerialPortRefresh.Text = "刷新串口";
            this.btn_SerialPortRefresh.UseVisualStyleBackColor = true;
            this.btn_SerialPortRefresh.Click += new System.EventHandler(this.btn_SerialPortOperate);
            // 
            // btn_SerialPortOpen
            // 
            this.btn_SerialPortOpen.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_SerialPortOpen.Location = new System.Drawing.Point(298, 16);
            this.btn_SerialPortOpen.Name = "btn_SerialPortOpen";
            this.btn_SerialPortOpen.Size = new System.Drawing.Size(75, 23);
            this.btn_SerialPortOpen.TabIndex = 2;
            this.btn_SerialPortOpen.Tag = "Open/Close";
            this.btn_SerialPortOpen.Text = "打开串口";
            this.btn_SerialPortOpen.UseVisualStyleBackColor = true;
            this.btn_SerialPortOpen.Click += new System.EventHandler(this.btn_SerialPortOperate);
            // 
            // cmb_SerialPortIndex
            // 
            this.cmb_SerialPortIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SerialPortIndex.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cmb_SerialPortIndex.FormattingEnabled = true;
            this.cmb_SerialPortIndex.Location = new System.Drawing.Point(84, 17);
            this.cmb_SerialPortIndex.Name = "cmb_SerialPortIndex";
            this.cmb_SerialPortIndex.Size = new System.Drawing.Size(208, 21);
            this.cmb_SerialPortIndex.TabIndex = 1;
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(25, 21);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(53, 12);
            this.label162.TabIndex = 0;
            this.label162.Text = "串口号：";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pl_FilterSet);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(774, 166);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "滤波器设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pl_FilterSet
            // 
            this.pl_FilterSet.AutoScroll = true;
            this.pl_FilterSet.Controls.Add(this.btn_FilterCal);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilterAll);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilterAll);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter13);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter13);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK13);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID13);
            this.pl_FilterSet.Controls.Add(this.label51);
            this.pl_FilterSet.Controls.Add(this.label52);
            this.pl_FilterSet.Controls.Add(this.label53);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter12);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter12);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK12);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID12);
            this.pl_FilterSet.Controls.Add(this.label54);
            this.pl_FilterSet.Controls.Add(this.label55);
            this.pl_FilterSet.Controls.Add(this.label56);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter11);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter11);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK11);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID11);
            this.pl_FilterSet.Controls.Add(this.label33);
            this.pl_FilterSet.Controls.Add(this.label34);
            this.pl_FilterSet.Controls.Add(this.label35);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter10);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter10);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK10);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID10);
            this.pl_FilterSet.Controls.Add(this.label36);
            this.pl_FilterSet.Controls.Add(this.label37);
            this.pl_FilterSet.Controls.Add(this.label38);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter9);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter9);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK9);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID9);
            this.pl_FilterSet.Controls.Add(this.label39);
            this.pl_FilterSet.Controls.Add(this.label40);
            this.pl_FilterSet.Controls.Add(this.label41);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter8);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter8);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK8);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID8);
            this.pl_FilterSet.Controls.Add(this.label42);
            this.pl_FilterSet.Controls.Add(this.label43);
            this.pl_FilterSet.Controls.Add(this.label44);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter7);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter7);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK7);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID7);
            this.pl_FilterSet.Controls.Add(this.label21);
            this.pl_FilterSet.Controls.Add(this.label22);
            this.pl_FilterSet.Controls.Add(this.label23);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter6);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter6);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK6);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID6);
            this.pl_FilterSet.Controls.Add(this.label24);
            this.pl_FilterSet.Controls.Add(this.label25);
            this.pl_FilterSet.Controls.Add(this.label26);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter5);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter5);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK5);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID5);
            this.pl_FilterSet.Controls.Add(this.label27);
            this.pl_FilterSet.Controls.Add(this.label28);
            this.pl_FilterSet.Controls.Add(this.label29);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter4);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter4);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK4);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID4);
            this.pl_FilterSet.Controls.Add(this.label30);
            this.pl_FilterSet.Controls.Add(this.label31);
            this.pl_FilterSet.Controls.Add(this.label32);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter3);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter3);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK3);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID3);
            this.pl_FilterSet.Controls.Add(this.label15);
            this.pl_FilterSet.Controls.Add(this.label16);
            this.pl_FilterSet.Controls.Add(this.label17);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter2);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter2);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK2);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID2);
            this.pl_FilterSet.Controls.Add(this.label18);
            this.pl_FilterSet.Controls.Add(this.label19);
            this.pl_FilterSet.Controls.Add(this.label20);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter1);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter1);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK1);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID1);
            this.pl_FilterSet.Controls.Add(this.label12);
            this.pl_FilterSet.Controls.Add(this.label13);
            this.pl_FilterSet.Controls.Add(this.label14);
            this.pl_FilterSet.Controls.Add(this.btn_CancelFilter0);
            this.pl_FilterSet.Controls.Add(this.btn_SetFilter0);
            this.pl_FilterSet.Controls.Add(this.txb_FilterMASK0);
            this.pl_FilterSet.Controls.Add(this.txb_FilterID0);
            this.pl_FilterSet.Controls.Add(this.label11);
            this.pl_FilterSet.Controls.Add(this.label10);
            this.pl_FilterSet.Controls.Add(this.label9);
            this.pl_FilterSet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl_FilterSet.Location = new System.Drawing.Point(3, 3);
            this.pl_FilterSet.Name = "pl_FilterSet";
            this.pl_FilterSet.Size = new System.Drawing.Size(768, 160);
            this.pl_FilterSet.TabIndex = 0;
            this.pl_FilterSet.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pl_SendBatch_MouseClick);
            // 
            // btn_FilterCal
            // 
            this.btn_FilterCal.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_FilterCal.Location = new System.Drawing.Point(693, 80);
            this.btn_FilterCal.Name = "btn_FilterCal";
            this.btn_FilterCal.Size = new System.Drawing.Size(54, 74);
            this.btn_FilterCal.TabIndex = 197;
            this.btn_FilterCal.Tag = "1";
            this.btn_FilterCal.Text = "滤波器计算器";
            this.btn_FilterCal.UseVisualStyleBackColor = true;
            this.btn_FilterCal.Click += new System.EventHandler(this.btn_FilterCal_Click);
            // 
            // btn_CancelFilterAll
            // 
            this.btn_CancelFilterAll.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_CancelFilterAll.Location = new System.Drawing.Point(723, 0);
            this.btn_CancelFilterAll.Name = "btn_CancelFilterAll";
            this.btn_CancelFilterAll.Size = new System.Drawing.Size(24, 74);
            this.btn_CancelFilterAll.TabIndex = 196;
            this.btn_CancelFilterAll.Tag = "0";
            this.btn_CancelFilterAll.Text = "全取消";
            this.btn_CancelFilterAll.UseVisualStyleBackColor = true;
            this.btn_CancelFilterAll.Click += new System.EventHandler(this.AllFliterOperation);
            // 
            // btn_SetFilterAll
            // 
            this.btn_SetFilterAll.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_SetFilterAll.Location = new System.Drawing.Point(693, 0);
            this.btn_SetFilterAll.Name = "btn_SetFilterAll";
            this.btn_SetFilterAll.Size = new System.Drawing.Size(24, 74);
            this.btn_SetFilterAll.TabIndex = 195;
            this.btn_SetFilterAll.Tag = "1";
            this.btn_SetFilterAll.Text = "全设置";
            this.btn_SetFilterAll.UseVisualStyleBackColor = true;
            this.btn_SetFilterAll.Click += new System.EventHandler(this.AllFliterOperation);
            // 
            // btn_CancelFilter13
            // 
            this.btn_CancelFilter13.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter13.Location = new System.Drawing.Point(645, 137);
            this.btn_CancelFilter13.Name = "btn_CancelFilter13";
            this.btn_CancelFilter13.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter13.TabIndex = 194;
            this.btn_CancelFilter13.Tag = "13";
            this.btn_CancelFilter13.Text = "取消";
            this.btn_CancelFilter13.UseVisualStyleBackColor = false;
            this.btn_CancelFilter13.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter13
            // 
            this.btn_SetFilter13.Location = new System.Drawing.Point(594, 137);
            this.btn_SetFilter13.Name = "btn_SetFilter13";
            this.btn_SetFilter13.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter13.TabIndex = 193;
            this.btn_SetFilter13.Tag = "13";
            this.btn_SetFilter13.Text = "设置";
            this.btn_SetFilter13.UseVisualStyleBackColor = true;
            this.btn_SetFilter13.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter13.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(497, 141);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 12);
            this.label51.TabIndex = 191;
            this.label51.Text = "MASK：";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(405, 141);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(35, 12);
            this.label52.TabIndex = 190;
            this.label52.Text = "AID：";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(350, 141);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(65, 12);
            this.label53.TabIndex = 188;
            this.label53.Text = "滤波器13：";
            // 
            // btn_CancelFilter12
            // 
            this.btn_CancelFilter12.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter12.Location = new System.Drawing.Point(299, 137);
            this.btn_CancelFilter12.Name = "btn_CancelFilter12";
            this.btn_CancelFilter12.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter12.TabIndex = 187;
            this.btn_CancelFilter12.Tag = "12";
            this.btn_CancelFilter12.Text = "取消";
            this.btn_CancelFilter12.UseVisualStyleBackColor = false;
            this.btn_CancelFilter12.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter12
            // 
            this.btn_SetFilter12.Location = new System.Drawing.Point(248, 137);
            this.btn_SetFilter12.Name = "btn_SetFilter12";
            this.btn_SetFilter12.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter12.TabIndex = 186;
            this.btn_SetFilter12.Tag = "12";
            this.btn_SetFilter12.Text = "设置";
            this.btn_SetFilter12.UseVisualStyleBackColor = true;
            this.btn_SetFilter12.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter12.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(151, 141);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(41, 12);
            this.label54.TabIndex = 184;
            this.label54.Text = "MASK：";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(59, 141);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(35, 12);
            this.label55.TabIndex = 183;
            this.label55.Text = "AID：";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(4, 141);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(65, 12);
            this.label56.TabIndex = 181;
            this.label56.Text = "滤波器12：";
            // 
            // btn_CancelFilter11
            // 
            this.btn_CancelFilter11.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter11.Location = new System.Drawing.Point(645, 114);
            this.btn_CancelFilter11.Name = "btn_CancelFilter11";
            this.btn_CancelFilter11.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter11.TabIndex = 180;
            this.btn_CancelFilter11.Tag = "11";
            this.btn_CancelFilter11.Text = "取消";
            this.btn_CancelFilter11.UseVisualStyleBackColor = false;
            this.btn_CancelFilter11.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter11
            // 
            this.btn_SetFilter11.Location = new System.Drawing.Point(594, 114);
            this.btn_SetFilter11.Name = "btn_SetFilter11";
            this.btn_SetFilter11.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter11.TabIndex = 179;
            this.btn_SetFilter11.Tag = "11";
            this.btn_SetFilter11.Text = "设置";
            this.btn_SetFilter11.UseVisualStyleBackColor = true;
            this.btn_SetFilter11.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter11.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(497, 118);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 177;
            this.label33.Text = "MASK：";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(405, 118);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 12);
            this.label34.TabIndex = 176;
            this.label34.Text = "AID：";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(350, 118);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(65, 12);
            this.label35.TabIndex = 174;
            this.label35.Text = "滤波器11：";
            // 
            // btn_CancelFilter10
            // 
            this.btn_CancelFilter10.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter10.Location = new System.Drawing.Point(299, 114);
            this.btn_CancelFilter10.Name = "btn_CancelFilter10";
            this.btn_CancelFilter10.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter10.TabIndex = 173;
            this.btn_CancelFilter10.Tag = "10";
            this.btn_CancelFilter10.Text = "取消";
            this.btn_CancelFilter10.UseVisualStyleBackColor = false;
            this.btn_CancelFilter10.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter10
            // 
            this.btn_SetFilter10.Location = new System.Drawing.Point(248, 114);
            this.btn_SetFilter10.Name = "btn_SetFilter10";
            this.btn_SetFilter10.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter10.TabIndex = 172;
            this.btn_SetFilter10.Tag = "10";
            this.btn_SetFilter10.Text = "设置";
            this.btn_SetFilter10.UseVisualStyleBackColor = true;
            this.btn_SetFilter10.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter10.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(151, 118);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 12);
            this.label36.TabIndex = 170;
            this.label36.Text = "MASK：";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(59, 118);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 12);
            this.label37.TabIndex = 169;
            this.label37.Text = "AID：";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(4, 118);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 12);
            this.label38.TabIndex = 167;
            this.label38.Text = "滤波器10：";
            // 
            // btn_CancelFilter9
            // 
            this.btn_CancelFilter9.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter9.Location = new System.Drawing.Point(645, 91);
            this.btn_CancelFilter9.Name = "btn_CancelFilter9";
            this.btn_CancelFilter9.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter9.TabIndex = 166;
            this.btn_CancelFilter9.Tag = "9";
            this.btn_CancelFilter9.Text = "取消";
            this.btn_CancelFilter9.UseVisualStyleBackColor = false;
            this.btn_CancelFilter9.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter9
            // 
            this.btn_SetFilter9.Location = new System.Drawing.Point(594, 91);
            this.btn_SetFilter9.Name = "btn_SetFilter9";
            this.btn_SetFilter9.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter9.TabIndex = 165;
            this.btn_SetFilter9.Tag = "9";
            this.btn_SetFilter9.Text = "设置";
            this.btn_SetFilter9.UseVisualStyleBackColor = true;
            this.btn_SetFilter9.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter9.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(497, 95);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 12);
            this.label39.TabIndex = 163;
            this.label39.Text = "MASK：";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(405, 95);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 12);
            this.label40.TabIndex = 162;
            this.label40.Text = "AID：";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(350, 95);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(59, 12);
            this.label41.TabIndex = 160;
            this.label41.Text = "滤波器9：";
            // 
            // btn_CancelFilter8
            // 
            this.btn_CancelFilter8.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter8.Location = new System.Drawing.Point(299, 91);
            this.btn_CancelFilter8.Name = "btn_CancelFilter8";
            this.btn_CancelFilter8.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter8.TabIndex = 159;
            this.btn_CancelFilter8.Tag = "8";
            this.btn_CancelFilter8.Text = "取消";
            this.btn_CancelFilter8.UseVisualStyleBackColor = false;
            this.btn_CancelFilter8.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter8
            // 
            this.btn_SetFilter8.Location = new System.Drawing.Point(248, 91);
            this.btn_SetFilter8.Name = "btn_SetFilter8";
            this.btn_SetFilter8.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter8.TabIndex = 158;
            this.btn_SetFilter8.Tag = "8";
            this.btn_SetFilter8.Text = "设置";
            this.btn_SetFilter8.UseVisualStyleBackColor = true;
            this.btn_SetFilter8.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter8.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(151, 95);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 12);
            this.label42.TabIndex = 156;
            this.label42.Text = "MASK：";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(59, 95);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 12);
            this.label43.TabIndex = 155;
            this.label43.Text = "AID：";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(4, 95);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(59, 12);
            this.label44.TabIndex = 153;
            this.label44.Text = "滤波器8：";
            // 
            // btn_CancelFilter7
            // 
            this.btn_CancelFilter7.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter7.Location = new System.Drawing.Point(645, 70);
            this.btn_CancelFilter7.Name = "btn_CancelFilter7";
            this.btn_CancelFilter7.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter7.TabIndex = 152;
            this.btn_CancelFilter7.Tag = "7";
            this.btn_CancelFilter7.Text = "取消";
            this.btn_CancelFilter7.UseVisualStyleBackColor = false;
            this.btn_CancelFilter7.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter7
            // 
            this.btn_SetFilter7.Location = new System.Drawing.Point(594, 70);
            this.btn_SetFilter7.Name = "btn_SetFilter7";
            this.btn_SetFilter7.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter7.TabIndex = 151;
            this.btn_SetFilter7.Tag = "7";
            this.btn_SetFilter7.Text = "设置";
            this.btn_SetFilter7.UseVisualStyleBackColor = true;
            this.btn_SetFilter7.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter7.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(497, 74);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 149;
            this.label21.Text = "MASK：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(405, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 12);
            this.label22.TabIndex = 148;
            this.label22.Text = "AID：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(350, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(59, 12);
            this.label23.TabIndex = 146;
            this.label23.Text = "滤波器7：";
            // 
            // btn_CancelFilter6
            // 
            this.btn_CancelFilter6.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter6.Location = new System.Drawing.Point(299, 70);
            this.btn_CancelFilter6.Name = "btn_CancelFilter6";
            this.btn_CancelFilter6.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter6.TabIndex = 145;
            this.btn_CancelFilter6.Tag = "6";
            this.btn_CancelFilter6.Text = "取消";
            this.btn_CancelFilter6.UseVisualStyleBackColor = false;
            this.btn_CancelFilter6.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter6
            // 
            this.btn_SetFilter6.Location = new System.Drawing.Point(248, 70);
            this.btn_SetFilter6.Name = "btn_SetFilter6";
            this.btn_SetFilter6.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter6.TabIndex = 144;
            this.btn_SetFilter6.Tag = "6";
            this.btn_SetFilter6.Text = "设置";
            this.btn_SetFilter6.UseVisualStyleBackColor = true;
            this.btn_SetFilter6.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter6.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(151, 74);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 12);
            this.label24.TabIndex = 142;
            this.label24.Text = "MASK：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(59, 74);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 12);
            this.label25.TabIndex = 141;
            this.label25.Text = "AID：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(4, 74);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 12);
            this.label26.TabIndex = 139;
            this.label26.Text = "滤波器6：";
            // 
            // btn_CancelFilter5
            // 
            this.btn_CancelFilter5.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter5.Location = new System.Drawing.Point(645, 47);
            this.btn_CancelFilter5.Name = "btn_CancelFilter5";
            this.btn_CancelFilter5.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter5.TabIndex = 138;
            this.btn_CancelFilter5.Tag = "5";
            this.btn_CancelFilter5.Text = "取消";
            this.btn_CancelFilter5.UseVisualStyleBackColor = false;
            this.btn_CancelFilter5.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter5
            // 
            this.btn_SetFilter5.Location = new System.Drawing.Point(594, 47);
            this.btn_SetFilter5.Name = "btn_SetFilter5";
            this.btn_SetFilter5.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter5.TabIndex = 137;
            this.btn_SetFilter5.Tag = "5";
            this.btn_SetFilter5.Text = "设置";
            this.btn_SetFilter5.UseVisualStyleBackColor = true;
            this.btn_SetFilter5.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter5.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(497, 51);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 12);
            this.label27.TabIndex = 135;
            this.label27.Text = "MASK：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(405, 51);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 12);
            this.label28.TabIndex = 134;
            this.label28.Text = "AID：";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(350, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(59, 12);
            this.label29.TabIndex = 132;
            this.label29.Text = "滤波器5：";
            // 
            // btn_CancelFilter4
            // 
            this.btn_CancelFilter4.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter4.Location = new System.Drawing.Point(299, 47);
            this.btn_CancelFilter4.Name = "btn_CancelFilter4";
            this.btn_CancelFilter4.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter4.TabIndex = 131;
            this.btn_CancelFilter4.Tag = "4";
            this.btn_CancelFilter4.Text = "取消";
            this.btn_CancelFilter4.UseVisualStyleBackColor = false;
            this.btn_CancelFilter4.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter4
            // 
            this.btn_SetFilter4.Location = new System.Drawing.Point(248, 47);
            this.btn_SetFilter4.Name = "btn_SetFilter4";
            this.btn_SetFilter4.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter4.TabIndex = 130;
            this.btn_SetFilter4.Tag = "4";
            this.btn_SetFilter4.Text = "设置";
            this.btn_SetFilter4.UseVisualStyleBackColor = true;
            this.btn_SetFilter4.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter4.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(151, 51);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 12);
            this.label30.TabIndex = 128;
            this.label30.Text = "MASK：";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(59, 51);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 12);
            this.label31.TabIndex = 127;
            this.label31.Text = "AID：";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 51);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(59, 12);
            this.label32.TabIndex = 125;
            this.label32.Text = "滤波器4：";
            // 
            // btn_CancelFilter3
            // 
            this.btn_CancelFilter3.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter3.Location = new System.Drawing.Point(645, 25);
            this.btn_CancelFilter3.Name = "btn_CancelFilter3";
            this.btn_CancelFilter3.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter3.TabIndex = 124;
            this.btn_CancelFilter3.Tag = "3";
            this.btn_CancelFilter3.Text = "取消";
            this.btn_CancelFilter3.UseVisualStyleBackColor = false;
            this.btn_CancelFilter3.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter3
            // 
            this.btn_SetFilter3.Location = new System.Drawing.Point(594, 25);
            this.btn_SetFilter3.Name = "btn_SetFilter3";
            this.btn_SetFilter3.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter3.TabIndex = 123;
            this.btn_SetFilter3.Tag = "3";
            this.btn_SetFilter3.Text = "设置";
            this.btn_SetFilter3.UseVisualStyleBackColor = true;
            this.btn_SetFilter3.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter3.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(497, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 121;
            this.label15.Text = "MASK：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(405, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 120;
            this.label16.Text = "AID：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(350, 29);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 12);
            this.label17.TabIndex = 118;
            this.label17.Text = "滤波器3：";
            // 
            // btn_CancelFilter2
            // 
            this.btn_CancelFilter2.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter2.Location = new System.Drawing.Point(299, 25);
            this.btn_CancelFilter2.Name = "btn_CancelFilter2";
            this.btn_CancelFilter2.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter2.TabIndex = 117;
            this.btn_CancelFilter2.Tag = "2";
            this.btn_CancelFilter2.Text = "取消";
            this.btn_CancelFilter2.UseVisualStyleBackColor = false;
            this.btn_CancelFilter2.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter2
            // 
            this.btn_SetFilter2.Location = new System.Drawing.Point(248, 25);
            this.btn_SetFilter2.Name = "btn_SetFilter2";
            this.btn_SetFilter2.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter2.TabIndex = 116;
            this.btn_SetFilter2.Tag = "2";
            this.btn_SetFilter2.Text = "设置";
            this.btn_SetFilter2.UseVisualStyleBackColor = true;
            this.btn_SetFilter2.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter2.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(151, 29);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 114;
            this.label18.Text = "MASK：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(59, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 12);
            this.label19.TabIndex = 113;
            this.label19.Text = "AID：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 29);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 12);
            this.label20.TabIndex = 111;
            this.label20.Text = "滤波器2：";
            // 
            // btn_CancelFilter1
            // 
            this.btn_CancelFilter1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_CancelFilter1.Location = new System.Drawing.Point(645, 2);
            this.btn_CancelFilter1.Name = "btn_CancelFilter1";
            this.btn_CancelFilter1.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter1.TabIndex = 110;
            this.btn_CancelFilter1.Tag = "1";
            this.btn_CancelFilter1.Text = "取消";
            this.btn_CancelFilter1.UseVisualStyleBackColor = false;
            this.btn_CancelFilter1.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter1
            // 
            this.btn_SetFilter1.Location = new System.Drawing.Point(594, 2);
            this.btn_SetFilter1.Name = "btn_SetFilter1";
            this.btn_SetFilter1.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter1.TabIndex = 109;
            this.btn_SetFilter1.Tag = "1";
            this.btn_SetFilter1.Text = "设置";
            this.btn_SetFilter1.UseVisualStyleBackColor = true;
            this.btn_SetFilter1.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter1.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(497, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 107;
            this.label12.Text = "MASK：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(405, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 12);
            this.label13.TabIndex = 106;
            this.label13.Text = "AID：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(350, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 12);
            this.label14.TabIndex = 104;
            this.label14.Text = "滤波器1：";
            // 
            // btn_CancelFilter0
            // 
            this.btn_CancelFilter0.BackColor = System.Drawing.Color.Transparent;
            this.btn_CancelFilter0.Location = new System.Drawing.Point(299, 2);
            this.btn_CancelFilter0.Name = "btn_CancelFilter0";
            this.btn_CancelFilter0.Size = new System.Drawing.Size(45, 20);
            this.btn_CancelFilter0.TabIndex = 103;
            this.btn_CancelFilter0.Tag = "0";
            this.btn_CancelFilter0.Text = "取消";
            this.btn_CancelFilter0.UseVisualStyleBackColor = false;
            this.btn_CancelFilter0.Click += new System.EventHandler(this.SingleFilterCancel);
            // 
            // btn_SetFilter0
            // 
            this.btn_SetFilter0.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_SetFilter0.Location = new System.Drawing.Point(248, 2);
            this.btn_SetFilter0.Name = "btn_SetFilter0";
            this.btn_SetFilter0.Size = new System.Drawing.Size(45, 20);
            this.btn_SetFilter0.TabIndex = 102;
            this.btn_SetFilter0.Tag = "0";
            this.btn_SetFilter0.Text = "设置";
            this.btn_SetFilter0.UseVisualStyleBackColor = false;
            this.btn_SetFilter0.Click += new System.EventHandler(this.SingleFilterSet);
            this.btn_SetFilter0.MouseEnter += new System.EventHandler(this.FilterAnalysis);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(151, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 100;
            this.label11.Text = "MASK：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(59, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 12);
            this.label10.TabIndex = 99;
            this.label10.Text = "AID：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 12);
            this.label9.TabIndex = 97;
            this.label9.Text = "滤波器0：";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pl_MainSend);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(774, 166);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "发送窗口";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pl_MainSend
            // 
            this.pl_MainSend.AutoScroll = true;
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle4);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime4);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID4);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData4);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType4);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType4);
            this.pl_MainSend.Controls.Add(this.label66);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle4);
            this.pl_MainSend.Controls.Add(this.btn_MainSend4);
            this.pl_MainSend.Controls.Add(this.label67);
            this.pl_MainSend.Controls.Add(this.label68);
            this.pl_MainSend.Controls.Add(this.label69);
            this.pl_MainSend.Controls.Add(this.label70);
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle5);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime5);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID5);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData5);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType5);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType5);
            this.pl_MainSend.Controls.Add(this.label71);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle5);
            this.pl_MainSend.Controls.Add(this.btn_MainSend5);
            this.pl_MainSend.Controls.Add(this.label72);
            this.pl_MainSend.Controls.Add(this.label73);
            this.pl_MainSend.Controls.Add(this.label74);
            this.pl_MainSend.Controls.Add(this.label75);
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle3);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime3);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID3);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData3);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType3);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType3);
            this.pl_MainSend.Controls.Add(this.label76);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle3);
            this.pl_MainSend.Controls.Add(this.btn_MainSend3);
            this.pl_MainSend.Controls.Add(this.label77);
            this.pl_MainSend.Controls.Add(this.label78);
            this.pl_MainSend.Controls.Add(this.label79);
            this.pl_MainSend.Controls.Add(this.label80);
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle1);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime1);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID1);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData1);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType1);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType1);
            this.pl_MainSend.Controls.Add(this.label61);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle1);
            this.pl_MainSend.Controls.Add(this.btn_MainSend1);
            this.pl_MainSend.Controls.Add(this.label62);
            this.pl_MainSend.Controls.Add(this.label63);
            this.pl_MainSend.Controls.Add(this.label64);
            this.pl_MainSend.Controls.Add(this.label65);
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle2);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime2);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID2);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData2);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType2);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType2);
            this.pl_MainSend.Controls.Add(this.label50);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle2);
            this.pl_MainSend.Controls.Add(this.btn_MainSend2);
            this.pl_MainSend.Controls.Add(this.label57);
            this.pl_MainSend.Controls.Add(this.label58);
            this.pl_MainSend.Controls.Add(this.label59);
            this.pl_MainSend.Controls.Add(this.label60);
            this.pl_MainSend.Controls.Add(this.btn_MainCycleSendCancle0);
            this.pl_MainSend.Controls.Add(this.txb_MainSendCycleTime0);
            this.pl_MainSend.Controls.Add(this.txb_MainSendID0);
            this.pl_MainSend.Controls.Add(this.txb_MainSendData0);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendDataType0);
            this.pl_MainSend.Controls.Add(this.cmb_MainSendIDType0);
            this.pl_MainSend.Controls.Add(this.label49);
            this.pl_MainSend.Controls.Add(this.btn_MainSendCycle0);
            this.pl_MainSend.Controls.Add(this.btn_MainSend0);
            this.pl_MainSend.Controls.Add(this.label48);
            this.pl_MainSend.Controls.Add(this.label47);
            this.pl_MainSend.Controls.Add(this.label46);
            this.pl_MainSend.Controls.Add(this.label45);
            this.pl_MainSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pl_MainSend.Location = new System.Drawing.Point(3, 3);
            this.pl_MainSend.Name = "pl_MainSend";
            this.pl_MainSend.Size = new System.Drawing.Size(768, 160);
            this.pl_MainSend.TabIndex = 0;
            this.pl_MainSend.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pl_SendBatch_MouseClick);
            // 
            // btn_MainCycleSendCancle4
            // 
            this.btn_MainCycleSendCancle4.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle4.Location = new System.Drawing.Point(710, 112);
            this.btn_MainCycleSendCancle4.Name = "btn_MainCycleSendCancle4";
            this.btn_MainCycleSendCancle4.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle4.TabIndex = 155;
            this.btn_MainCycleSendCancle4.Tag = "4";
            this.btn_MainCycleSendCancle4.Text = "停止";
            this.btn_MainCycleSendCancle4.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle4.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType4
            // 
            this.cmb_MainSendDataType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType4.FormattingEnabled = true;
            this.cmb_MainSendDataType4.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType4.Location = new System.Drawing.Point(159, 112);
            this.cmb_MainSendDataType4.Name = "cmb_MainSendDataType4";
            this.cmb_MainSendDataType4.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType4.TabIndex = 151;
            this.cmb_MainSendDataType4.Tag = "4";
            this.cmb_MainSendDataType4.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType4
            // 
            this.cmb_MainSendIDType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType4.FormattingEnabled = true;
            this.cmb_MainSendIDType4.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType4.Location = new System.Drawing.Point(52, 112);
            this.cmb_MainSendIDType4.Name = "cmb_MainSendIDType4";
            this.cmb_MainSendIDType4.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType4.TabIndex = 150;
            this.cmb_MainSendIDType4.Tag = "4";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(530, 115);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(65, 12);
            this.label66.TabIndex = 149;
            this.label66.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle4
            // 
            this.btn_MainSendCycle4.Location = new System.Drawing.Point(645, 112);
            this.btn_MainSendCycle4.Name = "btn_MainSendCycle4";
            this.btn_MainSendCycle4.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle4.TabIndex = 148;
            this.btn_MainSendCycle4.Tag = "4";
            this.btn_MainSendCycle4.Text = "循环发送";
            this.btn_MainSendCycle4.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle4.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend4
            // 
            this.btn_MainSend4.Location = new System.Drawing.Point(490, 111);
            this.btn_MainSend4.Name = "btn_MainSend4";
            this.btn_MainSend4.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend4.TabIndex = 147;
            this.btn_MainSend4.Tag = "4";
            this.btn_MainSend4.Text = "发送";
            this.btn_MainSend4.UseVisualStyleBackColor = true;
            this.btn_MainSend4.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(310, 116);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 12);
            this.label67.TabIndex = 146;
            this.label67.Text = "数据：";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(220, 116);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 12);
            this.label68.TabIndex = 145;
            this.label68.Text = "帧ID：";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(112, 116);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(53, 12);
            this.label69.TabIndex = 144;
            this.label69.Text = "帧类型：";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(3, 116);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(53, 12);
            this.label70.TabIndex = 143;
            this.label70.Text = "帧格式：";
            // 
            // btn_MainCycleSendCancle5
            // 
            this.btn_MainCycleSendCancle5.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle5.Location = new System.Drawing.Point(710, 140);
            this.btn_MainCycleSendCancle5.Name = "btn_MainCycleSendCancle5";
            this.btn_MainCycleSendCancle5.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle5.TabIndex = 142;
            this.btn_MainCycleSendCancle5.Tag = "5";
            this.btn_MainCycleSendCancle5.Text = "停止";
            this.btn_MainCycleSendCancle5.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle5.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType5
            // 
            this.cmb_MainSendDataType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType5.FormattingEnabled = true;
            this.cmb_MainSendDataType5.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType5.Location = new System.Drawing.Point(159, 140);
            this.cmb_MainSendDataType5.Name = "cmb_MainSendDataType5";
            this.cmb_MainSendDataType5.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType5.TabIndex = 138;
            this.cmb_MainSendDataType5.Tag = "5";
            this.cmb_MainSendDataType5.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType5
            // 
            this.cmb_MainSendIDType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType5.FormattingEnabled = true;
            this.cmb_MainSendIDType5.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType5.Location = new System.Drawing.Point(52, 140);
            this.cmb_MainSendIDType5.Name = "cmb_MainSendIDType5";
            this.cmb_MainSendIDType5.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType5.TabIndex = 137;
            this.cmb_MainSendIDType5.Tag = "5";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(530, 143);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(65, 12);
            this.label71.TabIndex = 136;
            this.label71.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle5
            // 
            this.btn_MainSendCycle5.Location = new System.Drawing.Point(645, 140);
            this.btn_MainSendCycle5.Name = "btn_MainSendCycle5";
            this.btn_MainSendCycle5.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle5.TabIndex = 135;
            this.btn_MainSendCycle5.Tag = "5";
            this.btn_MainSendCycle5.Text = "循环发送";
            this.btn_MainSendCycle5.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle5.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend5
            // 
            this.btn_MainSend5.Location = new System.Drawing.Point(490, 139);
            this.btn_MainSend5.Name = "btn_MainSend5";
            this.btn_MainSend5.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend5.TabIndex = 134;
            this.btn_MainSend5.Tag = "5";
            this.btn_MainSend5.Text = "发送";
            this.btn_MainSend5.UseVisualStyleBackColor = true;
            this.btn_MainSend5.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(310, 144);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(41, 12);
            this.label72.TabIndex = 133;
            this.label72.Text = "数据：";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(220, 144);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(41, 12);
            this.label73.TabIndex = 132;
            this.label73.Text = "帧ID：";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(112, 144);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(53, 12);
            this.label74.TabIndex = 131;
            this.label74.Text = "帧类型：";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(3, 144);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(53, 12);
            this.label75.TabIndex = 130;
            this.label75.Text = "帧格式：";
            // 
            // btn_MainCycleSendCancle3
            // 
            this.btn_MainCycleSendCancle3.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle3.Location = new System.Drawing.Point(710, 84);
            this.btn_MainCycleSendCancle3.Name = "btn_MainCycleSendCancle3";
            this.btn_MainCycleSendCancle3.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle3.TabIndex = 129;
            this.btn_MainCycleSendCancle3.Tag = "3";
            this.btn_MainCycleSendCancle3.Text = "停止";
            this.btn_MainCycleSendCancle3.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle3.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType3
            // 
            this.cmb_MainSendDataType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType3.FormattingEnabled = true;
            this.cmb_MainSendDataType3.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType3.Location = new System.Drawing.Point(159, 84);
            this.cmb_MainSendDataType3.Name = "cmb_MainSendDataType3";
            this.cmb_MainSendDataType3.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType3.TabIndex = 125;
            this.cmb_MainSendDataType3.Tag = "3";
            this.cmb_MainSendDataType3.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType3
            // 
            this.cmb_MainSendIDType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType3.FormattingEnabled = true;
            this.cmb_MainSendIDType3.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType3.Location = new System.Drawing.Point(52, 84);
            this.cmb_MainSendIDType3.Name = "cmb_MainSendIDType3";
            this.cmb_MainSendIDType3.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType3.TabIndex = 124;
            this.cmb_MainSendIDType3.Tag = "3";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(530, 87);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(65, 12);
            this.label76.TabIndex = 123;
            this.label76.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle3
            // 
            this.btn_MainSendCycle3.Location = new System.Drawing.Point(645, 84);
            this.btn_MainSendCycle3.Name = "btn_MainSendCycle3";
            this.btn_MainSendCycle3.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle3.TabIndex = 122;
            this.btn_MainSendCycle3.Tag = "3";
            this.btn_MainSendCycle3.Text = "循环发送";
            this.btn_MainSendCycle3.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle3.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend3
            // 
            this.btn_MainSend3.Location = new System.Drawing.Point(490, 83);
            this.btn_MainSend3.Name = "btn_MainSend3";
            this.btn_MainSend3.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend3.TabIndex = 121;
            this.btn_MainSend3.Tag = "3";
            this.btn_MainSend3.Text = "发送";
            this.btn_MainSend3.UseVisualStyleBackColor = true;
            this.btn_MainSend3.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(310, 88);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(41, 12);
            this.label77.TabIndex = 120;
            this.label77.Text = "数据：";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(220, 88);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(41, 12);
            this.label78.TabIndex = 119;
            this.label78.Text = "帧ID：";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(112, 88);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(53, 12);
            this.label79.TabIndex = 118;
            this.label79.Text = "帧类型：";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(3, 88);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(53, 12);
            this.label80.TabIndex = 117;
            this.label80.Text = "帧格式：";
            // 
            // btn_MainCycleSendCancle1
            // 
            this.btn_MainCycleSendCancle1.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle1.Location = new System.Drawing.Point(710, 28);
            this.btn_MainCycleSendCancle1.Name = "btn_MainCycleSendCancle1";
            this.btn_MainCycleSendCancle1.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle1.TabIndex = 116;
            this.btn_MainCycleSendCancle1.Tag = "1";
            this.btn_MainCycleSendCancle1.Text = "停止";
            this.btn_MainCycleSendCancle1.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle1.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType1
            // 
            this.cmb_MainSendDataType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType1.FormattingEnabled = true;
            this.cmb_MainSendDataType1.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType1.Location = new System.Drawing.Point(159, 28);
            this.cmb_MainSendDataType1.Name = "cmb_MainSendDataType1";
            this.cmb_MainSendDataType1.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType1.TabIndex = 112;
            this.cmb_MainSendDataType1.Tag = "1";
            this.cmb_MainSendDataType1.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType1
            // 
            this.cmb_MainSendIDType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType1.FormattingEnabled = true;
            this.cmb_MainSendIDType1.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType1.Location = new System.Drawing.Point(52, 28);
            this.cmb_MainSendIDType1.Name = "cmb_MainSendIDType1";
            this.cmb_MainSendIDType1.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType1.TabIndex = 111;
            this.cmb_MainSendIDType1.Tag = "1";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(530, 31);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(65, 12);
            this.label61.TabIndex = 110;
            this.label61.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle1
            // 
            this.btn_MainSendCycle1.Location = new System.Drawing.Point(645, 28);
            this.btn_MainSendCycle1.Name = "btn_MainSendCycle1";
            this.btn_MainSendCycle1.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle1.TabIndex = 109;
            this.btn_MainSendCycle1.Tag = "1";
            this.btn_MainSendCycle1.Text = "循环发送";
            this.btn_MainSendCycle1.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle1.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend1
            // 
            this.btn_MainSend1.Location = new System.Drawing.Point(490, 27);
            this.btn_MainSend1.Name = "btn_MainSend1";
            this.btn_MainSend1.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend1.TabIndex = 108;
            this.btn_MainSend1.Tag = "1";
            this.btn_MainSend1.Text = "发送";
            this.btn_MainSend1.UseVisualStyleBackColor = true;
            this.btn_MainSend1.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(310, 32);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(41, 12);
            this.label62.TabIndex = 107;
            this.label62.Text = "数据：";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(220, 32);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 12);
            this.label63.TabIndex = 106;
            this.label63.Text = "帧ID：";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(112, 32);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(53, 12);
            this.label64.TabIndex = 105;
            this.label64.Text = "帧类型：";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(3, 32);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(53, 12);
            this.label65.TabIndex = 104;
            this.label65.Text = "帧格式：";
            // 
            // btn_MainCycleSendCancle2
            // 
            this.btn_MainCycleSendCancle2.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle2.Location = new System.Drawing.Point(710, 56);
            this.btn_MainCycleSendCancle2.Name = "btn_MainCycleSendCancle2";
            this.btn_MainCycleSendCancle2.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle2.TabIndex = 103;
            this.btn_MainCycleSendCancle2.Tag = "2";
            this.btn_MainCycleSendCancle2.Text = "停止";
            this.btn_MainCycleSendCancle2.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle2.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType2
            // 
            this.cmb_MainSendDataType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType2.FormattingEnabled = true;
            this.cmb_MainSendDataType2.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType2.Location = new System.Drawing.Point(159, 56);
            this.cmb_MainSendDataType2.Name = "cmb_MainSendDataType2";
            this.cmb_MainSendDataType2.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType2.TabIndex = 99;
            this.cmb_MainSendDataType2.Tag = "2";
            this.cmb_MainSendDataType2.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType2
            // 
            this.cmb_MainSendIDType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType2.FormattingEnabled = true;
            this.cmb_MainSendIDType2.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType2.Location = new System.Drawing.Point(52, 56);
            this.cmb_MainSendIDType2.Name = "cmb_MainSendIDType2";
            this.cmb_MainSendIDType2.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType2.TabIndex = 98;
            this.cmb_MainSendIDType2.Tag = "2";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(530, 59);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 12);
            this.label50.TabIndex = 97;
            this.label50.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle2
            // 
            this.btn_MainSendCycle2.Location = new System.Drawing.Point(645, 56);
            this.btn_MainSendCycle2.Name = "btn_MainSendCycle2";
            this.btn_MainSendCycle2.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle2.TabIndex = 96;
            this.btn_MainSendCycle2.Tag = "2";
            this.btn_MainSendCycle2.Text = "循环发送";
            this.btn_MainSendCycle2.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle2.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend2
            // 
            this.btn_MainSend2.Location = new System.Drawing.Point(490, 55);
            this.btn_MainSend2.Name = "btn_MainSend2";
            this.btn_MainSend2.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend2.TabIndex = 95;
            this.btn_MainSend2.Tag = "2";
            this.btn_MainSend2.Text = "发送";
            this.btn_MainSend2.UseVisualStyleBackColor = true;
            this.btn_MainSend2.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(310, 60);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 12);
            this.label57.TabIndex = 94;
            this.label57.Text = "数据：";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(220, 60);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 12);
            this.label58.TabIndex = 93;
            this.label58.Text = "帧ID：";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(112, 60);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(53, 12);
            this.label59.TabIndex = 92;
            this.label59.Text = "帧类型：";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 60);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(53, 12);
            this.label60.TabIndex = 91;
            this.label60.Text = "帧格式：";
            // 
            // btn_MainCycleSendCancle0
            // 
            this.btn_MainCycleSendCancle0.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btn_MainCycleSendCancle0.Location = new System.Drawing.Point(710, 0);
            this.btn_MainCycleSendCancle0.Name = "btn_MainCycleSendCancle0";
            this.btn_MainCycleSendCancle0.Size = new System.Drawing.Size(38, 20);
            this.btn_MainCycleSendCancle0.TabIndex = 90;
            this.btn_MainCycleSendCancle0.Tag = "0";
            this.btn_MainCycleSendCancle0.Text = "停止";
            this.btn_MainCycleSendCancle0.UseVisualStyleBackColor = false;
            this.btn_MainCycleSendCancle0.Click += new System.EventHandler(this.btn_MainCycleSendCancle0_Click);
            // 
            // cmb_MainSendDataType0
            // 
            this.cmb_MainSendDataType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendDataType0.FormattingEnabled = true;
            this.cmb_MainSendDataType0.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.cmb_MainSendDataType0.Location = new System.Drawing.Point(159, 0);
            this.cmb_MainSendDataType0.Name = "cmb_MainSendDataType0";
            this.cmb_MainSendDataType0.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendDataType0.TabIndex = 86;
            this.cmb_MainSendDataType0.Tag = "0";
            this.cmb_MainSendDataType0.TextChanged += new System.EventHandler(this.cmb_MainSendDataType0_TextChanged);
            // 
            // cmb_MainSendIDType0
            // 
            this.cmb_MainSendIDType0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_MainSendIDType0.FormattingEnabled = true;
            this.cmb_MainSendIDType0.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_MainSendIDType0.Location = new System.Drawing.Point(52, 0);
            this.cmb_MainSendIDType0.Name = "cmb_MainSendIDType0";
            this.cmb_MainSendIDType0.Size = new System.Drawing.Size(60, 20);
            this.cmb_MainSendIDType0.TabIndex = 85;
            this.cmb_MainSendIDType0.Tag = "0";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(530, 3);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(65, 12);
            this.label49.TabIndex = 84;
            this.label49.Text = "周期(ms)：";
            // 
            // btn_MainSendCycle0
            // 
            this.btn_MainSendCycle0.Location = new System.Drawing.Point(645, 0);
            this.btn_MainSendCycle0.Name = "btn_MainSendCycle0";
            this.btn_MainSendCycle0.Size = new System.Drawing.Size(61, 20);
            this.btn_MainSendCycle0.TabIndex = 83;
            this.btn_MainSendCycle0.Tag = "0";
            this.btn_MainSendCycle0.Text = "循环发送";
            this.btn_MainSendCycle0.UseVisualStyleBackColor = true;
            this.btn_MainSendCycle0.Click += new System.EventHandler(this.btn_MainSendCycle0_Click);
            // 
            // btn_MainSend0
            // 
            this.btn_MainSend0.Location = new System.Drawing.Point(490, -1);
            this.btn_MainSend0.Name = "btn_MainSend0";
            this.btn_MainSend0.Size = new System.Drawing.Size(38, 20);
            this.btn_MainSend0.TabIndex = 82;
            this.btn_MainSend0.Tag = "0";
            this.btn_MainSend0.Text = "发送";
            this.btn_MainSend0.UseVisualStyleBackColor = true;
            this.btn_MainSend0.Click += new System.EventHandler(this.btn_MainSend0_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(310, 4);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 12);
            this.label48.TabIndex = 81;
            this.label48.Text = "数据：";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(220, 4);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 12);
            this.label47.TabIndex = 80;
            this.label47.Text = "帧ID：";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(112, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 79;
            this.label46.Text = "帧类型：";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(3, 4);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 12);
            this.label45.TabIndex = 78;
            this.label45.Text = "帧格式：";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightCyan;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridView1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.TransferDirection,
            this.Time,
            this.IDType,
            this.DataType,
            this.FrameID,
            this.DataLength,
            this.Data});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView1.Location = new System.Drawing.Point(0, 26);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView1.Size = new System.Drawing.Size(782, 19);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // Index
            // 
            this.Index.HeaderText = "序号";
            this.Index.Name = "Index";
            this.Index.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Index.Width = 60;
            // 
            // TransferDirection
            // 
            this.TransferDirection.HeaderText = "传输方向";
            this.TransferDirection.Name = "TransferDirection";
            this.TransferDirection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.TransferDirection.Width = 80;
            // 
            // Time
            // 
            this.Time.HeaderText = "时间标识";
            this.Time.Name = "Time";
            this.Time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Time.Width = 135;
            // 
            // IDType
            // 
            this.IDType.HeaderText = "帧类型";
            this.IDType.Name = "IDType";
            this.IDType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.IDType.Width = 80;
            // 
            // DataType
            // 
            this.DataType.HeaderText = "帧格式";
            this.DataType.Name = "DataType";
            this.DataType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DataType.Width = 80;
            // 
            // FrameID
            // 
            this.FrameID.HeaderText = "帧ID";
            this.FrameID.Name = "FrameID";
            this.FrameID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FrameID.Width = 80;
            // 
            // DataLength
            // 
            this.DataLength.HeaderText = "数据长度";
            this.DataLength.Name = "DataLength";
            this.DataLength.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.DataLength.Width = 60;
            // 
            // Data
            // 
            this.Data.HeaderText = "数据";
            this.Data.Name = "Data";
            this.Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Data.Width = 205;
            // 
            // txb_DataShow
            // 
            this.txb_DataShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txb_DataShow.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txb_DataShow.Location = new System.Drawing.Point(0, 46);
            this.txb_DataShow.Margin = new System.Windows.Forms.Padding(0);
            this.txb_DataShow.MaxLength = 0;
            this.txb_DataShow.Multiline = true;
            this.txb_DataShow.Name = "txb_DataShow";
            this.txb_DataShow.ReadOnly = true;
            this.txb_DataShow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txb_DataShow.Size = new System.Drawing.Size(778, 366);
            this.txb_DataShow.TabIndex = 6;
            // 
            // chb_NotUpdateRecvToDataGridView
            // 
            this.chb_NotUpdateRecvToDataGridView.AutoSize = true;
            this.chb_NotUpdateRecvToDataGridView.Location = new System.Drawing.Point(166, 4);
            this.chb_NotUpdateRecvToDataGridView.Name = "chb_NotUpdateRecvToDataGridView";
            this.chb_NotUpdateRecvToDataGridView.Size = new System.Drawing.Size(84, 16);
            this.chb_NotUpdateRecvToDataGridView.TabIndex = 5;
            this.chb_NotUpdateRecvToDataGridView.Text = "不显示接收";
            this.chb_NotUpdateRecvToDataGridView.UseVisualStyleBackColor = true;
            // 
            // btn_SaveTxt
            // 
            this.btn_SaveTxt.Location = new System.Drawing.Point(341, 1);
            this.btn_SaveTxt.Name = "btn_SaveTxt";
            this.btn_SaveTxt.Size = new System.Drawing.Size(75, 20);
            this.btn_SaveTxt.TabIndex = 3;
            this.btn_SaveTxt.Text = "保存为txt";
            this.btn_SaveTxt.UseVisualStyleBackColor = true;
            this.btn_SaveTxt.Click += new System.EventHandler(this.btn_SaveTxt_Click);
            // 
            // chb_NotUpdateDataGrid
            // 
            this.chb_NotUpdateDataGrid.AutoSize = true;
            this.chb_NotUpdateDataGrid.Location = new System.Drawing.Point(84, 4);
            this.chb_NotUpdateDataGrid.Name = "chb_NotUpdateDataGrid";
            this.chb_NotUpdateDataGrid.Size = new System.Drawing.Size(84, 16);
            this.chb_NotUpdateDataGrid.TabIndex = 2;
            this.chb_NotUpdateDataGrid.Text = "不显示发送";
            this.chb_NotUpdateDataGrid.UseVisualStyleBackColor = true;
            // 
            // btn_ClearData
            // 
            this.btn_ClearData.Location = new System.Drawing.Point(3, 1);
            this.btn_ClearData.Name = "btn_ClearData";
            this.btn_ClearData.Size = new System.Drawing.Size(75, 20);
            this.btn_ClearData.TabIndex = 1;
            this.btn_ClearData.Text = "清空数据";
            this.btn_ClearData.UseVisualStyleBackColor = true;
            this.btn_ClearData.Click += new System.EventHandler(this.btn_ClearData_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.pl_SendBatch);
            this.groupBox1.Location = new System.Drawing.Point(789, 53);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(689, 581);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "批量发送窗口:";
            // 
            // pl_SendBatch
            // 
            this.pl_SendBatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pl_SendBatch.AutoScroll = true;
            this.pl_SendBatch.Controls.Add(this.chb_All);
            this.pl_SendBatch.Controls.Add(this.btn_CycleSendCancle);
            this.pl_SendBatch.Controls.Add(this.txb_SendCycleTime);
            this.pl_SendBatch.Controls.Add(this.lab_cycleTimeInterval);
            this.pl_SendBatch.Controls.Add(this.btn_SendCycle);
            this.pl_SendBatch.Controls.Add(this.txb_Specification1);
            this.pl_SendBatch.Controls.Add(this.txb_Specification2);
            this.pl_SendBatch.Controls.Add(this.txb_Specification3);
            this.pl_SendBatch.Controls.Add(this.txb_Specification4);
            this.pl_SendBatch.Controls.Add(this.txb_Specification5);
            this.pl_SendBatch.Controls.Add(this.txb_Specification6);
            this.pl_SendBatch.Controls.Add(this.txb_Specification7);
            this.pl_SendBatch.Controls.Add(this.txb_Specification8);
            this.pl_SendBatch.Controls.Add(this.txb_Specification9);
            this.pl_SendBatch.Controls.Add(this.txb_Specification10);
            this.pl_SendBatch.Controls.Add(this.txb_Specification11);
            this.pl_SendBatch.Controls.Add(this.txb_Specification12);
            this.pl_SendBatch.Controls.Add(this.txb_Specification13);
            this.pl_SendBatch.Controls.Add(this.txb_Specification14);
            this.pl_SendBatch.Controls.Add(this.txb_Specification15);
            this.pl_SendBatch.Controls.Add(this.txb_Specification16);
            this.pl_SendBatch.Controls.Add(this.txb_Specification17);
            this.pl_SendBatch.Controls.Add(this.txb_Specification18);
            this.pl_SendBatch.Controls.Add(this.txb_Specification19);
            this.pl_SendBatch.Controls.Add(this.txb_Specification20);
            this.pl_SendBatch.Controls.Add(this.txb_SendID20);
            this.pl_SendBatch.Controls.Add(this.txb_SendData20);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType20);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType20);
            this.pl_SendBatch.Controls.Add(this.btn_Send20);
            this.pl_SendBatch.Controls.Add(this.label153);
            this.pl_SendBatch.Controls.Add(this.label154);
            this.pl_SendBatch.Controls.Add(this.label155);
            this.pl_SendBatch.Controls.Add(this.label156);
            this.pl_SendBatch.Controls.Add(this.chb_Send20);
            this.pl_SendBatch.Controls.Add(this.txb_SendID19);
            this.pl_SendBatch.Controls.Add(this.txb_SendData19);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType19);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType19);
            this.pl_SendBatch.Controls.Add(this.btn_Send19);
            this.pl_SendBatch.Controls.Add(this.label157);
            this.pl_SendBatch.Controls.Add(this.label158);
            this.pl_SendBatch.Controls.Add(this.label159);
            this.pl_SendBatch.Controls.Add(this.label160);
            this.pl_SendBatch.Controls.Add(this.chb_Send19);
            this.pl_SendBatch.Controls.Add(this.txb_SendID18);
            this.pl_SendBatch.Controls.Add(this.txb_SendData18);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType18);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType18);
            this.pl_SendBatch.Controls.Add(this.btn_Send18);
            this.pl_SendBatch.Controls.Add(this.label145);
            this.pl_SendBatch.Controls.Add(this.label146);
            this.pl_SendBatch.Controls.Add(this.label147);
            this.pl_SendBatch.Controls.Add(this.label148);
            this.pl_SendBatch.Controls.Add(this.chb_Send18);
            this.pl_SendBatch.Controls.Add(this.txb_SendID17);
            this.pl_SendBatch.Controls.Add(this.txb_SendData17);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType17);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType17);
            this.pl_SendBatch.Controls.Add(this.btn_Send17);
            this.pl_SendBatch.Controls.Add(this.label149);
            this.pl_SendBatch.Controls.Add(this.label150);
            this.pl_SendBatch.Controls.Add(this.label151);
            this.pl_SendBatch.Controls.Add(this.label152);
            this.pl_SendBatch.Controls.Add(this.chb_Send17);
            this.pl_SendBatch.Controls.Add(this.txb_SendID16);
            this.pl_SendBatch.Controls.Add(this.txb_SendData16);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType16);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType16);
            this.pl_SendBatch.Controls.Add(this.btn_Send16);
            this.pl_SendBatch.Controls.Add(this.label129);
            this.pl_SendBatch.Controls.Add(this.label130);
            this.pl_SendBatch.Controls.Add(this.label131);
            this.pl_SendBatch.Controls.Add(this.label132);
            this.pl_SendBatch.Controls.Add(this.chb_Send16);
            this.pl_SendBatch.Controls.Add(this.txb_SendID15);
            this.pl_SendBatch.Controls.Add(this.txb_SendData15);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType15);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType15);
            this.pl_SendBatch.Controls.Add(this.btn_Send15);
            this.pl_SendBatch.Controls.Add(this.label133);
            this.pl_SendBatch.Controls.Add(this.label134);
            this.pl_SendBatch.Controls.Add(this.label135);
            this.pl_SendBatch.Controls.Add(this.label136);
            this.pl_SendBatch.Controls.Add(this.chb_Send15);
            this.pl_SendBatch.Controls.Add(this.txb_SendID14);
            this.pl_SendBatch.Controls.Add(this.txb_SendData14);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType14);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType14);
            this.pl_SendBatch.Controls.Add(this.btn_Send14);
            this.pl_SendBatch.Controls.Add(this.label137);
            this.pl_SendBatch.Controls.Add(this.label138);
            this.pl_SendBatch.Controls.Add(this.label139);
            this.pl_SendBatch.Controls.Add(this.label140);
            this.pl_SendBatch.Controls.Add(this.chb_Send14);
            this.pl_SendBatch.Controls.Add(this.txb_SendID13);
            this.pl_SendBatch.Controls.Add(this.txb_SendData13);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType13);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType13);
            this.pl_SendBatch.Controls.Add(this.btn_Send13);
            this.pl_SendBatch.Controls.Add(this.label141);
            this.pl_SendBatch.Controls.Add(this.label142);
            this.pl_SendBatch.Controls.Add(this.label143);
            this.pl_SendBatch.Controls.Add(this.label144);
            this.pl_SendBatch.Controls.Add(this.chb_Send13);
            this.pl_SendBatch.Controls.Add(this.txb_SendID12);
            this.pl_SendBatch.Controls.Add(this.txb_SendData12);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType12);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType12);
            this.pl_SendBatch.Controls.Add(this.btn_Send12);
            this.pl_SendBatch.Controls.Add(this.label117);
            this.pl_SendBatch.Controls.Add(this.label118);
            this.pl_SendBatch.Controls.Add(this.label119);
            this.pl_SendBatch.Controls.Add(this.label120);
            this.pl_SendBatch.Controls.Add(this.chb_Send12);
            this.pl_SendBatch.Controls.Add(this.txb_SendID11);
            this.pl_SendBatch.Controls.Add(this.txb_SendData11);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType11);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType11);
            this.pl_SendBatch.Controls.Add(this.btn_Send11);
            this.pl_SendBatch.Controls.Add(this.label121);
            this.pl_SendBatch.Controls.Add(this.label122);
            this.pl_SendBatch.Controls.Add(this.label123);
            this.pl_SendBatch.Controls.Add(this.label124);
            this.pl_SendBatch.Controls.Add(this.chb_Send11);
            this.pl_SendBatch.Controls.Add(this.txb_SendID10);
            this.pl_SendBatch.Controls.Add(this.txb_SendData10);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType10);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType10);
            this.pl_SendBatch.Controls.Add(this.btn_Send10);
            this.pl_SendBatch.Controls.Add(this.label125);
            this.pl_SendBatch.Controls.Add(this.label126);
            this.pl_SendBatch.Controls.Add(this.label127);
            this.pl_SendBatch.Controls.Add(this.label128);
            this.pl_SendBatch.Controls.Add(this.chb_Send10);
            this.pl_SendBatch.Controls.Add(this.txb_SendID9);
            this.pl_SendBatch.Controls.Add(this.txb_SendData9);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType9);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType9);
            this.pl_SendBatch.Controls.Add(this.btn_Send9);
            this.pl_SendBatch.Controls.Add(this.label105);
            this.pl_SendBatch.Controls.Add(this.label106);
            this.pl_SendBatch.Controls.Add(this.label107);
            this.pl_SendBatch.Controls.Add(this.label108);
            this.pl_SendBatch.Controls.Add(this.chb_Send9);
            this.pl_SendBatch.Controls.Add(this.txb_SendID8);
            this.pl_SendBatch.Controls.Add(this.txb_SendData8);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType8);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType8);
            this.pl_SendBatch.Controls.Add(this.btn_Send8);
            this.pl_SendBatch.Controls.Add(this.label109);
            this.pl_SendBatch.Controls.Add(this.label110);
            this.pl_SendBatch.Controls.Add(this.label111);
            this.pl_SendBatch.Controls.Add(this.label112);
            this.pl_SendBatch.Controls.Add(this.chb_Send8);
            this.pl_SendBatch.Controls.Add(this.txb_SendID7);
            this.pl_SendBatch.Controls.Add(this.txb_SendData7);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType7);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType7);
            this.pl_SendBatch.Controls.Add(this.btn_Send7);
            this.pl_SendBatch.Controls.Add(this.label113);
            this.pl_SendBatch.Controls.Add(this.label114);
            this.pl_SendBatch.Controls.Add(this.label115);
            this.pl_SendBatch.Controls.Add(this.label116);
            this.pl_SendBatch.Controls.Add(this.chb_Send7);
            this.pl_SendBatch.Controls.Add(this.txb_SendID6);
            this.pl_SendBatch.Controls.Add(this.txb_SendData6);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType6);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType6);
            this.pl_SendBatch.Controls.Add(this.btn_Send6);
            this.pl_SendBatch.Controls.Add(this.label93);
            this.pl_SendBatch.Controls.Add(this.label94);
            this.pl_SendBatch.Controls.Add(this.label95);
            this.pl_SendBatch.Controls.Add(this.label96);
            this.pl_SendBatch.Controls.Add(this.chb_Send6);
            this.pl_SendBatch.Controls.Add(this.txb_SendID5);
            this.pl_SendBatch.Controls.Add(this.txb_SendData5);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType5);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType5);
            this.pl_SendBatch.Controls.Add(this.btn_Send5);
            this.pl_SendBatch.Controls.Add(this.label97);
            this.pl_SendBatch.Controls.Add(this.label98);
            this.pl_SendBatch.Controls.Add(this.label99);
            this.pl_SendBatch.Controls.Add(this.label100);
            this.pl_SendBatch.Controls.Add(this.chb_Send5);
            this.pl_SendBatch.Controls.Add(this.txb_SendID4);
            this.pl_SendBatch.Controls.Add(this.txb_SendData4);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType4);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType4);
            this.pl_SendBatch.Controls.Add(this.btn_Send4);
            this.pl_SendBatch.Controls.Add(this.label101);
            this.pl_SendBatch.Controls.Add(this.label102);
            this.pl_SendBatch.Controls.Add(this.label103);
            this.pl_SendBatch.Controls.Add(this.label104);
            this.pl_SendBatch.Controls.Add(this.chb_Send4);
            this.pl_SendBatch.Controls.Add(this.txb_SendID3);
            this.pl_SendBatch.Controls.Add(this.txb_SendData3);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType3);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType3);
            this.pl_SendBatch.Controls.Add(this.btn_Send3);
            this.pl_SendBatch.Controls.Add(this.label89);
            this.pl_SendBatch.Controls.Add(this.label90);
            this.pl_SendBatch.Controls.Add(this.label91);
            this.pl_SendBatch.Controls.Add(this.label92);
            this.pl_SendBatch.Controls.Add(this.chb_Send3);
            this.pl_SendBatch.Controls.Add(this.txb_SendID2);
            this.pl_SendBatch.Controls.Add(this.txb_SendData2);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType2);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType2);
            this.pl_SendBatch.Controls.Add(this.btn_Send2);
            this.pl_SendBatch.Controls.Add(this.label85);
            this.pl_SendBatch.Controls.Add(this.label86);
            this.pl_SendBatch.Controls.Add(this.label87);
            this.pl_SendBatch.Controls.Add(this.label88);
            this.pl_SendBatch.Controls.Add(this.chb_Send2);
            this.pl_SendBatch.Controls.Add(this.txb_SendID1);
            this.pl_SendBatch.Controls.Add(this.txb_SendData1);
            this.pl_SendBatch.Controls.Add(this.chb_SendDataType1);
            this.pl_SendBatch.Controls.Add(this.cmb_SendIDType1);
            this.pl_SendBatch.Controls.Add(this.btn_Send1);
            this.pl_SendBatch.Controls.Add(this.label81);
            this.pl_SendBatch.Controls.Add(this.label82);
            this.pl_SendBatch.Controls.Add(this.label83);
            this.pl_SendBatch.Controls.Add(this.label84);
            this.pl_SendBatch.Controls.Add(this.chb_Send1);
            this.pl_SendBatch.Location = new System.Drawing.Point(6, 20);
            this.pl_SendBatch.Name = "pl_SendBatch";
            this.pl_SendBatch.Size = new System.Drawing.Size(677, 555);
            this.pl_SendBatch.TabIndex = 0;
            this.pl_SendBatch.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pl_SendBatch_MouseClick);
            // 
            // chb_All
            // 
            this.chb_All.AutoSize = true;
            this.chb_All.Location = new System.Drawing.Point(6, 533);
            this.chb_All.Name = "chb_All";
            this.chb_All.Size = new System.Drawing.Size(48, 16);
            this.chb_All.TabIndex = 244;
            this.chb_All.Tag = "";
            this.chb_All.Text = "全选";
            this.chb_All.UseVisualStyleBackColor = true;
            this.chb_All.CheckedChanged += new System.EventHandler(this.chb_All_CheckedChanged);
            // 
            // btn_CycleSendCancle
            // 
            this.btn_CycleSendCancle.Location = new System.Drawing.Point(456, 529);
            this.btn_CycleSendCancle.Name = "btn_CycleSendCancle";
            this.btn_CycleSendCancle.Size = new System.Drawing.Size(75, 23);
            this.btn_CycleSendCancle.TabIndex = 243;
            this.btn_CycleSendCancle.Text = "停止";
            this.btn_CycleSendCancle.UseVisualStyleBackColor = true;
            this.btn_CycleSendCancle.Click += new System.EventHandler(this.btn_CycleSendCancle_Click);
            // 
            // txb_SendCycleTime
            // 
            this.txb_SendCycleTime.Location = new System.Drawing.Point(593, 531);
            this.txb_SendCycleTime.Multiline = true;
            this.txb_SendCycleTime.Name = "txb_SendCycleTime";
            this.txb_SendCycleTime.Size = new System.Drawing.Size(55, 18);
            this.txb_SendCycleTime.TabIndex = 242;
            this.txb_SendCycleTime.Text = "1000";
            // 
            // lab_cycleTimeInterval
            // 
            this.lab_cycleTimeInterval.AutoSize = true;
            this.lab_cycleTimeInterval.Location = new System.Drawing.Point(537, 534);
            this.lab_cycleTimeInterval.Name = "lab_cycleTimeInterval";
            this.lab_cycleTimeInterval.Size = new System.Drawing.Size(59, 12);
            this.lab_cycleTimeInterval.TabIndex = 241;
            this.lab_cycleTimeInterval.Text = "间隔(ms):";
            // 
            // btn_SendCycle
            // 
            this.btn_SendCycle.Location = new System.Drawing.Point(285, 529);
            this.btn_SendCycle.Name = "btn_SendCycle";
            this.btn_SendCycle.Size = new System.Drawing.Size(165, 23);
            this.btn_SendCycle.TabIndex = 240;
            this.btn_SendCycle.Text = "循环发送选中数据";
            this.btn_SendCycle.UseVisualStyleBackColor = true;
            this.btn_SendCycle.Click += new System.EventHandler(this.btn_SendCycle_Click);
            // 
            // txb_Specification1
            // 
            this.txb_Specification1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification1.Location = new System.Drawing.Point(40, 9);
            this.txb_Specification1.MaxLength = 10;
            this.txb_Specification1.Multiline = true;
            this.txb_Specification1.Name = "txb_Specification1";
            this.txb_Specification1.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification1.TabIndex = 239;
            // 
            // txb_Specification2
            // 
            this.txb_Specification2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification2.Location = new System.Drawing.Point(40, 35);
            this.txb_Specification2.MaxLength = 10;
            this.txb_Specification2.Multiline = true;
            this.txb_Specification2.Name = "txb_Specification2";
            this.txb_Specification2.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification2.TabIndex = 238;
            // 
            // txb_Specification3
            // 
            this.txb_Specification3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification3.Location = new System.Drawing.Point(40, 61);
            this.txb_Specification3.MaxLength = 10;
            this.txb_Specification3.Multiline = true;
            this.txb_Specification3.Name = "txb_Specification3";
            this.txb_Specification3.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification3.TabIndex = 237;
            // 
            // txb_Specification4
            // 
            this.txb_Specification4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification4.Location = new System.Drawing.Point(40, 87);
            this.txb_Specification4.MaxLength = 10;
            this.txb_Specification4.Multiline = true;
            this.txb_Specification4.Name = "txb_Specification4";
            this.txb_Specification4.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification4.TabIndex = 236;
            // 
            // txb_Specification5
            // 
            this.txb_Specification5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification5.Location = new System.Drawing.Point(40, 113);
            this.txb_Specification5.MaxLength = 10;
            this.txb_Specification5.Multiline = true;
            this.txb_Specification5.Name = "txb_Specification5";
            this.txb_Specification5.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification5.TabIndex = 235;
            // 
            // txb_Specification6
            // 
            this.txb_Specification6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification6.Location = new System.Drawing.Point(40, 139);
            this.txb_Specification6.MaxLength = 10;
            this.txb_Specification6.Multiline = true;
            this.txb_Specification6.Name = "txb_Specification6";
            this.txb_Specification6.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification6.TabIndex = 234;
            // 
            // txb_Specification7
            // 
            this.txb_Specification7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification7.Location = new System.Drawing.Point(40, 165);
            this.txb_Specification7.MaxLength = 10;
            this.txb_Specification7.Multiline = true;
            this.txb_Specification7.Name = "txb_Specification7";
            this.txb_Specification7.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification7.TabIndex = 233;
            // 
            // txb_Specification8
            // 
            this.txb_Specification8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification8.Location = new System.Drawing.Point(40, 191);
            this.txb_Specification8.MaxLength = 10;
            this.txb_Specification8.Multiline = true;
            this.txb_Specification8.Name = "txb_Specification8";
            this.txb_Specification8.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification8.TabIndex = 232;
            // 
            // txb_Specification9
            // 
            this.txb_Specification9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification9.Location = new System.Drawing.Point(40, 217);
            this.txb_Specification9.MaxLength = 10;
            this.txb_Specification9.Multiline = true;
            this.txb_Specification9.Name = "txb_Specification9";
            this.txb_Specification9.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification9.TabIndex = 231;
            // 
            // txb_Specification10
            // 
            this.txb_Specification10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification10.Location = new System.Drawing.Point(40, 243);
            this.txb_Specification10.MaxLength = 10;
            this.txb_Specification10.Multiline = true;
            this.txb_Specification10.Name = "txb_Specification10";
            this.txb_Specification10.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification10.TabIndex = 230;
            // 
            // txb_Specification11
            // 
            this.txb_Specification11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification11.Location = new System.Drawing.Point(40, 269);
            this.txb_Specification11.MaxLength = 10;
            this.txb_Specification11.Multiline = true;
            this.txb_Specification11.Name = "txb_Specification11";
            this.txb_Specification11.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification11.TabIndex = 229;
            // 
            // txb_Specification12
            // 
            this.txb_Specification12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification12.Location = new System.Drawing.Point(40, 295);
            this.txb_Specification12.MaxLength = 10;
            this.txb_Specification12.Multiline = true;
            this.txb_Specification12.Name = "txb_Specification12";
            this.txb_Specification12.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification12.TabIndex = 228;
            // 
            // txb_Specification13
            // 
            this.txb_Specification13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification13.Location = new System.Drawing.Point(40, 321);
            this.txb_Specification13.MaxLength = 10;
            this.txb_Specification13.Multiline = true;
            this.txb_Specification13.Name = "txb_Specification13";
            this.txb_Specification13.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification13.TabIndex = 227;
            // 
            // txb_Specification14
            // 
            this.txb_Specification14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification14.Location = new System.Drawing.Point(40, 347);
            this.txb_Specification14.MaxLength = 10;
            this.txb_Specification14.Multiline = true;
            this.txb_Specification14.Name = "txb_Specification14";
            this.txb_Specification14.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification14.TabIndex = 226;
            // 
            // txb_Specification15
            // 
            this.txb_Specification15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification15.Location = new System.Drawing.Point(40, 373);
            this.txb_Specification15.MaxLength = 10;
            this.txb_Specification15.Multiline = true;
            this.txb_Specification15.Name = "txb_Specification15";
            this.txb_Specification15.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification15.TabIndex = 225;
            // 
            // txb_Specification16
            // 
            this.txb_Specification16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification16.Location = new System.Drawing.Point(40, 399);
            this.txb_Specification16.MaxLength = 10;
            this.txb_Specification16.Multiline = true;
            this.txb_Specification16.Name = "txb_Specification16";
            this.txb_Specification16.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification16.TabIndex = 224;
            // 
            // txb_Specification17
            // 
            this.txb_Specification17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification17.Location = new System.Drawing.Point(40, 425);
            this.txb_Specification17.MaxLength = 10;
            this.txb_Specification17.Multiline = true;
            this.txb_Specification17.Name = "txb_Specification17";
            this.txb_Specification17.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification17.TabIndex = 223;
            // 
            // txb_Specification18
            // 
            this.txb_Specification18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification18.Location = new System.Drawing.Point(40, 451);
            this.txb_Specification18.MaxLength = 10;
            this.txb_Specification18.Multiline = true;
            this.txb_Specification18.Name = "txb_Specification18";
            this.txb_Specification18.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification18.TabIndex = 222;
            // 
            // txb_Specification19
            // 
            this.txb_Specification19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification19.Location = new System.Drawing.Point(40, 477);
            this.txb_Specification19.MaxLength = 10;
            this.txb_Specification19.Multiline = true;
            this.txb_Specification19.Name = "txb_Specification19";
            this.txb_Specification19.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification19.TabIndex = 221;
            // 
            // txb_Specification20
            // 
            this.txb_Specification20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_Specification20.Location = new System.Drawing.Point(40, 503);
            this.txb_Specification20.MaxLength = 10;
            this.txb_Specification20.Multiline = true;
            this.txb_Specification20.Name = "txb_Specification20";
            this.txb_Specification20.Size = new System.Drawing.Size(66, 18);
            this.txb_Specification20.TabIndex = 220;
            // 
            // txb_SendID20
            // 
            this.txb_SendID20.Location = new System.Drawing.Point(363, 503);
            this.txb_SendID20.Multiline = true;
            this.txb_SendID20.Name = "txb_SendID20";
            this.txb_SendID20.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID20.TabIndex = 219;
            this.txb_SendID20.Tag = "20";
            this.txb_SendID20.Text = "00000013";
            // 
            // txb_SendData20
            // 
            this.txb_SendData20.Location = new System.Drawing.Point(456, 503);
            this.txb_SendData20.Multiline = true;
            this.txb_SendData20.Name = "txb_SendData20";
            this.txb_SendData20.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData20.TabIndex = 218;
            this.txb_SendData20.Tag = "20";
            this.txb_SendData20.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType20
            // 
            this.chb_SendDataType20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType20.FormattingEnabled = true;
            this.chb_SendDataType20.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType20.Location = new System.Drawing.Point(267, 502);
            this.chb_SendDataType20.Name = "chb_SendDataType20";
            this.chb_SendDataType20.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType20.TabIndex = 217;
            this.chb_SendDataType20.Tag = "20";
            this.chb_SendDataType20.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType20
            // 
            this.cmb_SendIDType20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType20.FormattingEnabled = true;
            this.cmb_SendIDType20.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType20.Location = new System.Drawing.Point(160, 502);
            this.cmb_SendIDType20.Name = "cmb_SendIDType20";
            this.cmb_SendIDType20.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType20.TabIndex = 216;
            this.cmb_SendIDType20.Tag = "20";
            // 
            // btn_Send20
            // 
            this.btn_Send20.Location = new System.Drawing.Point(606, 502);
            this.btn_Send20.Name = "btn_Send20";
            this.btn_Send20.Size = new System.Drawing.Size(45, 20);
            this.btn_Send20.TabIndex = 215;
            this.btn_Send20.Tag = "20";
            this.btn_Send20.Text = "发送";
            this.btn_Send20.UseVisualStyleBackColor = true;
            this.btn_Send20.Click += new System.EventHandler(this.SendBatch);
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(424, 506);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(41, 12);
            this.label153.TabIndex = 214;
            this.label153.Text = "数据：";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(331, 505);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(41, 12);
            this.label154.TabIndex = 213;
            this.label154.Text = "帧ID：";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(220, 506);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(53, 12);
            this.label155.TabIndex = 212;
            this.label155.Text = "帧类型：";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(111, 506);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(53, 12);
            this.label156.TabIndex = 211;
            this.label156.Text = "帧格式：";
            // 
            // chb_Send20
            // 
            this.chb_Send20.AutoSize = true;
            this.chb_Send20.Location = new System.Drawing.Point(6, 504);
            this.chb_Send20.Name = "chb_Send20";
            this.chb_Send20.Size = new System.Drawing.Size(42, 16);
            this.chb_Send20.TabIndex = 210;
            this.chb_Send20.Tag = "20";
            this.chb_Send20.Text = "20:";
            this.chb_Send20.UseVisualStyleBackColor = true;
            // 
            // txb_SendID19
            // 
            this.txb_SendID19.Location = new System.Drawing.Point(363, 477);
            this.txb_SendID19.Multiline = true;
            this.txb_SendID19.Name = "txb_SendID19";
            this.txb_SendID19.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID19.TabIndex = 209;
            this.txb_SendID19.Tag = "19";
            this.txb_SendID19.Text = "00000012";
            // 
            // txb_SendData19
            // 
            this.txb_SendData19.Location = new System.Drawing.Point(456, 477);
            this.txb_SendData19.Multiline = true;
            this.txb_SendData19.Name = "txb_SendData19";
            this.txb_SendData19.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData19.TabIndex = 208;
            this.txb_SendData19.Tag = "19";
            this.txb_SendData19.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType19
            // 
            this.chb_SendDataType19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType19.FormattingEnabled = true;
            this.chb_SendDataType19.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType19.Location = new System.Drawing.Point(267, 476);
            this.chb_SendDataType19.Name = "chb_SendDataType19";
            this.chb_SendDataType19.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType19.TabIndex = 207;
            this.chb_SendDataType19.Tag = "19";
            this.chb_SendDataType19.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType19
            // 
            this.cmb_SendIDType19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType19.FormattingEnabled = true;
            this.cmb_SendIDType19.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType19.Location = new System.Drawing.Point(160, 476);
            this.cmb_SendIDType19.Name = "cmb_SendIDType19";
            this.cmb_SendIDType19.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType19.TabIndex = 206;
            this.cmb_SendIDType19.Tag = "19";
            // 
            // btn_Send19
            // 
            this.btn_Send19.Location = new System.Drawing.Point(606, 476);
            this.btn_Send19.Name = "btn_Send19";
            this.btn_Send19.Size = new System.Drawing.Size(45, 20);
            this.btn_Send19.TabIndex = 205;
            this.btn_Send19.Tag = "19";
            this.btn_Send19.Text = "发送";
            this.btn_Send19.UseVisualStyleBackColor = true;
            this.btn_Send19.Click += new System.EventHandler(this.SendBatch);
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(424, 480);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(41, 12);
            this.label157.TabIndex = 204;
            this.label157.Text = "数据：";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(331, 480);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(41, 12);
            this.label158.TabIndex = 203;
            this.label158.Text = "帧ID：";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(220, 480);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(53, 12);
            this.label159.TabIndex = 202;
            this.label159.Text = "帧类型：";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(111, 480);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(53, 12);
            this.label160.TabIndex = 201;
            this.label160.Text = "帧格式：";
            // 
            // chb_Send19
            // 
            this.chb_Send19.AutoSize = true;
            this.chb_Send19.Location = new System.Drawing.Point(6, 478);
            this.chb_Send19.Name = "chb_Send19";
            this.chb_Send19.Size = new System.Drawing.Size(42, 16);
            this.chb_Send19.TabIndex = 200;
            this.chb_Send19.Tag = "19";
            this.chb_Send19.Text = "19:";
            this.chb_Send19.UseVisualStyleBackColor = true;
            // 
            // txb_SendID18
            // 
            this.txb_SendID18.Location = new System.Drawing.Point(363, 451);
            this.txb_SendID18.Multiline = true;
            this.txb_SendID18.Name = "txb_SendID18";
            this.txb_SendID18.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID18.TabIndex = 199;
            this.txb_SendID18.Tag = "18";
            this.txb_SendID18.Text = "00000011";
            // 
            // txb_SendData18
            // 
            this.txb_SendData18.Location = new System.Drawing.Point(456, 451);
            this.txb_SendData18.Multiline = true;
            this.txb_SendData18.Name = "txb_SendData18";
            this.txb_SendData18.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData18.TabIndex = 198;
            this.txb_SendData18.Tag = "18";
            this.txb_SendData18.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType18
            // 
            this.chb_SendDataType18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType18.FormattingEnabled = true;
            this.chb_SendDataType18.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType18.Location = new System.Drawing.Point(267, 450);
            this.chb_SendDataType18.Name = "chb_SendDataType18";
            this.chb_SendDataType18.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType18.TabIndex = 197;
            this.chb_SendDataType18.Tag = "18";
            this.chb_SendDataType18.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType18
            // 
            this.cmb_SendIDType18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType18.FormattingEnabled = true;
            this.cmb_SendIDType18.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType18.Location = new System.Drawing.Point(160, 450);
            this.cmb_SendIDType18.Name = "cmb_SendIDType18";
            this.cmb_SendIDType18.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType18.TabIndex = 196;
            this.cmb_SendIDType18.Tag = "18";
            // 
            // btn_Send18
            // 
            this.btn_Send18.Location = new System.Drawing.Point(606, 450);
            this.btn_Send18.Name = "btn_Send18";
            this.btn_Send18.Size = new System.Drawing.Size(45, 20);
            this.btn_Send18.TabIndex = 195;
            this.btn_Send18.Tag = "18";
            this.btn_Send18.Text = "发送";
            this.btn_Send18.UseVisualStyleBackColor = true;
            this.btn_Send18.Click += new System.EventHandler(this.SendBatch);
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(424, 454);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(41, 12);
            this.label145.TabIndex = 194;
            this.label145.Text = "数据：";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(331, 454);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(41, 12);
            this.label146.TabIndex = 193;
            this.label146.Text = "帧ID：";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(220, 454);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(53, 12);
            this.label147.TabIndex = 192;
            this.label147.Text = "帧类型：";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(111, 454);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(53, 12);
            this.label148.TabIndex = 191;
            this.label148.Text = "帧格式：";
            // 
            // chb_Send18
            // 
            this.chb_Send18.AutoSize = true;
            this.chb_Send18.Location = new System.Drawing.Point(6, 452);
            this.chb_Send18.Name = "chb_Send18";
            this.chb_Send18.Size = new System.Drawing.Size(42, 16);
            this.chb_Send18.TabIndex = 190;
            this.chb_Send18.Tag = "18";
            this.chb_Send18.Text = "18:";
            this.chb_Send18.UseVisualStyleBackColor = true;
            // 
            // txb_SendID17
            // 
            this.txb_SendID17.Location = new System.Drawing.Point(363, 425);
            this.txb_SendID17.Multiline = true;
            this.txb_SendID17.Name = "txb_SendID17";
            this.txb_SendID17.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID17.TabIndex = 189;
            this.txb_SendID17.Tag = "17";
            this.txb_SendID17.Text = "00000010";
            // 
            // txb_SendData17
            // 
            this.txb_SendData17.Location = new System.Drawing.Point(456, 425);
            this.txb_SendData17.Multiline = true;
            this.txb_SendData17.Name = "txb_SendData17";
            this.txb_SendData17.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData17.TabIndex = 188;
            this.txb_SendData17.Tag = "17";
            this.txb_SendData17.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType17
            // 
            this.chb_SendDataType17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType17.FormattingEnabled = true;
            this.chb_SendDataType17.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType17.Location = new System.Drawing.Point(267, 424);
            this.chb_SendDataType17.Name = "chb_SendDataType17";
            this.chb_SendDataType17.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType17.TabIndex = 187;
            this.chb_SendDataType17.Tag = "17";
            this.chb_SendDataType17.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType17
            // 
            this.cmb_SendIDType17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType17.FormattingEnabled = true;
            this.cmb_SendIDType17.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType17.Location = new System.Drawing.Point(160, 424);
            this.cmb_SendIDType17.Name = "cmb_SendIDType17";
            this.cmb_SendIDType17.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType17.TabIndex = 186;
            this.cmb_SendIDType17.Tag = "17";
            // 
            // btn_Send17
            // 
            this.btn_Send17.Location = new System.Drawing.Point(606, 424);
            this.btn_Send17.Name = "btn_Send17";
            this.btn_Send17.Size = new System.Drawing.Size(45, 20);
            this.btn_Send17.TabIndex = 185;
            this.btn_Send17.Tag = "17";
            this.btn_Send17.Text = "发送";
            this.btn_Send17.UseVisualStyleBackColor = true;
            this.btn_Send17.Click += new System.EventHandler(this.SendBatch);
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(424, 428);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(41, 12);
            this.label149.TabIndex = 184;
            this.label149.Text = "数据：";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(331, 428);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(41, 12);
            this.label150.TabIndex = 183;
            this.label150.Text = "帧ID：";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(220, 428);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(53, 12);
            this.label151.TabIndex = 182;
            this.label151.Text = "帧类型：";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(111, 428);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(53, 12);
            this.label152.TabIndex = 181;
            this.label152.Text = "帧格式：";
            // 
            // chb_Send17
            // 
            this.chb_Send17.AutoSize = true;
            this.chb_Send17.Location = new System.Drawing.Point(6, 426);
            this.chb_Send17.Name = "chb_Send17";
            this.chb_Send17.Size = new System.Drawing.Size(42, 16);
            this.chb_Send17.TabIndex = 180;
            this.chb_Send17.Tag = "17";
            this.chb_Send17.Text = "17:";
            this.chb_Send17.UseVisualStyleBackColor = true;
            // 
            // txb_SendID16
            // 
            this.txb_SendID16.Location = new System.Drawing.Point(363, 399);
            this.txb_SendID16.Multiline = true;
            this.txb_SendID16.Name = "txb_SendID16";
            this.txb_SendID16.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID16.TabIndex = 179;
            this.txb_SendID16.Tag = "16";
            this.txb_SendID16.Text = "0000000F";
            // 
            // txb_SendData16
            // 
            this.txb_SendData16.Location = new System.Drawing.Point(456, 399);
            this.txb_SendData16.Multiline = true;
            this.txb_SendData16.Name = "txb_SendData16";
            this.txb_SendData16.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData16.TabIndex = 178;
            this.txb_SendData16.Tag = "16";
            this.txb_SendData16.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType16
            // 
            this.chb_SendDataType16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType16.FormattingEnabled = true;
            this.chb_SendDataType16.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType16.Location = new System.Drawing.Point(267, 398);
            this.chb_SendDataType16.Name = "chb_SendDataType16";
            this.chb_SendDataType16.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType16.TabIndex = 177;
            this.chb_SendDataType16.Tag = "16";
            this.chb_SendDataType16.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType16
            // 
            this.cmb_SendIDType16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType16.FormattingEnabled = true;
            this.cmb_SendIDType16.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType16.Location = new System.Drawing.Point(160, 398);
            this.cmb_SendIDType16.Name = "cmb_SendIDType16";
            this.cmb_SendIDType16.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType16.TabIndex = 176;
            this.cmb_SendIDType16.Tag = "16";
            // 
            // btn_Send16
            // 
            this.btn_Send16.Location = new System.Drawing.Point(606, 398);
            this.btn_Send16.Name = "btn_Send16";
            this.btn_Send16.Size = new System.Drawing.Size(45, 20);
            this.btn_Send16.TabIndex = 175;
            this.btn_Send16.Tag = "16";
            this.btn_Send16.Text = "发送";
            this.btn_Send16.UseVisualStyleBackColor = true;
            this.btn_Send16.Click += new System.EventHandler(this.SendBatch);
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(424, 402);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(41, 12);
            this.label129.TabIndex = 174;
            this.label129.Text = "数据：";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(331, 402);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(41, 12);
            this.label130.TabIndex = 173;
            this.label130.Text = "帧ID：";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(220, 402);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(53, 12);
            this.label131.TabIndex = 172;
            this.label131.Text = "帧类型：";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(111, 402);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(53, 12);
            this.label132.TabIndex = 171;
            this.label132.Text = "帧格式：";
            // 
            // chb_Send16
            // 
            this.chb_Send16.AutoSize = true;
            this.chb_Send16.Location = new System.Drawing.Point(6, 400);
            this.chb_Send16.Name = "chb_Send16";
            this.chb_Send16.Size = new System.Drawing.Size(42, 16);
            this.chb_Send16.TabIndex = 170;
            this.chb_Send16.Tag = "16";
            this.chb_Send16.Text = "16:";
            this.chb_Send16.UseVisualStyleBackColor = true;
            // 
            // txb_SendID15
            // 
            this.txb_SendID15.Location = new System.Drawing.Point(363, 373);
            this.txb_SendID15.Multiline = true;
            this.txb_SendID15.Name = "txb_SendID15";
            this.txb_SendID15.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID15.TabIndex = 169;
            this.txb_SendID15.Tag = "15";
            this.txb_SendID15.Text = "0000000E";
            // 
            // txb_SendData15
            // 
            this.txb_SendData15.Location = new System.Drawing.Point(456, 373);
            this.txb_SendData15.Multiline = true;
            this.txb_SendData15.Name = "txb_SendData15";
            this.txb_SendData15.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData15.TabIndex = 168;
            this.txb_SendData15.Tag = "15";
            this.txb_SendData15.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType15
            // 
            this.chb_SendDataType15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType15.FormattingEnabled = true;
            this.chb_SendDataType15.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType15.Location = new System.Drawing.Point(267, 372);
            this.chb_SendDataType15.Name = "chb_SendDataType15";
            this.chb_SendDataType15.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType15.TabIndex = 167;
            this.chb_SendDataType15.Tag = "15";
            this.chb_SendDataType15.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType15
            // 
            this.cmb_SendIDType15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType15.FormattingEnabled = true;
            this.cmb_SendIDType15.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType15.Location = new System.Drawing.Point(160, 372);
            this.cmb_SendIDType15.Name = "cmb_SendIDType15";
            this.cmb_SendIDType15.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType15.TabIndex = 166;
            this.cmb_SendIDType15.Tag = "15";
            // 
            // btn_Send15
            // 
            this.btn_Send15.Location = new System.Drawing.Point(606, 372);
            this.btn_Send15.Name = "btn_Send15";
            this.btn_Send15.Size = new System.Drawing.Size(45, 20);
            this.btn_Send15.TabIndex = 165;
            this.btn_Send15.Tag = "15";
            this.btn_Send15.Text = "发送";
            this.btn_Send15.UseVisualStyleBackColor = true;
            this.btn_Send15.Click += new System.EventHandler(this.SendBatch);
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(424, 376);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(41, 12);
            this.label133.TabIndex = 164;
            this.label133.Text = "数据：";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(331, 376);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(41, 12);
            this.label134.TabIndex = 163;
            this.label134.Text = "帧ID：";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(220, 376);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(53, 12);
            this.label135.TabIndex = 162;
            this.label135.Text = "帧类型：";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(111, 376);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(53, 12);
            this.label136.TabIndex = 161;
            this.label136.Text = "帧格式：";
            // 
            // chb_Send15
            // 
            this.chb_Send15.AutoSize = true;
            this.chb_Send15.Location = new System.Drawing.Point(6, 374);
            this.chb_Send15.Name = "chb_Send15";
            this.chb_Send15.Size = new System.Drawing.Size(42, 16);
            this.chb_Send15.TabIndex = 160;
            this.chb_Send15.Tag = "15";
            this.chb_Send15.Text = "15:";
            this.chb_Send15.UseVisualStyleBackColor = true;
            // 
            // txb_SendID14
            // 
            this.txb_SendID14.Location = new System.Drawing.Point(363, 347);
            this.txb_SendID14.Multiline = true;
            this.txb_SendID14.Name = "txb_SendID14";
            this.txb_SendID14.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID14.TabIndex = 159;
            this.txb_SendID14.Tag = "14";
            this.txb_SendID14.Text = "0000000D";
            // 
            // txb_SendData14
            // 
            this.txb_SendData14.Location = new System.Drawing.Point(456, 347);
            this.txb_SendData14.Multiline = true;
            this.txb_SendData14.Name = "txb_SendData14";
            this.txb_SendData14.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData14.TabIndex = 158;
            this.txb_SendData14.Tag = "14";
            this.txb_SendData14.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType14
            // 
            this.chb_SendDataType14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType14.FormattingEnabled = true;
            this.chb_SendDataType14.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType14.Location = new System.Drawing.Point(267, 346);
            this.chb_SendDataType14.Name = "chb_SendDataType14";
            this.chb_SendDataType14.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType14.TabIndex = 157;
            this.chb_SendDataType14.Tag = "14";
            this.chb_SendDataType14.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType14
            // 
            this.cmb_SendIDType14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType14.FormattingEnabled = true;
            this.cmb_SendIDType14.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType14.Location = new System.Drawing.Point(160, 346);
            this.cmb_SendIDType14.Name = "cmb_SendIDType14";
            this.cmb_SendIDType14.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType14.TabIndex = 156;
            this.cmb_SendIDType14.Tag = "14";
            // 
            // btn_Send14
            // 
            this.btn_Send14.Location = new System.Drawing.Point(606, 346);
            this.btn_Send14.Name = "btn_Send14";
            this.btn_Send14.Size = new System.Drawing.Size(45, 20);
            this.btn_Send14.TabIndex = 155;
            this.btn_Send14.Tag = "14";
            this.btn_Send14.Text = "发送";
            this.btn_Send14.UseVisualStyleBackColor = true;
            this.btn_Send14.Click += new System.EventHandler(this.SendBatch);
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(424, 350);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(41, 12);
            this.label137.TabIndex = 154;
            this.label137.Text = "数据：";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(331, 350);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(41, 12);
            this.label138.TabIndex = 153;
            this.label138.Text = "帧ID：";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(220, 350);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(53, 12);
            this.label139.TabIndex = 152;
            this.label139.Text = "帧类型：";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(111, 350);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(53, 12);
            this.label140.TabIndex = 151;
            this.label140.Text = "帧格式：";
            // 
            // chb_Send14
            // 
            this.chb_Send14.AutoSize = true;
            this.chb_Send14.Location = new System.Drawing.Point(6, 348);
            this.chb_Send14.Name = "chb_Send14";
            this.chb_Send14.Size = new System.Drawing.Size(42, 16);
            this.chb_Send14.TabIndex = 150;
            this.chb_Send14.Tag = "14";
            this.chb_Send14.Text = "14:";
            this.chb_Send14.UseVisualStyleBackColor = true;
            // 
            // txb_SendID13
            // 
            this.txb_SendID13.Location = new System.Drawing.Point(363, 321);
            this.txb_SendID13.Multiline = true;
            this.txb_SendID13.Name = "txb_SendID13";
            this.txb_SendID13.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID13.TabIndex = 149;
            this.txb_SendID13.Tag = "13";
            this.txb_SendID13.Text = "0000000C";
            // 
            // txb_SendData13
            // 
            this.txb_SendData13.Location = new System.Drawing.Point(456, 321);
            this.txb_SendData13.Multiline = true;
            this.txb_SendData13.Name = "txb_SendData13";
            this.txb_SendData13.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData13.TabIndex = 148;
            this.txb_SendData13.Tag = "13";
            this.txb_SendData13.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType13
            // 
            this.chb_SendDataType13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType13.FormattingEnabled = true;
            this.chb_SendDataType13.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType13.Location = new System.Drawing.Point(267, 320);
            this.chb_SendDataType13.Name = "chb_SendDataType13";
            this.chb_SendDataType13.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType13.TabIndex = 147;
            this.chb_SendDataType13.Tag = "13";
            this.chb_SendDataType13.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType13
            // 
            this.cmb_SendIDType13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType13.FormattingEnabled = true;
            this.cmb_SendIDType13.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType13.Location = new System.Drawing.Point(160, 320);
            this.cmb_SendIDType13.Name = "cmb_SendIDType13";
            this.cmb_SendIDType13.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType13.TabIndex = 146;
            this.cmb_SendIDType13.Tag = "13";
            // 
            // btn_Send13
            // 
            this.btn_Send13.Location = new System.Drawing.Point(606, 320);
            this.btn_Send13.Name = "btn_Send13";
            this.btn_Send13.Size = new System.Drawing.Size(45, 20);
            this.btn_Send13.TabIndex = 145;
            this.btn_Send13.Tag = "13";
            this.btn_Send13.Text = "发送";
            this.btn_Send13.UseVisualStyleBackColor = true;
            this.btn_Send13.Click += new System.EventHandler(this.SendBatch);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(424, 324);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(41, 12);
            this.label141.TabIndex = 144;
            this.label141.Text = "数据：";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(331, 324);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(41, 12);
            this.label142.TabIndex = 143;
            this.label142.Text = "帧ID：";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(220, 324);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(53, 12);
            this.label143.TabIndex = 142;
            this.label143.Text = "帧类型：";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(111, 324);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(53, 12);
            this.label144.TabIndex = 141;
            this.label144.Text = "帧格式：";
            // 
            // chb_Send13
            // 
            this.chb_Send13.AutoSize = true;
            this.chb_Send13.Location = new System.Drawing.Point(6, 322);
            this.chb_Send13.Name = "chb_Send13";
            this.chb_Send13.Size = new System.Drawing.Size(42, 16);
            this.chb_Send13.TabIndex = 140;
            this.chb_Send13.Tag = "13";
            this.chb_Send13.Text = "13:";
            this.chb_Send13.UseVisualStyleBackColor = true;
            // 
            // txb_SendID12
            // 
            this.txb_SendID12.Location = new System.Drawing.Point(363, 295);
            this.txb_SendID12.Multiline = true;
            this.txb_SendID12.Name = "txb_SendID12";
            this.txb_SendID12.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID12.TabIndex = 139;
            this.txb_SendID12.Tag = "12";
            this.txb_SendID12.Text = "0000000B";
            // 
            // txb_SendData12
            // 
            this.txb_SendData12.Location = new System.Drawing.Point(456, 295);
            this.txb_SendData12.Multiline = true;
            this.txb_SendData12.Name = "txb_SendData12";
            this.txb_SendData12.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData12.TabIndex = 138;
            this.txb_SendData12.Tag = "12";
            this.txb_SendData12.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType12
            // 
            this.chb_SendDataType12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType12.FormattingEnabled = true;
            this.chb_SendDataType12.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType12.Location = new System.Drawing.Point(267, 294);
            this.chb_SendDataType12.Name = "chb_SendDataType12";
            this.chb_SendDataType12.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType12.TabIndex = 137;
            this.chb_SendDataType12.Tag = "12";
            this.chb_SendDataType12.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType12
            // 
            this.cmb_SendIDType12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType12.FormattingEnabled = true;
            this.cmb_SendIDType12.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType12.Location = new System.Drawing.Point(160, 294);
            this.cmb_SendIDType12.Name = "cmb_SendIDType12";
            this.cmb_SendIDType12.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType12.TabIndex = 136;
            this.cmb_SendIDType12.Tag = "12";
            // 
            // btn_Send12
            // 
            this.btn_Send12.Location = new System.Drawing.Point(606, 294);
            this.btn_Send12.Name = "btn_Send12";
            this.btn_Send12.Size = new System.Drawing.Size(45, 20);
            this.btn_Send12.TabIndex = 135;
            this.btn_Send12.Tag = "12";
            this.btn_Send12.Text = "发送";
            this.btn_Send12.UseVisualStyleBackColor = true;
            this.btn_Send12.Click += new System.EventHandler(this.SendBatch);
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(424, 298);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(41, 12);
            this.label117.TabIndex = 134;
            this.label117.Text = "数据：";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(331, 298);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(41, 12);
            this.label118.TabIndex = 133;
            this.label118.Text = "帧ID：";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(220, 298);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(53, 12);
            this.label119.TabIndex = 132;
            this.label119.Text = "帧类型：";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(111, 298);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(53, 12);
            this.label120.TabIndex = 131;
            this.label120.Text = "帧格式：";
            // 
            // chb_Send12
            // 
            this.chb_Send12.AutoSize = true;
            this.chb_Send12.Location = new System.Drawing.Point(6, 296);
            this.chb_Send12.Name = "chb_Send12";
            this.chb_Send12.Size = new System.Drawing.Size(42, 16);
            this.chb_Send12.TabIndex = 130;
            this.chb_Send12.Tag = "12";
            this.chb_Send12.Text = "12:";
            this.chb_Send12.UseVisualStyleBackColor = true;
            // 
            // txb_SendID11
            // 
            this.txb_SendID11.Location = new System.Drawing.Point(363, 269);
            this.txb_SendID11.Multiline = true;
            this.txb_SendID11.Name = "txb_SendID11";
            this.txb_SendID11.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID11.TabIndex = 129;
            this.txb_SendID11.Tag = "11";
            this.txb_SendID11.Text = "0000000A";
            // 
            // txb_SendData11
            // 
            this.txb_SendData11.Location = new System.Drawing.Point(456, 269);
            this.txb_SendData11.Multiline = true;
            this.txb_SendData11.Name = "txb_SendData11";
            this.txb_SendData11.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData11.TabIndex = 128;
            this.txb_SendData11.Tag = "11";
            this.txb_SendData11.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType11
            // 
            this.chb_SendDataType11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType11.FormattingEnabled = true;
            this.chb_SendDataType11.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType11.Location = new System.Drawing.Point(267, 268);
            this.chb_SendDataType11.Name = "chb_SendDataType11";
            this.chb_SendDataType11.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType11.TabIndex = 127;
            this.chb_SendDataType11.Tag = "11";
            this.chb_SendDataType11.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType11
            // 
            this.cmb_SendIDType11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType11.FormattingEnabled = true;
            this.cmb_SendIDType11.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType11.Location = new System.Drawing.Point(160, 268);
            this.cmb_SendIDType11.Name = "cmb_SendIDType11";
            this.cmb_SendIDType11.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType11.TabIndex = 126;
            this.cmb_SendIDType11.Tag = "11";
            // 
            // btn_Send11
            // 
            this.btn_Send11.Location = new System.Drawing.Point(606, 268);
            this.btn_Send11.Name = "btn_Send11";
            this.btn_Send11.Size = new System.Drawing.Size(45, 20);
            this.btn_Send11.TabIndex = 125;
            this.btn_Send11.Tag = "11";
            this.btn_Send11.Text = "发送";
            this.btn_Send11.UseVisualStyleBackColor = true;
            this.btn_Send11.Click += new System.EventHandler(this.SendBatch);
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(424, 272);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(41, 12);
            this.label121.TabIndex = 124;
            this.label121.Text = "数据：";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(331, 272);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(41, 12);
            this.label122.TabIndex = 123;
            this.label122.Text = "帧ID：";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(220, 272);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(53, 12);
            this.label123.TabIndex = 122;
            this.label123.Text = "帧类型：";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(111, 272);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(53, 12);
            this.label124.TabIndex = 121;
            this.label124.Text = "帧格式：";
            // 
            // chb_Send11
            // 
            this.chb_Send11.AutoSize = true;
            this.chb_Send11.Location = new System.Drawing.Point(6, 270);
            this.chb_Send11.Name = "chb_Send11";
            this.chb_Send11.Size = new System.Drawing.Size(42, 16);
            this.chb_Send11.TabIndex = 120;
            this.chb_Send11.Tag = "11";
            this.chb_Send11.Text = "11:";
            this.chb_Send11.UseVisualStyleBackColor = true;
            // 
            // txb_SendID10
            // 
            this.txb_SendID10.Location = new System.Drawing.Point(363, 243);
            this.txb_SendID10.Multiline = true;
            this.txb_SendID10.Name = "txb_SendID10";
            this.txb_SendID10.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID10.TabIndex = 119;
            this.txb_SendID10.Tag = "10";
            this.txb_SendID10.Text = "00000009";
            // 
            // txb_SendData10
            // 
            this.txb_SendData10.Location = new System.Drawing.Point(456, 243);
            this.txb_SendData10.Multiline = true;
            this.txb_SendData10.Name = "txb_SendData10";
            this.txb_SendData10.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData10.TabIndex = 118;
            this.txb_SendData10.Tag = "10";
            this.txb_SendData10.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType10
            // 
            this.chb_SendDataType10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType10.FormattingEnabled = true;
            this.chb_SendDataType10.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType10.Location = new System.Drawing.Point(267, 242);
            this.chb_SendDataType10.Name = "chb_SendDataType10";
            this.chb_SendDataType10.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType10.TabIndex = 117;
            this.chb_SendDataType10.Tag = "10";
            this.chb_SendDataType10.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType10
            // 
            this.cmb_SendIDType10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType10.FormattingEnabled = true;
            this.cmb_SendIDType10.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType10.Location = new System.Drawing.Point(160, 242);
            this.cmb_SendIDType10.Name = "cmb_SendIDType10";
            this.cmb_SendIDType10.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType10.TabIndex = 116;
            this.cmb_SendIDType10.Tag = "10";
            // 
            // btn_Send10
            // 
            this.btn_Send10.Location = new System.Drawing.Point(606, 242);
            this.btn_Send10.Name = "btn_Send10";
            this.btn_Send10.Size = new System.Drawing.Size(45, 20);
            this.btn_Send10.TabIndex = 115;
            this.btn_Send10.Tag = "10";
            this.btn_Send10.Text = "发送";
            this.btn_Send10.UseVisualStyleBackColor = true;
            this.btn_Send10.Click += new System.EventHandler(this.SendBatch);
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(424, 246);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(41, 12);
            this.label125.TabIndex = 114;
            this.label125.Text = "数据：";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(331, 246);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(41, 12);
            this.label126.TabIndex = 113;
            this.label126.Text = "帧ID：";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(220, 246);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(53, 12);
            this.label127.TabIndex = 112;
            this.label127.Text = "帧类型：";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(111, 246);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(53, 12);
            this.label128.TabIndex = 111;
            this.label128.Text = "帧格式：";
            // 
            // chb_Send10
            // 
            this.chb_Send10.AutoSize = true;
            this.chb_Send10.Location = new System.Drawing.Point(6, 244);
            this.chb_Send10.Name = "chb_Send10";
            this.chb_Send10.Size = new System.Drawing.Size(42, 16);
            this.chb_Send10.TabIndex = 110;
            this.chb_Send10.Tag = "10";
            this.chb_Send10.Text = "10:";
            this.chb_Send10.UseVisualStyleBackColor = true;
            // 
            // txb_SendID9
            // 
            this.txb_SendID9.Location = new System.Drawing.Point(363, 217);
            this.txb_SendID9.Multiline = true;
            this.txb_SendID9.Name = "txb_SendID9";
            this.txb_SendID9.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID9.TabIndex = 109;
            this.txb_SendID9.Tag = "9";
            this.txb_SendID9.Text = "00000008";
            // 
            // txb_SendData9
            // 
            this.txb_SendData9.Location = new System.Drawing.Point(456, 217);
            this.txb_SendData9.Multiline = true;
            this.txb_SendData9.Name = "txb_SendData9";
            this.txb_SendData9.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData9.TabIndex = 108;
            this.txb_SendData9.Tag = "9";
            this.txb_SendData9.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType9
            // 
            this.chb_SendDataType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType9.FormattingEnabled = true;
            this.chb_SendDataType9.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType9.Location = new System.Drawing.Point(267, 216);
            this.chb_SendDataType9.Name = "chb_SendDataType9";
            this.chb_SendDataType9.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType9.TabIndex = 107;
            this.chb_SendDataType9.Tag = "9";
            this.chb_SendDataType9.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType9
            // 
            this.cmb_SendIDType9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType9.FormattingEnabled = true;
            this.cmb_SendIDType9.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType9.Location = new System.Drawing.Point(160, 216);
            this.cmb_SendIDType9.Name = "cmb_SendIDType9";
            this.cmb_SendIDType9.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType9.TabIndex = 106;
            this.cmb_SendIDType9.Tag = "9";
            // 
            // btn_Send9
            // 
            this.btn_Send9.Location = new System.Drawing.Point(606, 216);
            this.btn_Send9.Name = "btn_Send9";
            this.btn_Send9.Size = new System.Drawing.Size(45, 20);
            this.btn_Send9.TabIndex = 105;
            this.btn_Send9.Tag = "9";
            this.btn_Send9.Text = "发送";
            this.btn_Send9.UseVisualStyleBackColor = true;
            this.btn_Send9.Click += new System.EventHandler(this.SendBatch);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(424, 220);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(41, 12);
            this.label105.TabIndex = 104;
            this.label105.Text = "数据：";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(331, 220);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(41, 12);
            this.label106.TabIndex = 103;
            this.label106.Text = "帧ID：";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(220, 220);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(53, 12);
            this.label107.TabIndex = 102;
            this.label107.Text = "帧类型：";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(111, 220);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(53, 12);
            this.label108.TabIndex = 101;
            this.label108.Text = "帧格式：";
            // 
            // chb_Send9
            // 
            this.chb_Send9.AutoSize = true;
            this.chb_Send9.Location = new System.Drawing.Point(6, 218);
            this.chb_Send9.Name = "chb_Send9";
            this.chb_Send9.Size = new System.Drawing.Size(36, 16);
            this.chb_Send9.TabIndex = 100;
            this.chb_Send9.Tag = "9";
            this.chb_Send9.Text = "9:";
            this.chb_Send9.UseVisualStyleBackColor = true;
            // 
            // txb_SendID8
            // 
            this.txb_SendID8.Location = new System.Drawing.Point(363, 191);
            this.txb_SendID8.Multiline = true;
            this.txb_SendID8.Name = "txb_SendID8";
            this.txb_SendID8.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID8.TabIndex = 99;
            this.txb_SendID8.Tag = "8";
            this.txb_SendID8.Text = "00000007";
            // 
            // txb_SendData8
            // 
            this.txb_SendData8.Location = new System.Drawing.Point(456, 191);
            this.txb_SendData8.Multiline = true;
            this.txb_SendData8.Name = "txb_SendData8";
            this.txb_SendData8.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData8.TabIndex = 98;
            this.txb_SendData8.Tag = "8";
            this.txb_SendData8.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType8
            // 
            this.chb_SendDataType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType8.FormattingEnabled = true;
            this.chb_SendDataType8.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType8.Location = new System.Drawing.Point(267, 190);
            this.chb_SendDataType8.Name = "chb_SendDataType8";
            this.chb_SendDataType8.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType8.TabIndex = 97;
            this.chb_SendDataType8.Tag = "8";
            this.chb_SendDataType8.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType8
            // 
            this.cmb_SendIDType8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType8.FormattingEnabled = true;
            this.cmb_SendIDType8.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType8.Location = new System.Drawing.Point(160, 190);
            this.cmb_SendIDType8.Name = "cmb_SendIDType8";
            this.cmb_SendIDType8.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType8.TabIndex = 96;
            this.cmb_SendIDType8.Tag = "8";
            // 
            // btn_Send8
            // 
            this.btn_Send8.Location = new System.Drawing.Point(606, 190);
            this.btn_Send8.Name = "btn_Send8";
            this.btn_Send8.Size = new System.Drawing.Size(45, 20);
            this.btn_Send8.TabIndex = 95;
            this.btn_Send8.Tag = "8";
            this.btn_Send8.Text = "发送";
            this.btn_Send8.UseVisualStyleBackColor = true;
            this.btn_Send8.Click += new System.EventHandler(this.SendBatch);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(424, 194);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(41, 12);
            this.label109.TabIndex = 94;
            this.label109.Text = "数据：";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(331, 194);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(41, 12);
            this.label110.TabIndex = 93;
            this.label110.Text = "帧ID：";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(220, 194);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(53, 12);
            this.label111.TabIndex = 92;
            this.label111.Text = "帧类型：";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(111, 194);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(53, 12);
            this.label112.TabIndex = 91;
            this.label112.Text = "帧格式：";
            // 
            // chb_Send8
            // 
            this.chb_Send8.AutoSize = true;
            this.chb_Send8.Location = new System.Drawing.Point(6, 192);
            this.chb_Send8.Name = "chb_Send8";
            this.chb_Send8.Size = new System.Drawing.Size(36, 16);
            this.chb_Send8.TabIndex = 90;
            this.chb_Send8.Tag = "8";
            this.chb_Send8.Text = "8:";
            this.chb_Send8.UseVisualStyleBackColor = true;
            // 
            // txb_SendID7
            // 
            this.txb_SendID7.Location = new System.Drawing.Point(363, 165);
            this.txb_SendID7.Multiline = true;
            this.txb_SendID7.Name = "txb_SendID7";
            this.txb_SendID7.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID7.TabIndex = 89;
            this.txb_SendID7.Tag = "7";
            this.txb_SendID7.Text = "00000006";
            // 
            // txb_SendData7
            // 
            this.txb_SendData7.Location = new System.Drawing.Point(456, 165);
            this.txb_SendData7.Multiline = true;
            this.txb_SendData7.Name = "txb_SendData7";
            this.txb_SendData7.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData7.TabIndex = 88;
            this.txb_SendData7.Tag = "7";
            this.txb_SendData7.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType7
            // 
            this.chb_SendDataType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType7.FormattingEnabled = true;
            this.chb_SendDataType7.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType7.Location = new System.Drawing.Point(267, 164);
            this.chb_SendDataType7.Name = "chb_SendDataType7";
            this.chb_SendDataType7.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType7.TabIndex = 87;
            this.chb_SendDataType7.Tag = "7";
            this.chb_SendDataType7.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType7
            // 
            this.cmb_SendIDType7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType7.FormattingEnabled = true;
            this.cmb_SendIDType7.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType7.Location = new System.Drawing.Point(160, 164);
            this.cmb_SendIDType7.Name = "cmb_SendIDType7";
            this.cmb_SendIDType7.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType7.TabIndex = 86;
            this.cmb_SendIDType7.Tag = "7";
            // 
            // btn_Send7
            // 
            this.btn_Send7.Location = new System.Drawing.Point(606, 164);
            this.btn_Send7.Name = "btn_Send7";
            this.btn_Send7.Size = new System.Drawing.Size(45, 20);
            this.btn_Send7.TabIndex = 85;
            this.btn_Send7.Tag = "7";
            this.btn_Send7.Text = "发送";
            this.btn_Send7.UseVisualStyleBackColor = true;
            this.btn_Send7.Click += new System.EventHandler(this.SendBatch);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(424, 168);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(41, 12);
            this.label113.TabIndex = 84;
            this.label113.Text = "数据：";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(331, 168);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(41, 12);
            this.label114.TabIndex = 83;
            this.label114.Text = "帧ID：";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(220, 168);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(53, 12);
            this.label115.TabIndex = 82;
            this.label115.Text = "帧类型：";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(111, 168);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(53, 12);
            this.label116.TabIndex = 81;
            this.label116.Text = "帧格式：";
            // 
            // chb_Send7
            // 
            this.chb_Send7.AutoSize = true;
            this.chb_Send7.Location = new System.Drawing.Point(6, 166);
            this.chb_Send7.Name = "chb_Send7";
            this.chb_Send7.Size = new System.Drawing.Size(36, 16);
            this.chb_Send7.TabIndex = 80;
            this.chb_Send7.Tag = "7";
            this.chb_Send7.Text = "7:";
            this.chb_Send7.UseVisualStyleBackColor = true;
            // 
            // txb_SendID6
            // 
            this.txb_SendID6.Location = new System.Drawing.Point(363, 139);
            this.txb_SendID6.Multiline = true;
            this.txb_SendID6.Name = "txb_SendID6";
            this.txb_SendID6.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID6.TabIndex = 79;
            this.txb_SendID6.Tag = "6";
            this.txb_SendID6.Text = "00000005";
            // 
            // txb_SendData6
            // 
            this.txb_SendData6.Location = new System.Drawing.Point(456, 139);
            this.txb_SendData6.Multiline = true;
            this.txb_SendData6.Name = "txb_SendData6";
            this.txb_SendData6.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData6.TabIndex = 78;
            this.txb_SendData6.Tag = "6";
            this.txb_SendData6.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType6
            // 
            this.chb_SendDataType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType6.FormattingEnabled = true;
            this.chb_SendDataType6.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType6.Location = new System.Drawing.Point(267, 138);
            this.chb_SendDataType6.Name = "chb_SendDataType6";
            this.chb_SendDataType6.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType6.TabIndex = 77;
            this.chb_SendDataType6.Tag = "6";
            this.chb_SendDataType6.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType6
            // 
            this.cmb_SendIDType6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType6.FormattingEnabled = true;
            this.cmb_SendIDType6.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType6.Location = new System.Drawing.Point(160, 138);
            this.cmb_SendIDType6.Name = "cmb_SendIDType6";
            this.cmb_SendIDType6.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType6.TabIndex = 76;
            this.cmb_SendIDType6.Tag = "6";
            // 
            // btn_Send6
            // 
            this.btn_Send6.Location = new System.Drawing.Point(606, 138);
            this.btn_Send6.Name = "btn_Send6";
            this.btn_Send6.Size = new System.Drawing.Size(45, 20);
            this.btn_Send6.TabIndex = 75;
            this.btn_Send6.Tag = "6";
            this.btn_Send6.Text = "发送";
            this.btn_Send6.UseVisualStyleBackColor = true;
            this.btn_Send6.Click += new System.EventHandler(this.SendBatch);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(424, 142);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(41, 12);
            this.label93.TabIndex = 74;
            this.label93.Text = "数据：";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(331, 142);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(41, 12);
            this.label94.TabIndex = 73;
            this.label94.Text = "帧ID：";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(220, 142);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(53, 12);
            this.label95.TabIndex = 72;
            this.label95.Text = "帧类型：";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(111, 142);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(53, 12);
            this.label96.TabIndex = 71;
            this.label96.Text = "帧格式：";
            // 
            // chb_Send6
            // 
            this.chb_Send6.AutoSize = true;
            this.chb_Send6.Location = new System.Drawing.Point(6, 140);
            this.chb_Send6.Name = "chb_Send6";
            this.chb_Send6.Size = new System.Drawing.Size(36, 16);
            this.chb_Send6.TabIndex = 70;
            this.chb_Send6.Tag = "6";
            this.chb_Send6.Text = "6:";
            this.chb_Send6.UseVisualStyleBackColor = true;
            // 
            // txb_SendID5
            // 
            this.txb_SendID5.Location = new System.Drawing.Point(363, 113);
            this.txb_SendID5.Multiline = true;
            this.txb_SendID5.Name = "txb_SendID5";
            this.txb_SendID5.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID5.TabIndex = 69;
            this.txb_SendID5.Tag = "5";
            this.txb_SendID5.Text = "00000004";
            // 
            // txb_SendData5
            // 
            this.txb_SendData5.Location = new System.Drawing.Point(456, 113);
            this.txb_SendData5.Multiline = true;
            this.txb_SendData5.Name = "txb_SendData5";
            this.txb_SendData5.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData5.TabIndex = 68;
            this.txb_SendData5.Tag = "5";
            this.txb_SendData5.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType5
            // 
            this.chb_SendDataType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType5.FormattingEnabled = true;
            this.chb_SendDataType5.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType5.Location = new System.Drawing.Point(267, 112);
            this.chb_SendDataType5.Name = "chb_SendDataType5";
            this.chb_SendDataType5.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType5.TabIndex = 67;
            this.chb_SendDataType5.Tag = "5";
            this.chb_SendDataType5.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType5
            // 
            this.cmb_SendIDType5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType5.FormattingEnabled = true;
            this.cmb_SendIDType5.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType5.Location = new System.Drawing.Point(160, 112);
            this.cmb_SendIDType5.Name = "cmb_SendIDType5";
            this.cmb_SendIDType5.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType5.TabIndex = 66;
            this.cmb_SendIDType5.Tag = "5";
            // 
            // btn_Send5
            // 
            this.btn_Send5.Location = new System.Drawing.Point(606, 112);
            this.btn_Send5.Name = "btn_Send5";
            this.btn_Send5.Size = new System.Drawing.Size(45, 20);
            this.btn_Send5.TabIndex = 65;
            this.btn_Send5.Tag = "5";
            this.btn_Send5.Text = "发送";
            this.btn_Send5.UseVisualStyleBackColor = true;
            this.btn_Send5.Click += new System.EventHandler(this.SendBatch);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(424, 116);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(41, 12);
            this.label97.TabIndex = 64;
            this.label97.Text = "数据：";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(331, 116);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(41, 12);
            this.label98.TabIndex = 63;
            this.label98.Text = "帧ID：";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(220, 116);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(53, 12);
            this.label99.TabIndex = 62;
            this.label99.Text = "帧类型：";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(111, 116);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(53, 12);
            this.label100.TabIndex = 61;
            this.label100.Text = "帧格式：";
            // 
            // chb_Send5
            // 
            this.chb_Send5.AutoSize = true;
            this.chb_Send5.Location = new System.Drawing.Point(6, 114);
            this.chb_Send5.Name = "chb_Send5";
            this.chb_Send5.Size = new System.Drawing.Size(36, 16);
            this.chb_Send5.TabIndex = 60;
            this.chb_Send5.Tag = "5";
            this.chb_Send5.Text = "5:";
            this.chb_Send5.UseVisualStyleBackColor = true;
            // 
            // txb_SendID4
            // 
            this.txb_SendID4.Location = new System.Drawing.Point(363, 87);
            this.txb_SendID4.Multiline = true;
            this.txb_SendID4.Name = "txb_SendID4";
            this.txb_SendID4.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID4.TabIndex = 59;
            this.txb_SendID4.Tag = "4";
            this.txb_SendID4.Text = "00000003";
            // 
            // txb_SendData4
            // 
            this.txb_SendData4.Location = new System.Drawing.Point(456, 87);
            this.txb_SendData4.Multiline = true;
            this.txb_SendData4.Name = "txb_SendData4";
            this.txb_SendData4.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData4.TabIndex = 58;
            this.txb_SendData4.Tag = "4";
            this.txb_SendData4.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType4
            // 
            this.chb_SendDataType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType4.FormattingEnabled = true;
            this.chb_SendDataType4.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType4.Location = new System.Drawing.Point(267, 86);
            this.chb_SendDataType4.Name = "chb_SendDataType4";
            this.chb_SendDataType4.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType4.TabIndex = 57;
            this.chb_SendDataType4.Tag = "4";
            this.chb_SendDataType4.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType4
            // 
            this.cmb_SendIDType4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType4.FormattingEnabled = true;
            this.cmb_SendIDType4.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType4.Location = new System.Drawing.Point(160, 86);
            this.cmb_SendIDType4.Name = "cmb_SendIDType4";
            this.cmb_SendIDType4.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType4.TabIndex = 56;
            this.cmb_SendIDType4.Tag = "4";
            // 
            // btn_Send4
            // 
            this.btn_Send4.Location = new System.Drawing.Point(606, 86);
            this.btn_Send4.Name = "btn_Send4";
            this.btn_Send4.Size = new System.Drawing.Size(45, 20);
            this.btn_Send4.TabIndex = 55;
            this.btn_Send4.Tag = "4";
            this.btn_Send4.Text = "发送";
            this.btn_Send4.UseVisualStyleBackColor = true;
            this.btn_Send4.Click += new System.EventHandler(this.SendBatch);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(424, 90);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(41, 12);
            this.label101.TabIndex = 54;
            this.label101.Text = "数据：";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(331, 90);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(41, 12);
            this.label102.TabIndex = 53;
            this.label102.Text = "帧ID：";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(220, 90);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(53, 12);
            this.label103.TabIndex = 52;
            this.label103.Text = "帧类型：";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(111, 90);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(53, 12);
            this.label104.TabIndex = 51;
            this.label104.Text = "帧格式：";
            // 
            // chb_Send4
            // 
            this.chb_Send4.AutoSize = true;
            this.chb_Send4.Location = new System.Drawing.Point(6, 88);
            this.chb_Send4.Name = "chb_Send4";
            this.chb_Send4.Size = new System.Drawing.Size(36, 16);
            this.chb_Send4.TabIndex = 50;
            this.chb_Send4.Tag = "4";
            this.chb_Send4.Text = "4:";
            this.chb_Send4.UseVisualStyleBackColor = true;
            // 
            // txb_SendID3
            // 
            this.txb_SendID3.Location = new System.Drawing.Point(363, 61);
            this.txb_SendID3.Multiline = true;
            this.txb_SendID3.Name = "txb_SendID3";
            this.txb_SendID3.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID3.TabIndex = 49;
            this.txb_SendID3.Tag = "3";
            this.txb_SendID3.Text = "00000002";
            // 
            // txb_SendData3
            // 
            this.txb_SendData3.Location = new System.Drawing.Point(456, 61);
            this.txb_SendData3.Multiline = true;
            this.txb_SendData3.Name = "txb_SendData3";
            this.txb_SendData3.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData3.TabIndex = 48;
            this.txb_SendData3.Tag = "3";
            this.txb_SendData3.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType3
            // 
            this.chb_SendDataType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType3.FormattingEnabled = true;
            this.chb_SendDataType3.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType3.Location = new System.Drawing.Point(267, 60);
            this.chb_SendDataType3.Name = "chb_SendDataType3";
            this.chb_SendDataType3.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType3.TabIndex = 47;
            this.chb_SendDataType3.Tag = "3";
            this.chb_SendDataType3.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType3
            // 
            this.cmb_SendIDType3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType3.FormattingEnabled = true;
            this.cmb_SendIDType3.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType3.Location = new System.Drawing.Point(160, 60);
            this.cmb_SendIDType3.Name = "cmb_SendIDType3";
            this.cmb_SendIDType3.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType3.TabIndex = 46;
            this.cmb_SendIDType3.Tag = "3";
            // 
            // btn_Send3
            // 
            this.btn_Send3.Location = new System.Drawing.Point(606, 60);
            this.btn_Send3.Name = "btn_Send3";
            this.btn_Send3.Size = new System.Drawing.Size(45, 20);
            this.btn_Send3.TabIndex = 45;
            this.btn_Send3.Tag = "3";
            this.btn_Send3.Text = "发送";
            this.btn_Send3.UseVisualStyleBackColor = true;
            this.btn_Send3.Click += new System.EventHandler(this.SendBatch);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(424, 64);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(41, 12);
            this.label89.TabIndex = 44;
            this.label89.Text = "数据：";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(331, 64);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(41, 12);
            this.label90.TabIndex = 43;
            this.label90.Text = "帧ID：";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(220, 64);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(53, 12);
            this.label91.TabIndex = 42;
            this.label91.Text = "帧类型：";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(111, 64);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(53, 12);
            this.label92.TabIndex = 41;
            this.label92.Text = "帧格式：";
            // 
            // chb_Send3
            // 
            this.chb_Send3.AutoSize = true;
            this.chb_Send3.Location = new System.Drawing.Point(6, 62);
            this.chb_Send3.Name = "chb_Send3";
            this.chb_Send3.Size = new System.Drawing.Size(36, 16);
            this.chb_Send3.TabIndex = 40;
            this.chb_Send3.Tag = "3";
            this.chb_Send3.Text = "3:";
            this.chb_Send3.UseVisualStyleBackColor = true;
            // 
            // txb_SendID2
            // 
            this.txb_SendID2.Location = new System.Drawing.Point(363, 35);
            this.txb_SendID2.Multiline = true;
            this.txb_SendID2.Name = "txb_SendID2";
            this.txb_SendID2.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID2.TabIndex = 39;
            this.txb_SendID2.Tag = "2";
            this.txb_SendID2.Text = "00000001";
            // 
            // txb_SendData2
            // 
            this.txb_SendData2.Location = new System.Drawing.Point(456, 35);
            this.txb_SendData2.Multiline = true;
            this.txb_SendData2.Name = "txb_SendData2";
            this.txb_SendData2.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData2.TabIndex = 38;
            this.txb_SendData2.Tag = "2";
            this.txb_SendData2.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType2
            // 
            this.chb_SendDataType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType2.FormattingEnabled = true;
            this.chb_SendDataType2.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType2.Location = new System.Drawing.Point(267, 34);
            this.chb_SendDataType2.Name = "chb_SendDataType2";
            this.chb_SendDataType2.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType2.TabIndex = 37;
            this.chb_SendDataType2.Tag = "2";
            this.chb_SendDataType2.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType2
            // 
            this.cmb_SendIDType2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType2.FormattingEnabled = true;
            this.cmb_SendIDType2.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType2.Location = new System.Drawing.Point(160, 34);
            this.cmb_SendIDType2.Name = "cmb_SendIDType2";
            this.cmb_SendIDType2.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType2.TabIndex = 36;
            this.cmb_SendIDType2.Tag = "2";
            // 
            // btn_Send2
            // 
            this.btn_Send2.Location = new System.Drawing.Point(606, 34);
            this.btn_Send2.Name = "btn_Send2";
            this.btn_Send2.Size = new System.Drawing.Size(45, 20);
            this.btn_Send2.TabIndex = 35;
            this.btn_Send2.Tag = "2";
            this.btn_Send2.Text = "发送";
            this.btn_Send2.UseVisualStyleBackColor = true;
            this.btn_Send2.Click += new System.EventHandler(this.SendBatch);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(424, 38);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(41, 12);
            this.label85.TabIndex = 34;
            this.label85.Text = "数据：";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(331, 38);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(41, 12);
            this.label86.TabIndex = 33;
            this.label86.Text = "帧ID：";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(220, 38);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(53, 12);
            this.label87.TabIndex = 32;
            this.label87.Text = "帧类型：";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(111, 38);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(53, 12);
            this.label88.TabIndex = 31;
            this.label88.Text = "帧格式：";
            // 
            // chb_Send2
            // 
            this.chb_Send2.AutoSize = true;
            this.chb_Send2.Location = new System.Drawing.Point(6, 36);
            this.chb_Send2.Name = "chb_Send2";
            this.chb_Send2.Size = new System.Drawing.Size(36, 16);
            this.chb_Send2.TabIndex = 30;
            this.chb_Send2.Tag = "2";
            this.chb_Send2.Text = "2:";
            this.chb_Send2.UseVisualStyleBackColor = true;
            // 
            // txb_SendID1
            // 
            this.txb_SendID1.Location = new System.Drawing.Point(363, 9);
            this.txb_SendID1.Multiline = true;
            this.txb_SendID1.Name = "txb_SendID1";
            this.txb_SendID1.Size = new System.Drawing.Size(55, 18);
            this.txb_SendID1.TabIndex = 29;
            this.txb_SendID1.Tag = "1";
            this.txb_SendID1.Text = "00000000";
            // 
            // txb_SendData1
            // 
            this.txb_SendData1.Location = new System.Drawing.Point(456, 9);
            this.txb_SendData1.Multiline = true;
            this.txb_SendData1.Name = "txb_SendData1";
            this.txb_SendData1.Size = new System.Drawing.Size(144, 18);
            this.txb_SendData1.TabIndex = 28;
            this.txb_SendData1.Tag = "1";
            this.txb_SendData1.Text = "00 11 22 33 44 55 66 77";
            // 
            // chb_SendDataType1
            // 
            this.chb_SendDataType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chb_SendDataType1.FormattingEnabled = true;
            this.chb_SendDataType1.Items.AddRange(new object[] {
            "数据帧",
            "远程帧"});
            this.chb_SendDataType1.Location = new System.Drawing.Point(267, 8);
            this.chb_SendDataType1.Name = "chb_SendDataType1";
            this.chb_SendDataType1.Size = new System.Drawing.Size(60, 20);
            this.chb_SendDataType1.TabIndex = 27;
            this.chb_SendDataType1.Tag = "1";
            this.chb_SendDataType1.TextChanged += new System.EventHandler(this.cmb_DataTypeChange);
            // 
            // cmb_SendIDType1
            // 
            this.cmb_SendIDType1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_SendIDType1.FormattingEnabled = true;
            this.cmb_SendIDType1.Items.AddRange(new object[] {
            "标准帧",
            "扩展帧"});
            this.cmb_SendIDType1.Location = new System.Drawing.Point(160, 8);
            this.cmb_SendIDType1.Name = "cmb_SendIDType1";
            this.cmb_SendIDType1.Size = new System.Drawing.Size(60, 20);
            this.cmb_SendIDType1.TabIndex = 26;
            this.cmb_SendIDType1.Tag = "1";
            // 
            // btn_Send1
            // 
            this.btn_Send1.Location = new System.Drawing.Point(606, 8);
            this.btn_Send1.Name = "btn_Send1";
            this.btn_Send1.Size = new System.Drawing.Size(45, 20);
            this.btn_Send1.TabIndex = 25;
            this.btn_Send1.Tag = "1";
            this.btn_Send1.Text = "发送";
            this.btn_Send1.UseVisualStyleBackColor = true;
            this.btn_Send1.Click += new System.EventHandler(this.SendBatch);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(424, 12);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(41, 12);
            this.label81.TabIndex = 24;
            this.label81.Text = "数据：";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(331, 12);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(41, 12);
            this.label82.TabIndex = 23;
            this.label82.Text = "帧ID：";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(220, 12);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(53, 12);
            this.label83.TabIndex = 22;
            this.label83.Text = "帧类型：";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(111, 12);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(53, 12);
            this.label84.TabIndex = 21;
            this.label84.Text = "帧格式：";
            // 
            // chb_Send1
            // 
            this.chb_Send1.AutoSize = true;
            this.chb_Send1.Location = new System.Drawing.Point(6, 10);
            this.chb_Send1.Name = "chb_Send1";
            this.chb_Send1.Size = new System.Drawing.Size(36, 16);
            this.chb_Send1.TabIndex = 20;
            this.chb_Send1.Tag = "1";
            this.chb_Send1.Text = "1:";
            this.chb_Send1.UseVisualStyleBackColor = true;
            // 
            // tmr_MainSend0
            // 
            this.tmr_MainSend0.Tag = "0";
            this.tmr_MainSend0.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_MainSend1
            // 
            this.tmr_MainSend1.Tag = "1";
            this.tmr_MainSend1.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_MainSend2
            // 
            this.tmr_MainSend2.Tag = "2";
            this.tmr_MainSend2.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_MainSend3
            // 
            this.tmr_MainSend3.Tag = "3";
            this.tmr_MainSend3.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_MainSend4
            // 
            this.tmr_MainSend4.Tag = "4";
            this.tmr_MainSend4.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_MainSend5
            // 
            this.tmr_MainSend5.Tag = "5";
            this.tmr_MainSend5.Tick += new System.EventHandler(this.tmr_MainSend0_Tick);
            // 
            // tmr_Send
            // 
            this.tmr_Send.Tick += new System.EventHandler(this.tmr_Send_Tick);
            // 
            // timer1
            // 
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tmr_StrartRefreshCom
            // 
            this.tmr_StrartRefreshCom.Tick += new System.EventHandler(this.tmr_StrartRefreshCom_Tick);
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 682);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.btn_Adapter);
            this.Controls.Add(this.btn_LeftShrink);
            this.Controls.Add(this.btn_UpDownShrink);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_UpDownExtend);
            this.Controls.Add(this.btn_LeftExtend);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnPin);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Frm_Main";
            this.Text = "USB_CAN_DebugTool_V6.0";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frm_Main_FormClosing);
            this.Load += new System.EventHandler(this.Frm_Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pl_BasicFunction.ResumeLayout(false);
            this.gb_CanFunction.ResumeLayout(false);
            this.gb_CanFunction.PerformLayout();
            this.gb_SerialPortSet.ResumeLayout(false);
            this.gb_SerialPortSet.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.pl_FilterSet.ResumeLayout(false);
            this.pl_FilterSet.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.pl_MainSend.ResumeLayout(false);
            this.pl_MainSend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.pl_SendBatch.ResumeLayout(false);
            this.pl_SendBatch.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.Button btnPin;
        private System.Windows.Forms.ToolStripMenuItem 透明度ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity20Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity40Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity60Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity80Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity100Percent1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 显示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel9;
        private System.Windows.Forms.ToolStripStatusLabel ts_TransferOrderSum;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel11;
        private System.Windows.Forms.ToolStripStatusLabel ts_TransferSucessedSum;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel13;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel ts_Baudrate;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel16;
        private System.Windows.Forms.ToolStripStatusLabel ts_FilterEnabledSum;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel18;
        private System.Windows.Forms.ToolStripStatusLabel ts_TransferErrorSum;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel ts_RecvErrorSum;
        private System.Windows.Forms.ToolStripStatusLabel ts_TransferFailedSum;
        private System.Windows.Forms.ToolStripMenuItem tsm_StatusStripDisplay1;
        private System.Windows.Forms.ToolStripMenuItem tsm_StatusStripDisplay2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_LeftExtend;
        private System.Windows.Forms.ComboBox cmb_WorkFunSet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chb_AtuoRetransfer;
        private System.Windows.Forms.ComboBox cmb_Baudrate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chb_CustomEnable;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txb_BRPValue;
        private System.Windows.Forms.Button btn_BasicSet;
        private System.Windows.Forms.TextBox txb_CalculateCANBaudrate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel pl_SendBatch;
        private System.Windows.Forms.CheckBox chb_Send1;
        private System.Windows.Forms.TextBox txb_SendID1;
        private System.Windows.Forms.TextBox txb_SendData1;
        private System.Windows.Forms.ComboBox chb_SendDataType1;
        private System.Windows.Forms.ComboBox cmb_SendIDType1;
        private System.Windows.Forms.Button btn_Send1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TextBox txb_SendID12;
        private System.Windows.Forms.TextBox txb_SendData12;
        private System.Windows.Forms.ComboBox chb_SendDataType12;
        private System.Windows.Forms.ComboBox cmb_SendIDType12;
        private System.Windows.Forms.Button btn_Send12;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.CheckBox chb_Send12;
        private System.Windows.Forms.TextBox txb_SendID11;
        private System.Windows.Forms.TextBox txb_SendData11;
        private System.Windows.Forms.ComboBox chb_SendDataType11;
        private System.Windows.Forms.ComboBox cmb_SendIDType11;
        private System.Windows.Forms.Button btn_Send11;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.CheckBox chb_Send11;
        private System.Windows.Forms.TextBox txb_SendID10;
        private System.Windows.Forms.TextBox txb_SendData10;
        private System.Windows.Forms.ComboBox chb_SendDataType10;
        private System.Windows.Forms.ComboBox cmb_SendIDType10;
        private System.Windows.Forms.Button btn_Send10;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.CheckBox chb_Send10;
        private System.Windows.Forms.TextBox txb_SendID9;
        private System.Windows.Forms.TextBox txb_SendData9;
        private System.Windows.Forms.ComboBox chb_SendDataType9;
        private System.Windows.Forms.ComboBox cmb_SendIDType9;
        private System.Windows.Forms.Button btn_Send9;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.CheckBox chb_Send9;
        private System.Windows.Forms.TextBox txb_SendID8;
        private System.Windows.Forms.TextBox txb_SendData8;
        private System.Windows.Forms.ComboBox chb_SendDataType8;
        private System.Windows.Forms.ComboBox cmb_SendIDType8;
        private System.Windows.Forms.Button btn_Send8;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.CheckBox chb_Send8;
        private System.Windows.Forms.TextBox txb_SendID7;
        private System.Windows.Forms.TextBox txb_SendData7;
        private System.Windows.Forms.ComboBox chb_SendDataType7;
        private System.Windows.Forms.ComboBox cmb_SendIDType7;
        private System.Windows.Forms.Button btn_Send7;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.CheckBox chb_Send7;
        private System.Windows.Forms.TextBox txb_SendID6;
        private System.Windows.Forms.TextBox txb_SendData6;
        private System.Windows.Forms.ComboBox chb_SendDataType6;
        private System.Windows.Forms.ComboBox cmb_SendIDType6;
        private System.Windows.Forms.Button btn_Send6;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.CheckBox chb_Send6;
        private System.Windows.Forms.TextBox txb_SendID5;
        private System.Windows.Forms.TextBox txb_SendData5;
        private System.Windows.Forms.ComboBox chb_SendDataType5;
        private System.Windows.Forms.ComboBox cmb_SendIDType5;
        private System.Windows.Forms.Button btn_Send5;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.CheckBox chb_Send5;
        private System.Windows.Forms.TextBox txb_SendID4;
        private System.Windows.Forms.TextBox txb_SendData4;
        private System.Windows.Forms.ComboBox chb_SendDataType4;
        private System.Windows.Forms.ComboBox cmb_SendIDType4;
        private System.Windows.Forms.Button btn_Send4;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.CheckBox chb_Send4;
        private System.Windows.Forms.TextBox txb_SendID3;
        private System.Windows.Forms.TextBox txb_SendData3;
        private System.Windows.Forms.ComboBox chb_SendDataType3;
        private System.Windows.Forms.ComboBox cmb_SendIDType3;
        private System.Windows.Forms.Button btn_Send3;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox chb_Send3;
        private System.Windows.Forms.TextBox txb_SendID2;
        private System.Windows.Forms.TextBox txb_SendData2;
        private System.Windows.Forms.ComboBox chb_SendDataType2;
        private System.Windows.Forms.ComboBox cmb_SendIDType2;
        private System.Windows.Forms.Button btn_Send2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.CheckBox chb_Send2;
        private System.Windows.Forms.TextBox txb_SendID16;
        private System.Windows.Forms.TextBox txb_SendData16;
        private System.Windows.Forms.ComboBox chb_SendDataType16;
        private System.Windows.Forms.ComboBox cmb_SendIDType16;
        private System.Windows.Forms.Button btn_Send16;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.CheckBox chb_Send16;
        private System.Windows.Forms.TextBox txb_SendID15;
        private System.Windows.Forms.TextBox txb_SendData15;
        private System.Windows.Forms.ComboBox chb_SendDataType15;
        private System.Windows.Forms.ComboBox cmb_SendIDType15;
        private System.Windows.Forms.Button btn_Send15;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.CheckBox chb_Send15;
        private System.Windows.Forms.TextBox txb_SendID14;
        private System.Windows.Forms.TextBox txb_SendData14;
        private System.Windows.Forms.ComboBox chb_SendDataType14;
        private System.Windows.Forms.ComboBox cmb_SendIDType14;
        private System.Windows.Forms.Button btn_Send14;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.CheckBox chb_Send14;
        private System.Windows.Forms.TextBox txb_SendID13;
        private System.Windows.Forms.TextBox txb_SendData13;
        private System.Windows.Forms.ComboBox chb_SendDataType13;
        private System.Windows.Forms.ComboBox cmb_SendIDType13;
        private System.Windows.Forms.Button btn_Send13;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.CheckBox chb_Send13;
        private System.Windows.Forms.TextBox txb_SendID20;
        private System.Windows.Forms.TextBox txb_SendData20;
        private System.Windows.Forms.ComboBox chb_SendDataType20;
        private System.Windows.Forms.ComboBox cmb_SendIDType20;
        private System.Windows.Forms.Button btn_Send20;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.CheckBox chb_Send20;
        private System.Windows.Forms.TextBox txb_SendID19;
        private System.Windows.Forms.TextBox txb_SendData19;
        private System.Windows.Forms.ComboBox chb_SendDataType19;
        private System.Windows.Forms.ComboBox cmb_SendIDType19;
        private System.Windows.Forms.Button btn_Send19;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.CheckBox chb_Send19;
        private System.Windows.Forms.TextBox txb_SendID18;
        private System.Windows.Forms.TextBox txb_SendData18;
        private System.Windows.Forms.ComboBox chb_SendDataType18;
        private System.Windows.Forms.ComboBox cmb_SendIDType18;
        private System.Windows.Forms.Button btn_Send18;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.CheckBox chb_Send18;
        private System.Windows.Forms.TextBox txb_SendID17;
        private System.Windows.Forms.TextBox txb_SendData17;
        private System.Windows.Forms.ComboBox chb_SendDataType17;
        private System.Windows.Forms.ComboBox cmb_SendIDType17;
        private System.Windows.Forms.Button btn_Send17;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.CheckBox chb_Send17;
        private System.Windows.Forms.TextBox txb_Specification20;
        private System.Windows.Forms.TextBox txb_Specification1;
        private System.Windows.Forms.TextBox txb_Specification2;
        private System.Windows.Forms.TextBox txb_Specification3;
        private System.Windows.Forms.TextBox txb_Specification4;
        private System.Windows.Forms.TextBox txb_Specification5;
        private System.Windows.Forms.TextBox txb_Specification6;
        private System.Windows.Forms.TextBox txb_Specification7;
        private System.Windows.Forms.TextBox txb_Specification8;
        private System.Windows.Forms.TextBox txb_Specification9;
        private System.Windows.Forms.TextBox txb_Specification10;
        private System.Windows.Forms.TextBox txb_Specification11;
        private System.Windows.Forms.TextBox txb_Specification12;
        private System.Windows.Forms.TextBox txb_Specification13;
        private System.Windows.Forms.TextBox txb_Specification14;
        private System.Windows.Forms.TextBox txb_Specification15;
        private System.Windows.Forms.TextBox txb_Specification16;
        private System.Windows.Forms.TextBox txb_Specification17;
        private System.Windows.Forms.TextBox txb_Specification18;
        private System.Windows.Forms.TextBox txb_Specification19;
        private System.Windows.Forms.Button btn_SendCycle;
        private System.Windows.Forms.Label lab_cycleTimeInterval;
        private System.Windows.Forms.Button btn_CycleSendCancle;
        private System.Windows.Forms.TextBox txb_SendCycleTime;
        private System.Windows.Forms.Panel pl_FilterSet;
        private System.Windows.Forms.Button btn_CancelFilter13;
        private System.Windows.Forms.Button btn_SetFilter13;
        private System.Windows.Forms.TextBox txb_FilterMASK13;
        private System.Windows.Forms.TextBox txb_FilterID13;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Button btn_CancelFilter12;
        private System.Windows.Forms.Button btn_SetFilter12;
        private System.Windows.Forms.TextBox txb_FilterMASK12;
        private System.Windows.Forms.TextBox txb_FilterID12;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button btn_CancelFilter11;
        private System.Windows.Forms.Button btn_SetFilter11;
        private System.Windows.Forms.TextBox txb_FilterMASK11;
        private System.Windows.Forms.TextBox txb_FilterID11;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Button btn_CancelFilter10;
        private System.Windows.Forms.Button btn_SetFilter10;
        private System.Windows.Forms.TextBox txb_FilterMASK10;
        private System.Windows.Forms.TextBox txb_FilterID10;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Button btn_CancelFilter9;
        private System.Windows.Forms.Button btn_SetFilter9;
        private System.Windows.Forms.TextBox txb_FilterMASK9;
        private System.Windows.Forms.TextBox txb_FilterID9;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button btn_CancelFilter8;
        private System.Windows.Forms.Button btn_SetFilter8;
        private System.Windows.Forms.TextBox txb_FilterMASK8;
        private System.Windows.Forms.TextBox txb_FilterID8;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btn_CancelFilter7;
        private System.Windows.Forms.Button btn_SetFilter7;
        private System.Windows.Forms.TextBox txb_FilterMASK7;
        private System.Windows.Forms.TextBox txb_FilterID7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btn_CancelFilter6;
        private System.Windows.Forms.Button btn_SetFilter6;
        private System.Windows.Forms.TextBox txb_FilterMASK6;
        private System.Windows.Forms.TextBox txb_FilterID6;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btn_CancelFilter5;
        private System.Windows.Forms.Button btn_SetFilter5;
        private System.Windows.Forms.TextBox txb_FilterMASK5;
        private System.Windows.Forms.TextBox txb_FilterID5;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btn_CancelFilter4;
        private System.Windows.Forms.Button btn_SetFilter4;
        private System.Windows.Forms.TextBox txb_FilterMASK4;
        private System.Windows.Forms.TextBox txb_FilterID4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btn_CancelFilter3;
        private System.Windows.Forms.Button btn_SetFilter3;
        private System.Windows.Forms.TextBox txb_FilterMASK3;
        private System.Windows.Forms.TextBox txb_FilterID3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btn_CancelFilter2;
        private System.Windows.Forms.Button btn_SetFilter2;
        private System.Windows.Forms.TextBox txb_FilterMASK2;
        private System.Windows.Forms.TextBox txb_FilterID2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btn_CancelFilter1;
        private System.Windows.Forms.Button btn_SetFilter1;
        private System.Windows.Forms.TextBox txb_FilterMASK1;
        private System.Windows.Forms.TextBox txb_FilterID1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btn_CancelFilter0;
        private System.Windows.Forms.Button btn_SetFilter0;
        private System.Windows.Forms.TextBox txb_FilterMASK0;
        private System.Windows.Forms.TextBox txb_FilterID0;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel pl_MainSend;
        private System.Windows.Forms.Button btn_MainCycleSendCancle4;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime4;
        private System.Windows.Forms.TextBox txb_MainSendID4;
        private System.Windows.Forms.TextBox txb_MainSendData4;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType4;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType4;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Button btn_MainSendCycle4;
        private System.Windows.Forms.Button btn_MainSend4;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Button btn_MainCycleSendCancle5;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime5;
        private System.Windows.Forms.TextBox txb_MainSendID5;
        private System.Windows.Forms.TextBox txb_MainSendData5;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType5;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType5;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Button btn_MainSendCycle5;
        private System.Windows.Forms.Button btn_MainSend5;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Button btn_MainCycleSendCancle3;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime3;
        private System.Windows.Forms.TextBox txb_MainSendID3;
        private System.Windows.Forms.TextBox txb_MainSendData3;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType3;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType3;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Button btn_MainSendCycle3;
        private System.Windows.Forms.Button btn_MainSend3;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Button btn_MainCycleSendCancle1;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime1;
        private System.Windows.Forms.TextBox txb_MainSendID1;
        private System.Windows.Forms.TextBox txb_MainSendData1;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType1;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType1;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Button btn_MainSendCycle1;
        private System.Windows.Forms.Button btn_MainSend1;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Button btn_MainCycleSendCancle2;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime2;
        private System.Windows.Forms.TextBox txb_MainSendID2;
        private System.Windows.Forms.TextBox txb_MainSendData2;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType2;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType2;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Button btn_MainSendCycle2;
        private System.Windows.Forms.Button btn_MainSend2;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button btn_MainCycleSendCancle0;
        private System.Windows.Forms.TextBox txb_MainSendCycleTime0;
        private System.Windows.Forms.TextBox txb_MainSendID0;
        private System.Windows.Forms.TextBox txb_MainSendData0;
        private System.Windows.Forms.ComboBox cmb_MainSendDataType0;
        private System.Windows.Forms.ComboBox cmb_MainSendIDType0;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button btn_MainSendCycle0;
        private System.Windows.Forms.Button btn_MainSend0;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btn_SetFilterAll;
        private System.Windows.Forms.Button btn_CancelFilterAll;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity10Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity30Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity50Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity70Percent1;
        private System.Windows.Forms.ToolStripMenuItem tsm_Opacity90Percent1;
        private System.Windows.Forms.Button btn_UpDownExtend;
        private System.Windows.Forms.Button btn_UpDownShrink;
        private System.Windows.Forms.Button btn_LeftShrink;
        private System.Windows.Forms.Button btn_Adapter;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel ts_RecvSucessedSum;
        private System.Windows.Forms.ToolStripStatusLabel ts_WorkMode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel19;
        private System.Windows.Forms.ToolStripStatusLabel ts_AutoRetransferEnable;
        public System.Windows.Forms.ToolStripMenuItem tsm_OpenSendArrayWindow;
        private System.Windows.Forms.ToolStripMenuItem tsm_ResetSTM32;
        private System.Windows.Forms.ToolStripMenuItem tsm_ReserAPP;
        public System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox cmb_tBS2;
        private System.Windows.Forms.ComboBox cmb_tBS1;
        private System.Windows.Forms.Timer tmr_MainSend0;
        private System.Windows.Forms.Timer tmr_MainSend1;
        private System.Windows.Forms.Timer tmr_MainSend2;
        private System.Windows.Forms.Timer tmr_MainSend3;
        private System.Windows.Forms.Timer tmr_MainSend4;
        private System.Windows.Forms.Timer tmr_MainSend5;
        private System.Windows.Forms.Timer tmr_Send;
        private System.Windows.Forms.ToolStripStatusLabel ts_OffLineFlag;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel14;
        private System.Windows.Forms.ToolStripStatusLabel ts_LastErrorCode;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_ClearData;
        private System.Windows.Forms.Button btn_SaveExcel;
        private System.Windows.Forms.Button btn_SaveTxt;
        private System.Windows.Forms.Panel pl_BasicFunction;
        private System.Windows.Forms.GroupBox gb_CanFunction;
        private System.Windows.Forms.GroupBox gb_SerialPortSet;
        private System.Windows.Forms.Label lablel_SerialPortStatus;
        private System.Windows.Forms.Button btn_SerialPortRefresh;
        private System.Windows.Forms.Button btn_SerialPortOpen;
        private System.Windows.Forms.ComboBox cmb_SerialPortIndex;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.CheckBox chb_NotUpdateDataGrid;
        private System.Windows.Forms.CheckBox chb_All;
        public System.Windows.Forms.ToolStripMenuItem tsm_Help;
        private System.Windows.Forms.ToolStripSplitButton ts_Clear;
        private System.Windows.Forms.ToolStripStatusLabel ts_Version;
        private System.Windows.Forms.ToolStripMenuItem ts_ClearRecv;
        private System.Windows.Forms.ToolStripMenuItem ts_ClearTransfer;
        public System.Windows.Forms.CheckBox chb_NotUpdateRecvToDataGridView;
        private System.Windows.Forms.Button btn_FilterCal;
        public System.Windows.Forms.ToolStripMenuItem tsm_AboutUs;
        public System.Windows.Forms.TextBox txb_DataShow;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransferDirection;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataType;
        private System.Windows.Forms.DataGridViewTextBoxColumn FrameID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.TextBox txb_SamleValue;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Timer tmr_StrartRefreshCom;
    }
}

