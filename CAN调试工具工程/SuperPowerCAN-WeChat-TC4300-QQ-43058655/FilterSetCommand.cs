﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class FilterSetCommand_Class
    {
        public byte SendHead0 = 0x55;
        public byte SendHead1 = 0xAA;
        public byte FunctionCode  = 0x02;

        public byte FilterIndex   = 0x00;
        public byte OpenFilter    = 0x00;
        public byte FilterID_H    = 0x00;
        public byte FilterID_MH   = 0x00;
        public byte FilterID_ML   = 0x00;
        public byte FilterID_L    = 0x00;
        public byte FilterMask_H  = 0x00;
        public byte FilterMask_MH = 0x00;
        public byte FilterMask_ML = 0x00;
        public byte FilterMask_L  = 0x00;

        public byte Dummy = 0x00;
        public byte Sum   = 0x00;
        public byte SendTail = 0x88;
    }
}
