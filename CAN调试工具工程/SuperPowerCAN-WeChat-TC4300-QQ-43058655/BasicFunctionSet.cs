﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class BasicFunctionSetCommand_Class
    {
        public byte SendHead0 = 0x55;
        public byte SendHead1 = 0xAA;
        public byte FunctionCode = 0x01;

        public byte RunMode = 0x00;
        public byte tBS1 = 0x00;
        public byte tBS2 = 0x00;

        public UInt16 BRP = 0;
        public byte BRP_H = 0x00;
        public byte BRP_L = 0x00;
        public byte ForbidAutomaticReSend = 0x00;

        public byte Dummy = 0x00;
        public byte Sum = 0x00;
        public byte SendTail = 0x88;
    }
}
