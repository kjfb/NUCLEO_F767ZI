﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    public partial class Help : Form
    {
        public Help()
        {
            InitializeComponent();
        }

        private void Help_Load(object sender, EventArgs e)
        {
            this.textBox1.Text = "\t作者：WeChat:TC4300 QQ:43058655  具体使用方法,请参考使用手册和使用视频教程!  "
                                      + "\r\n\t淘宝购买地址（志明电子）: https://shop104166087.taobao.com"
                                      + "\r\n\t软件更新地址: http://www.zmdzbbs.com/ （官方技术论坛！）" 
                                      + "\r\n\r\n\t特别说明:关于技术支持!"
                                        + "只提供使用方法,不提供诸如:我为什么调试不通!为什么数据和我想的不一样!CAN总线显性位隐性位是什么?这一类问题!关于这类问题,请使用者自行解决!"
            + "\r\n\r\n\t产品推荐:USB-SPI/I2C/ADC/PWM调试工具:SPI支持全双工下的主模式/从模式!I2C支持:可设置地址的_主发送/主接收/从发送/从接收\r\n\t欢迎选购!";
        }

        private void Help_FormClosing(object sender, FormClosingEventArgs e)
        {
            Frm_Main.f1.tsm_Help.Enabled = Enabled;
            
        }
    }
}
