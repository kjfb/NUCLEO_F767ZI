﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    class ClearCount_Class
    {
        public byte SendHead0 = 0x55;
        public byte SendHead1 = 0xAA;
        public byte FunctionCode = 0x05;
        public byte ClearWay = 0x00;

        public byte Dummy = 0x00;
        public byte Sum = 0x00;
        public byte SendTail = 0x88;
    }
}
