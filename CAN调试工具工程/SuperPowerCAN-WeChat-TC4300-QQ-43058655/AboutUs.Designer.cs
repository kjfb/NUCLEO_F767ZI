﻿namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    partial class AboutUs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutUs));
            this.label164 = new System.Windows.Forms.Label();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_OpenWebsite = new System.Windows.Forms.Button();
            this.label163 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pic_WeChat = new System.Windows.Forms.PictureBox();
            this.pic_tb = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_WeChat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_tb)).BeginInit();
            this.SuspendLayout();
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label164.Location = new System.Drawing.Point(152, 76);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(464, 21);
            this.label164.TabIndex = 14;
            this.label164.Text = "本人承接C#上位机、STM32、STC、NXP、PCB及相关项目开发";
            // 
            // btn_Update
            // 
            this.btn_Update.Font = new System.Drawing.Font("Microsoft YaHei", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_Update.Location = new System.Drawing.Point(173, 135);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(422, 30);
            this.btn_Update.TabIndex = 13;
            this.btn_Update.Text = "软件更新：http://www.zmdzbbs.com/(USB-CAN专区)";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_OpenWebsite
            // 
            this.btn_OpenWebsite.Font = new System.Drawing.Font("Microsoft YaHei", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_OpenWebsite.Location = new System.Drawing.Point(173, 101);
            this.btn_OpenWebsite.Name = "btn_OpenWebsite";
            this.btn_OpenWebsite.Size = new System.Drawing.Size(422, 30);
            this.btn_OpenWebsite.TabIndex = 12;
            this.btn_OpenWebsite.Text = "淘宝网址：https://shop104166087.taobao.com/";
            this.btn_OpenWebsite.UseVisualStyleBackColor = true;
            this.btn_OpenWebsite.Click += new System.EventHandler(this.btn_OpenWebsite_Click);
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label163.Location = new System.Drawing.Point(225, 51);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(318, 21);
            this.label163.TabIndex = 11;
            this.label163.Text = "联系方式：WeChat:TC4300 QQ:43058655";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Microsoft YaHei", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label161.Location = new System.Drawing.Point(318, 9);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(133, 38);
            this.label161.TabIndex = 10;
            this.label161.Text = "志明电子";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(658, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 17);
            this.label2.TabIndex = 27;
            this.label2.Text = "微信二维码";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(40, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "淘宝二维码";
            // 
            // pic_WeChat
            // 
            this.pic_WeChat.Image = ((System.Drawing.Image)(resources.GetObject("pic_WeChat.Image")));
            this.pic_WeChat.Location = new System.Drawing.Point(620, 18);
            this.pic_WeChat.Name = "pic_WeChat";
            this.pic_WeChat.Size = new System.Drawing.Size(140, 140);
            this.pic_WeChat.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_WeChat.TabIndex = 25;
            this.pic_WeChat.TabStop = false;
            // 
            // pic_tb
            // 
            this.pic_tb.Image = ((System.Drawing.Image)(resources.GetObject("pic_tb.Image")));
            this.pic_tb.Location = new System.Drawing.Point(9, 18);
            this.pic_tb.Name = "pic_tb";
            this.pic_tb.Size = new System.Drawing.Size(140, 140);
            this.pic_tb.TabIndex = 24;
            this.pic_tb.TabStop = false;
            // 
            // AboutUs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(772, 180);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pic_WeChat);
            this.Controls.Add(this.pic_tb);
            this.Controls.Add(this.label164);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.btn_OpenWebsite);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.label161);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "AboutUs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AboutUs - 志明电子";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AboutUs_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pic_WeChat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_tb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_OpenWebsite;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pic_WeChat;
        private System.Windows.Forms.PictureBox pic_tb;
    }
}