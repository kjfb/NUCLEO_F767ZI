﻿namespace SuperPowerCAN_WeChat_TC4300_QQ_43058655
{
    partial class FilterCal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btn_SetTopmost = new System.Windows.Forms.Button();
            this.gb_SetFilter = new System.Windows.Forms.GroupBox();
            this.txb_SetExtendID25 = new System.Windows.Forms.TextBox();
            this.lb_SetFilterExtendFrame = new System.Windows.Forms.Label();
            this.txb_SetExtendID24 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txb_SetExtendID28 = new System.Windows.Forms.TextBox();
            this.txb_SetFilter_MASK = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID27 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID26 = new System.Windows.Forms.TextBox();
            this.lb_SetFilterStardardFrame = new System.Windows.Forms.Label();
            this.txb_SetExtendID1 = new System.Windows.Forms.TextBox();
            this.txb_SetFilter_AID = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID0 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txb_SetExtendID7 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rdb_DataFrame = new System.Windows.Forms.RadioButton();
            this.rdb_DataAndRemoteFrame = new System.Windows.Forms.RadioButton();
            this.rdb_RemoteFrame = new System.Windows.Forms.RadioButton();
            this.txb_SetExtendID6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txb_SetExtendID5 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdb_StandardFrame = new System.Windows.Forms.RadioButton();
            this.rdb_ExtendFrame = new System.Windows.Forms.RadioButton();
            this.txb_SetExtendID4 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID9 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID3 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID8 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID2 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID2 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID9 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID3 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID8 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID10 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID15 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID4 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID14 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID5 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID13 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID6 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID12 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID7 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID11 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID0 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID10 = new System.Windows.Forms.TextBox();
            this.txb_SetStandardID1 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID17 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID18 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID16 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID19 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID23 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID20 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID22 = new System.Windows.Forms.TextBox();
            this.txb_SetExtendID21 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lb_AnalysisFilter_ExtendID = new System.Windows.Forms.Label();
            this.lb_AnalysisFilter_DisplayExtendID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lb_AnalysisFilter_StandardID = new System.Windows.Forms.Label();
            this.lb_AnalysisFilter_FrameType = new System.Windows.Forms.Label();
            this.lb_AnalysisFilter_FrameFormat = new System.Windows.Forms.Label();
            this.lb_AnalysisFilter_DisplayStandardID = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txb_AnalysisFilter_MASK = new System.Windows.Forms.TextBox();
            this.txb_AnalysisFilter_AID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rdb_StandardAndExtendFrame = new System.Windows.Forms.RadioButton();
            this.gb_SetFilter.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_SetTopmost
            // 
            this.btn_SetTopmost.BackgroundImage = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Resources.Pin0;
            this.btn_SetTopmost.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_SetTopmost.Location = new System.Drawing.Point(360, 10);
            this.btn_SetTopmost.Name = "btn_SetTopmost";
            this.btn_SetTopmost.Size = new System.Drawing.Size(27, 27);
            this.btn_SetTopmost.TabIndex = 3;
            this.toolTip1.SetToolTip(this.btn_SetTopmost, "前端显示");
            this.btn_SetTopmost.UseVisualStyleBackColor = true;
            this.btn_SetTopmost.Click += new System.EventHandler(this.btn_SetTopmost_Click);
            // 
            // gb_SetFilter
            // 
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID25);
            this.gb_SetFilter.Controls.Add(this.lb_SetFilterExtendFrame);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID24);
            this.gb_SetFilter.Controls.Add(this.label12);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID28);
            this.gb_SetFilter.Controls.Add(this.txb_SetFilter_MASK);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID27);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID26);
            this.gb_SetFilter.Controls.Add(this.lb_SetFilterStardardFrame);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID1);
            this.gb_SetFilter.Controls.Add(this.txb_SetFilter_AID);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID0);
            this.gb_SetFilter.Controls.Add(this.label7);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID7);
            this.gb_SetFilter.Controls.Add(this.panel2);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID6);
            this.gb_SetFilter.Controls.Add(this.label8);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID5);
            this.gb_SetFilter.Controls.Add(this.panel1);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID4);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID9);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID3);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID8);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID2);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID2);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID9);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID3);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID8);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID10);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID15);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID4);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID14);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID5);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID13);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID6);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID12);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID7);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID11);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID0);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID10);
            this.gb_SetFilter.Controls.Add(this.txb_SetStandardID1);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID17);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID18);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID16);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID19);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID23);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID20);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID22);
            this.gb_SetFilter.Controls.Add(this.txb_SetExtendID21);
            this.gb_SetFilter.Location = new System.Drawing.Point(6, 127);
            this.gb_SetFilter.Name = "gb_SetFilter";
            this.gb_SetFilter.Size = new System.Drawing.Size(390, 135);
            this.gb_SetFilter.TabIndex = 1;
            this.gb_SetFilter.TabStop = false;
            this.gb_SetFilter.Text = "设置滤波器:";
            // 
            // txb_SetExtendID25
            // 
            this.txb_SetExtendID25.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting25", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID25.Location = new System.Drawing.Point(110, 89);
            this.txb_SetExtendID25.MaxLength = 1;
            this.txb_SetExtendID25.Multiline = true;
            this.txb_SetExtendID25.Name = "txb_SetExtendID25";
            this.txb_SetExtendID25.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID25.TabIndex = 1003;
            this.txb_SetExtendID25.Tag = "25";
            this.txb_SetExtendID25.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting25;
            this.txb_SetExtendID25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID25.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID25.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // lb_SetFilterExtendFrame
            // 
            this.lb_SetFilterExtendFrame.AutoSize = true;
            this.lb_SetFilterExtendFrame.Location = new System.Drawing.Point(33, 92);
            this.lb_SetFilterExtendFrame.Name = "lb_SetFilterExtendFrame";
            this.lb_SetFilterExtendFrame.Size = new System.Drawing.Size(47, 12);
            this.lb_SetFilterExtendFrame.TabIndex = 470;
            this.lb_SetFilterExtendFrame.Text = "扩展帧:";
            // 
            // txb_SetExtendID24
            // 
            this.txb_SetExtendID24.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting24", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID24.Location = new System.Drawing.Point(120, 89);
            this.txb_SetExtendID24.MaxLength = 1;
            this.txb_SetExtendID24.Multiline = true;
            this.txb_SetExtendID24.Name = "txb_SetExtendID24";
            this.txb_SetExtendID24.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID24.TabIndex = 1004;
            this.txb_SetExtendID24.Tag = "24";
            this.txb_SetExtendID24.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting24;
            this.txb_SetExtendID24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID24.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID24.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 57);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 12);
            this.label12.TabIndex = 469;
            this.label12.Text = "二进制ID号：";
            // 
            // txb_SetExtendID28
            // 
            this.txb_SetExtendID28.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting28", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID28.Location = new System.Drawing.Point(80, 89);
            this.txb_SetExtendID28.MaxLength = 1;
            this.txb_SetExtendID28.Multiline = true;
            this.txb_SetExtendID28.Name = "txb_SetExtendID28";
            this.txb_SetExtendID28.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID28.TabIndex = 1000;
            this.txb_SetExtendID28.Tag = "28";
            this.txb_SetExtendID28.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting28;
            this.txb_SetExtendID28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID28.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID28.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetFilter_MASK
            // 
            this.txb_SetFilter_MASK.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetFilter_MASK.Location = new System.Drawing.Point(187, 115);
            this.txb_SetFilter_MASK.MaxLength = 12;
            this.txb_SetFilter_MASK.Multiline = true;
            this.txb_SetFilter_MASK.Name = "txb_SetFilter_MASK";
            this.txb_SetFilter_MASK.ReadOnly = true;
            this.txb_SetFilter_MASK.Size = new System.Drawing.Size(57, 18);
            this.txb_SetFilter_MASK.TabIndex = 466;
            this.txb_SetFilter_MASK.Text = "00000000";
            // 
            // txb_SetExtendID27
            // 
            this.txb_SetExtendID27.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting27", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID27.Location = new System.Drawing.Point(90, 89);
            this.txb_SetExtendID27.MaxLength = 1;
            this.txb_SetExtendID27.Multiline = true;
            this.txb_SetExtendID27.Name = "txb_SetExtendID27";
            this.txb_SetExtendID27.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID27.TabIndex = 1001;
            this.txb_SetExtendID27.Tag = "27";
            this.txb_SetExtendID27.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting27;
            this.txb_SetExtendID27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID27.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID27.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID26
            // 
            this.txb_SetExtendID26.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting26", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID26.Location = new System.Drawing.Point(100, 89);
            this.txb_SetExtendID26.MaxLength = 1;
            this.txb_SetExtendID26.Multiline = true;
            this.txb_SetExtendID26.Name = "txb_SetExtendID26";
            this.txb_SetExtendID26.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID26.TabIndex = 1002;
            this.txb_SetExtendID26.Tag = "26";
            this.txb_SetExtendID26.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting26;
            this.txb_SetExtendID26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID26.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID26.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // lb_SetFilterStardardFrame
            // 
            this.lb_SetFilterStardardFrame.AutoSize = true;
            this.lb_SetFilterStardardFrame.Location = new System.Drawing.Point(33, 74);
            this.lb_SetFilterStardardFrame.Name = "lb_SetFilterStardardFrame";
            this.lb_SetFilterStardardFrame.Size = new System.Drawing.Size(47, 12);
            this.lb_SetFilterStardardFrame.TabIndex = 4;
            this.lb_SetFilterStardardFrame.Text = "标准帧:";
            // 
            // txb_SetExtendID1
            // 
            this.txb_SetExtendID1.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID1.Location = new System.Drawing.Point(364, 89);
            this.txb_SetExtendID1.MaxLength = 1;
            this.txb_SetExtendID1.Multiline = true;
            this.txb_SetExtendID1.Name = "txb_SetExtendID1";
            this.txb_SetExtendID1.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID1.TabIndex = 1027;
            this.txb_SetExtendID1.Tag = "1";
            this.txb_SetExtendID1.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting1;
            this.txb_SetExtendID1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID1.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetFilter_AID
            // 
            this.txb_SetFilter_AID.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetFilter_AID.Location = new System.Drawing.Point(96, 115);
            this.txb_SetFilter_AID.MaxLength = 12;
            this.txb_SetFilter_AID.Multiline = true;
            this.txb_SetFilter_AID.Name = "txb_SetFilter_AID";
            this.txb_SetFilter_AID.ReadOnly = true;
            this.txb_SetFilter_AID.Size = new System.Drawing.Size(57, 18);
            this.txb_SetFilter_AID.TabIndex = 465;
            this.txb_SetFilter_AID.Text = "00000000";
            // 
            // txb_SetExtendID0
            // 
            this.txb_SetExtendID0.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting0", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID0.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID0.Location = new System.Drawing.Point(374, 89);
            this.txb_SetExtendID0.MaxLength = 1;
            this.txb_SetExtendID0.Multiline = true;
            this.txb_SetExtendID0.Name = "txb_SetExtendID0";
            this.txb_SetExtendID0.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID0.TabIndex = 1028;
            this.txb_SetExtendID0.Tag = "0";
            this.txb_SetExtendID0.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting0;
            this.txb_SetExtendID0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID0.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(156, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 12);
            this.label7.TabIndex = 464;
            this.label7.Text = "MASK:";
            // 
            // txb_SetExtendID7
            // 
            this.txb_SetExtendID7.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting7", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID7.Location = new System.Drawing.Point(304, 89);
            this.txb_SetExtendID7.MaxLength = 1;
            this.txb_SetExtendID7.Multiline = true;
            this.txb_SetExtendID7.Name = "txb_SetExtendID7";
            this.txb_SetExtendID7.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID7.TabIndex = 1021;
            this.txb_SetExtendID7.Tag = "7";
            this.txb_SetExtendID7.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting7;
            this.txb_SetExtendID7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID7.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rdb_DataFrame);
            this.panel2.Controls.Add(this.rdb_DataAndRemoteFrame);
            this.panel2.Controls.Add(this.rdb_RemoteFrame);
            this.panel2.Location = new System.Drawing.Point(19, 13);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 20);
            this.panel2.TabIndex = 3;
            // 
            // rdb_DataFrame
            // 
            this.rdb_DataFrame.AutoSize = true;
            this.rdb_DataFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_DataFrameCheckStatus;
            this.rdb_DataFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_DataFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_DataFrame.Location = new System.Drawing.Point(3, 3);
            this.rdb_DataFrame.Name = "rdb_DataFrame";
            this.rdb_DataFrame.Size = new System.Drawing.Size(59, 16);
            this.rdb_DataFrame.TabIndex = 0;
            this.rdb_DataFrame.Text = "数据帧";
            this.rdb_DataFrame.UseVisualStyleBackColor = true;
            this.rdb_DataFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // rdb_DataAndRemoteFrame
            // 
            this.rdb_DataAndRemoteFrame.AutoSize = true;
            this.rdb_DataAndRemoteFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_DataAndRemoteFrameCheckStatus;
            this.rdb_DataAndRemoteFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_DataAndRemoteFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_DataAndRemoteFrame.Location = new System.Drawing.Point(131, 3);
            this.rdb_DataAndRemoteFrame.Name = "rdb_DataAndRemoteFrame";
            this.rdb_DataAndRemoteFrame.Size = new System.Drawing.Size(107, 16);
            this.rdb_DataAndRemoteFrame.TabIndex = 2;
            this.rdb_DataAndRemoteFrame.TabStop = true;
            this.rdb_DataAndRemoteFrame.Text = "数据帧和远程帧";
            this.rdb_DataAndRemoteFrame.UseVisualStyleBackColor = true;
            this.rdb_DataAndRemoteFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // rdb_RemoteFrame
            // 
            this.rdb_RemoteFrame.AutoSize = true;
            this.rdb_RemoteFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_RemoteFrameCheckStatus;
            this.rdb_RemoteFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_RemoteFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_RemoteFrame.Location = new System.Drawing.Point(68, 3);
            this.rdb_RemoteFrame.Name = "rdb_RemoteFrame";
            this.rdb_RemoteFrame.Size = new System.Drawing.Size(59, 16);
            this.rdb_RemoteFrame.TabIndex = 1;
            this.rdb_RemoteFrame.Text = "远程帧";
            this.rdb_RemoteFrame.UseVisualStyleBackColor = true;
            this.rdb_RemoteFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // txb_SetExtendID6
            // 
            this.txb_SetExtendID6.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting6", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID6.Location = new System.Drawing.Point(314, 89);
            this.txb_SetExtendID6.MaxLength = 1;
            this.txb_SetExtendID6.Multiline = true;
            this.txb_SetExtendID6.Name = "txb_SetExtendID6";
            this.txb_SetExtendID6.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID6.TabIndex = 1022;
            this.txb_SetExtendID6.Tag = "6";
            this.txb_SetExtendID6.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting6;
            this.txb_SetExtendID6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID6.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 463;
            this.label8.Text = "AID:";
            // 
            // txb_SetExtendID5
            // 
            this.txb_SetExtendID5.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting5", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID5.Location = new System.Drawing.Point(324, 89);
            this.txb_SetExtendID5.MaxLength = 1;
            this.txb_SetExtendID5.Multiline = true;
            this.txb_SetExtendID5.Name = "txb_SetExtendID5";
            this.txb_SetExtendID5.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID5.TabIndex = 1023;
            this.txb_SetExtendID5.Tag = "5";
            this.txb_SetExtendID5.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting5;
            this.txb_SetExtendID5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID5.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdb_StandardFrame);
            this.panel1.Controls.Add(this.rdb_StandardAndExtendFrame);
            this.panel1.Controls.Add(this.rdb_ExtendFrame);
            this.panel1.Location = new System.Drawing.Point(19, 34);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 20);
            this.panel1.TabIndex = 2;
            // 
            // rdb_StandardFrame
            // 
            this.rdb_StandardFrame.AutoSize = true;
            this.rdb_StandardFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_StandardFrameCheckStatus;
            this.rdb_StandardFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_StandardFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_StandardFrame.Location = new System.Drawing.Point(3, 3);
            this.rdb_StandardFrame.Name = "rdb_StandardFrame";
            this.rdb_StandardFrame.Size = new System.Drawing.Size(59, 16);
            this.rdb_StandardFrame.TabIndex = 0;
            this.rdb_StandardFrame.Text = "标准帧";
            this.rdb_StandardFrame.UseVisualStyleBackColor = true;
            this.rdb_StandardFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // rdb_ExtendFrame
            // 
            this.rdb_ExtendFrame.AutoSize = true;
            this.rdb_ExtendFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_ExtendFrameCheckStatus;
            this.rdb_ExtendFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_ExtendFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_ExtendFrame.Location = new System.Drawing.Point(68, 3);
            this.rdb_ExtendFrame.Name = "rdb_ExtendFrame";
            this.rdb_ExtendFrame.Size = new System.Drawing.Size(59, 16);
            this.rdb_ExtendFrame.TabIndex = 1;
            this.rdb_ExtendFrame.Text = "扩展帧";
            this.rdb_ExtendFrame.UseVisualStyleBackColor = true;
            this.rdb_ExtendFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // txb_SetExtendID4
            // 
            this.txb_SetExtendID4.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting4", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID4.Location = new System.Drawing.Point(334, 89);
            this.txb_SetExtendID4.MaxLength = 1;
            this.txb_SetExtendID4.Multiline = true;
            this.txb_SetExtendID4.Name = "txb_SetExtendID4";
            this.txb_SetExtendID4.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID4.TabIndex = 1024;
            this.txb_SetExtendID4.Tag = "4";
            this.txb_SetExtendID4.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting4;
            this.txb_SetExtendID4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID4.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID9
            // 
            this.txb_SetStandardID9.Enabled = false;
            this.txb_SetStandardID9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID9.Location = new System.Drawing.Point(90, 71);
            this.txb_SetStandardID9.MaxLength = 1;
            this.txb_SetStandardID9.Multiline = true;
            this.txb_SetStandardID9.Name = "txb_SetStandardID9";
            this.txb_SetStandardID9.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID9.TabIndex = 476;
            this.txb_SetStandardID9.Text = "X";
            this.txb_SetStandardID9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID3
            // 
            this.txb_SetExtendID3.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID3.Location = new System.Drawing.Point(344, 89);
            this.txb_SetExtendID3.MaxLength = 1;
            this.txb_SetExtendID3.Multiline = true;
            this.txb_SetExtendID3.Name = "txb_SetExtendID3";
            this.txb_SetExtendID3.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID3.TabIndex = 1025;
            this.txb_SetExtendID3.Tag = "3";
            this.txb_SetExtendID3.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting3;
            this.txb_SetExtendID3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID3.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID8
            // 
            this.txb_SetStandardID8.Enabled = false;
            this.txb_SetStandardID8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID8.Location = new System.Drawing.Point(100, 71);
            this.txb_SetStandardID8.MaxLength = 1;
            this.txb_SetStandardID8.Multiline = true;
            this.txb_SetStandardID8.Name = "txb_SetStandardID8";
            this.txb_SetStandardID8.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID8.TabIndex = 472;
            this.txb_SetStandardID8.Text = "X";
            this.txb_SetStandardID8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID2
            // 
            this.txb_SetExtendID2.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID2.Location = new System.Drawing.Point(354, 89);
            this.txb_SetExtendID2.MaxLength = 1;
            this.txb_SetExtendID2.Multiline = true;
            this.txb_SetExtendID2.Name = "txb_SetExtendID2";
            this.txb_SetExtendID2.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID2.TabIndex = 1026;
            this.txb_SetExtendID2.Tag = "2";
            this.txb_SetExtendID2.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting2;
            this.txb_SetExtendID2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID2.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID2
            // 
            this.txb_SetStandardID2.Enabled = false;
            this.txb_SetStandardID2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID2.Location = new System.Drawing.Point(165, 71);
            this.txb_SetStandardID2.MaxLength = 1;
            this.txb_SetStandardID2.Multiline = true;
            this.txb_SetStandardID2.Name = "txb_SetStandardID2";
            this.txb_SetStandardID2.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID2.TabIndex = 473;
            this.txb_SetStandardID2.Text = "X";
            this.txb_SetStandardID2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID9
            // 
            this.txb_SetExtendID9.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting9", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID9.Location = new System.Drawing.Point(279, 89);
            this.txb_SetExtendID9.MaxLength = 1;
            this.txb_SetExtendID9.Multiline = true;
            this.txb_SetExtendID9.Name = "txb_SetExtendID9";
            this.txb_SetExtendID9.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID9.TabIndex = 1019;
            this.txb_SetExtendID9.Tag = "9";
            this.txb_SetExtendID9.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting9;
            this.txb_SetExtendID9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID9.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID3
            // 
            this.txb_SetStandardID3.Enabled = false;
            this.txb_SetStandardID3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID3.Location = new System.Drawing.Point(155, 71);
            this.txb_SetStandardID3.MaxLength = 1;
            this.txb_SetStandardID3.Multiline = true;
            this.txb_SetStandardID3.Name = "txb_SetStandardID3";
            this.txb_SetStandardID3.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID3.TabIndex = 474;
            this.txb_SetStandardID3.Text = "X";
            this.txb_SetStandardID3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID8
            // 
            this.txb_SetExtendID8.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting8", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID8.Location = new System.Drawing.Point(289, 89);
            this.txb_SetExtendID8.MaxLength = 1;
            this.txb_SetExtendID8.Multiline = true;
            this.txb_SetExtendID8.Name = "txb_SetExtendID8";
            this.txb_SetExtendID8.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID8.TabIndex = 1020;
            this.txb_SetExtendID8.Tag = "8";
            this.txb_SetExtendID8.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting8;
            this.txb_SetExtendID8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID8.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID10
            // 
            this.txb_SetStandardID10.Enabled = false;
            this.txb_SetStandardID10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID10.Location = new System.Drawing.Point(80, 71);
            this.txb_SetStandardID10.MaxLength = 1;
            this.txb_SetStandardID10.Multiline = true;
            this.txb_SetStandardID10.Name = "txb_SetStandardID10";
            this.txb_SetStandardID10.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID10.TabIndex = 475;
            this.txb_SetStandardID10.Text = "X";
            this.txb_SetStandardID10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID15
            // 
            this.txb_SetExtendID15.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting15", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID15.Location = new System.Drawing.Point(219, 89);
            this.txb_SetExtendID15.MaxLength = 1;
            this.txb_SetExtendID15.Multiline = true;
            this.txb_SetExtendID15.Name = "txb_SetExtendID15";
            this.txb_SetExtendID15.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID15.TabIndex = 1013;
            this.txb_SetExtendID15.Tag = "15";
            this.txb_SetExtendID15.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting15;
            this.txb_SetExtendID15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID15.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID15.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID4
            // 
            this.txb_SetStandardID4.Enabled = false;
            this.txb_SetStandardID4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID4.Location = new System.Drawing.Point(145, 71);
            this.txb_SetStandardID4.MaxLength = 1;
            this.txb_SetStandardID4.Multiline = true;
            this.txb_SetStandardID4.Name = "txb_SetStandardID4";
            this.txb_SetStandardID4.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID4.TabIndex = 477;
            this.txb_SetStandardID4.Text = "X";
            this.txb_SetStandardID4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID14
            // 
            this.txb_SetExtendID14.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting14", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID14.Location = new System.Drawing.Point(229, 89);
            this.txb_SetExtendID14.MaxLength = 1;
            this.txb_SetExtendID14.Multiline = true;
            this.txb_SetExtendID14.Name = "txb_SetExtendID14";
            this.txb_SetExtendID14.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID14.TabIndex = 1014;
            this.txb_SetExtendID14.Tag = "14";
            this.txb_SetExtendID14.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting14;
            this.txb_SetExtendID14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID14.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID5
            // 
            this.txb_SetStandardID5.Enabled = false;
            this.txb_SetStandardID5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID5.Location = new System.Drawing.Point(135, 71);
            this.txb_SetStandardID5.MaxLength = 1;
            this.txb_SetStandardID5.Multiline = true;
            this.txb_SetStandardID5.Name = "txb_SetStandardID5";
            this.txb_SetStandardID5.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID5.TabIndex = 478;
            this.txb_SetStandardID5.Text = "X";
            this.txb_SetStandardID5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID13
            // 
            this.txb_SetExtendID13.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting13", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID13.Location = new System.Drawing.Point(239, 89);
            this.txb_SetExtendID13.MaxLength = 1;
            this.txb_SetExtendID13.Multiline = true;
            this.txb_SetExtendID13.Name = "txb_SetExtendID13";
            this.txb_SetExtendID13.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID13.TabIndex = 1015;
            this.txb_SetExtendID13.Tag = "13";
            this.txb_SetExtendID13.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting13;
            this.txb_SetExtendID13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID13.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID6
            // 
            this.txb_SetStandardID6.Enabled = false;
            this.txb_SetStandardID6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID6.Location = new System.Drawing.Point(125, 71);
            this.txb_SetStandardID6.MaxLength = 1;
            this.txb_SetStandardID6.Multiline = true;
            this.txb_SetStandardID6.Name = "txb_SetStandardID6";
            this.txb_SetStandardID6.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID6.TabIndex = 479;
            this.txb_SetStandardID6.Text = "X";
            this.txb_SetStandardID6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID12
            // 
            this.txb_SetExtendID12.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting12", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID12.Location = new System.Drawing.Point(249, 89);
            this.txb_SetExtendID12.MaxLength = 1;
            this.txb_SetExtendID12.Multiline = true;
            this.txb_SetExtendID12.Name = "txb_SetExtendID12";
            this.txb_SetExtendID12.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID12.TabIndex = 1016;
            this.txb_SetExtendID12.Tag = "12";
            this.txb_SetExtendID12.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting12;
            this.txb_SetExtendID12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID12.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID7
            // 
            this.txb_SetStandardID7.Enabled = false;
            this.txb_SetStandardID7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID7.Location = new System.Drawing.Point(115, 71);
            this.txb_SetStandardID7.MaxLength = 1;
            this.txb_SetStandardID7.Multiline = true;
            this.txb_SetStandardID7.Name = "txb_SetStandardID7";
            this.txb_SetStandardID7.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID7.TabIndex = 480;
            this.txb_SetStandardID7.Text = "X";
            this.txb_SetStandardID7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID11
            // 
            this.txb_SetExtendID11.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting11", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID11.Location = new System.Drawing.Point(259, 89);
            this.txb_SetExtendID11.MaxLength = 1;
            this.txb_SetExtendID11.Multiline = true;
            this.txb_SetExtendID11.Name = "txb_SetExtendID11";
            this.txb_SetExtendID11.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID11.TabIndex = 1017;
            this.txb_SetExtendID11.Tag = "11";
            this.txb_SetExtendID11.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting11;
            this.txb_SetExtendID11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID11.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID0
            // 
            this.txb_SetStandardID0.Enabled = false;
            this.txb_SetStandardID0.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID0.Location = new System.Drawing.Point(185, 71);
            this.txb_SetStandardID0.MaxLength = 1;
            this.txb_SetStandardID0.Multiline = true;
            this.txb_SetStandardID0.Name = "txb_SetStandardID0";
            this.txb_SetStandardID0.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID0.TabIndex = 481;
            this.txb_SetStandardID0.Text = "X";
            this.txb_SetStandardID0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID10
            // 
            this.txb_SetExtendID10.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting10", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID10.Location = new System.Drawing.Point(269, 89);
            this.txb_SetExtendID10.MaxLength = 1;
            this.txb_SetExtendID10.Multiline = true;
            this.txb_SetExtendID10.Name = "txb_SetExtendID10";
            this.txb_SetExtendID10.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID10.TabIndex = 1018;
            this.txb_SetExtendID10.Tag = "10";
            this.txb_SetExtendID10.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting10;
            this.txb_SetExtendID10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID10.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetStandardID1
            // 
            this.txb_SetStandardID1.Enabled = false;
            this.txb_SetStandardID1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetStandardID1.Location = new System.Drawing.Point(175, 71);
            this.txb_SetStandardID1.MaxLength = 1;
            this.txb_SetStandardID1.Multiline = true;
            this.txb_SetStandardID1.Name = "txb_SetStandardID1";
            this.txb_SetStandardID1.Size = new System.Drawing.Size(12, 18);
            this.txb_SetStandardID1.TabIndex = 482;
            this.txb_SetStandardID1.Text = "X";
            this.txb_SetStandardID1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txb_SetExtendID17
            // 
            this.txb_SetExtendID17.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting17", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID17.Location = new System.Drawing.Point(195, 89);
            this.txb_SetExtendID17.MaxLength = 1;
            this.txb_SetExtendID17.Multiline = true;
            this.txb_SetExtendID17.Name = "txb_SetExtendID17";
            this.txb_SetExtendID17.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID17.TabIndex = 1011;
            this.txb_SetExtendID17.Tag = "17";
            this.txb_SetExtendID17.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting17;
            this.txb_SetExtendID17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID17.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID18
            // 
            this.txb_SetExtendID18.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting18", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID18.Location = new System.Drawing.Point(185, 89);
            this.txb_SetExtendID18.MaxLength = 1;
            this.txb_SetExtendID18.Multiline = true;
            this.txb_SetExtendID18.Name = "txb_SetExtendID18";
            this.txb_SetExtendID18.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID18.TabIndex = 1010;
            this.txb_SetExtendID18.Tag = "18";
            this.txb_SetExtendID18.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting18;
            this.txb_SetExtendID18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID18.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID18.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID16
            // 
            this.txb_SetExtendID16.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting16", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID16.Location = new System.Drawing.Point(205, 89);
            this.txb_SetExtendID16.MaxLength = 1;
            this.txb_SetExtendID16.Multiline = true;
            this.txb_SetExtendID16.Name = "txb_SetExtendID16";
            this.txb_SetExtendID16.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID16.TabIndex = 1012;
            this.txb_SetExtendID16.Tag = "16";
            this.txb_SetExtendID16.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting16;
            this.txb_SetExtendID16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID16.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID16.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID19
            // 
            this.txb_SetExtendID19.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting19", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID19.Location = new System.Drawing.Point(175, 89);
            this.txb_SetExtendID19.MaxLength = 1;
            this.txb_SetExtendID19.Multiline = true;
            this.txb_SetExtendID19.Name = "txb_SetExtendID19";
            this.txb_SetExtendID19.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID19.TabIndex = 1009;
            this.txb_SetExtendID19.Tag = "19";
            this.txb_SetExtendID19.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting19;
            this.txb_SetExtendID19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID19.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID19.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID23
            // 
            this.txb_SetExtendID23.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting23", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID23.Location = new System.Drawing.Point(135, 89);
            this.txb_SetExtendID23.MaxLength = 1;
            this.txb_SetExtendID23.Multiline = true;
            this.txb_SetExtendID23.Name = "txb_SetExtendID23";
            this.txb_SetExtendID23.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID23.TabIndex = 1005;
            this.txb_SetExtendID23.Tag = "23";
            this.txb_SetExtendID23.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting23;
            this.txb_SetExtendID23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID23.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID23.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID20
            // 
            this.txb_SetExtendID20.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting20", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID20.Location = new System.Drawing.Point(165, 89);
            this.txb_SetExtendID20.MaxLength = 1;
            this.txb_SetExtendID20.Multiline = true;
            this.txb_SetExtendID20.Name = "txb_SetExtendID20";
            this.txb_SetExtendID20.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID20.TabIndex = 1008;
            this.txb_SetExtendID20.Tag = "20";
            this.txb_SetExtendID20.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting20;
            this.txb_SetExtendID20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID20.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID20.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID22
            // 
            this.txb_SetExtendID22.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting22", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID22.Location = new System.Drawing.Point(145, 89);
            this.txb_SetExtendID22.MaxLength = 1;
            this.txb_SetExtendID22.Multiline = true;
            this.txb_SetExtendID22.Name = "txb_SetExtendID22";
            this.txb_SetExtendID22.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID22.TabIndex = 1006;
            this.txb_SetExtendID22.Tag = "22";
            this.txb_SetExtendID22.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting22;
            this.txb_SetExtendID22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID22.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // txb_SetExtendID21
            // 
            this.txb_SetExtendID21.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "txb_SetExtendIDSetting21", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_SetExtendID21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_SetExtendID21.Location = new System.Drawing.Point(155, 89);
            this.txb_SetExtendID21.MaxLength = 1;
            this.txb_SetExtendID21.Multiline = true;
            this.txb_SetExtendID21.Name = "txb_SetExtendID21";
            this.txb_SetExtendID21.Size = new System.Drawing.Size(12, 18);
            this.txb_SetExtendID21.TabIndex = 1007;
            this.txb_SetExtendID21.Tag = "21";
            this.txb_SetExtendID21.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.txb_SetExtendIDSetting21;
            this.txb_SetExtendID21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txb_SetExtendID21.TextChanged += new System.EventHandler(this.SetFilterFun);
            this.txb_SetExtendID21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_SetID_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_ExtendID);
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_DisplayExtendID);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.btn_SetTopmost);
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_StandardID);
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_FrameType);
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_FrameFormat);
            this.groupBox1.Controls.Add(this.lb_AnalysisFilter_DisplayStandardID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txb_AnalysisFilter_MASK);
            this.groupBox1.Controls.Add(this.txb_AnalysisFilter_AID);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(391, 118);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "解析滤波器：";
            // 
            // lb_AnalysisFilter_ExtendID
            // 
            this.lb_AnalysisFilter_ExtendID.AutoSize = true;
            this.lb_AnalysisFilter_ExtendID.Location = new System.Drawing.Point(104, 101);
            this.lb_AnalysisFilter_ExtendID.Name = "lb_AnalysisFilter_ExtendID";
            this.lb_AnalysisFilter_ExtendID.Size = new System.Drawing.Size(197, 12);
            this.lb_AnalysisFilter_ExtendID.TabIndex = 468;
            this.lb_AnalysisFilter_ExtendID.Text = "XXXXX XXXXXXXX XXXXXXXX XXXXXXXX";
            // 
            // lb_AnalysisFilter_DisplayExtendID
            // 
            this.lb_AnalysisFilter_DisplayExtendID.AutoSize = true;
            this.lb_AnalysisFilter_DisplayExtendID.Location = new System.Drawing.Point(44, 101);
            this.lb_AnalysisFilter_DisplayExtendID.Name = "lb_AnalysisFilter_DisplayExtendID";
            this.lb_AnalysisFilter_DisplayExtendID.Size = new System.Drawing.Size(53, 12);
            this.lb_AnalysisFilter_DisplayExtendID.TabIndex = 467;
            this.lb_AnalysisFilter_DisplayExtendID.Text = "扩展帧：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(34, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 466;
            this.label9.Text = "二进制ID号：";
            // 
            // lb_AnalysisFilter_StandardID
            // 
            this.lb_AnalysisFilter_StandardID.AutoSize = true;
            this.lb_AnalysisFilter_StandardID.Location = new System.Drawing.Point(104, 86);
            this.lb_AnalysisFilter_StandardID.Name = "lb_AnalysisFilter_StandardID";
            this.lb_AnalysisFilter_StandardID.Size = new System.Drawing.Size(77, 12);
            this.lb_AnalysisFilter_StandardID.TabIndex = 465;
            this.lb_AnalysisFilter_StandardID.Text = "XXX XXXXXXXX";
            // 
            // lb_AnalysisFilter_FrameType
            // 
            this.lb_AnalysisFilter_FrameType.AutoSize = true;
            this.lb_AnalysisFilter_FrameType.Location = new System.Drawing.Point(104, 35);
            this.lb_AnalysisFilter_FrameType.Name = "lb_AnalysisFilter_FrameType";
            this.lb_AnalysisFilter_FrameType.Size = new System.Drawing.Size(89, 12);
            this.lb_AnalysisFilter_FrameType.TabIndex = 464;
            this.lb_AnalysisFilter_FrameType.Text = "远程帧和数据帧";
            // 
            // lb_AnalysisFilter_FrameFormat
            // 
            this.lb_AnalysisFilter_FrameFormat.AutoSize = true;
            this.lb_AnalysisFilter_FrameFormat.Location = new System.Drawing.Point(104, 52);
            this.lb_AnalysisFilter_FrameFormat.Name = "lb_AnalysisFilter_FrameFormat";
            this.lb_AnalysisFilter_FrameFormat.Size = new System.Drawing.Size(89, 12);
            this.lb_AnalysisFilter_FrameFormat.TabIndex = 463;
            this.lb_AnalysisFilter_FrameFormat.Text = "标准帧和扩展帧";
            // 
            // lb_AnalysisFilter_DisplayStandardID
            // 
            this.lb_AnalysisFilter_DisplayStandardID.AutoSize = true;
            this.lb_AnalysisFilter_DisplayStandardID.Location = new System.Drawing.Point(44, 86);
            this.lb_AnalysisFilter_DisplayStandardID.Name = "lb_AnalysisFilter_DisplayStandardID";
            this.lb_AnalysisFilter_DisplayStandardID.Size = new System.Drawing.Size(53, 12);
            this.lb_AnalysisFilter_DisplayStandardID.TabIndex = 462;
            this.lb_AnalysisFilter_DisplayStandardID.Text = "标准帧：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 461;
            this.label5.Text = "帧类型:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "帧格式:";
            // 
            // txb_AnalysisFilter_MASK
            // 
            this.txb_AnalysisFilter_MASK.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "Cal_Aalysis_MASKValue", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_AnalysisFilter_MASK.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_AnalysisFilter_MASK.Location = new System.Drawing.Point(137, 14);
            this.txb_AnalysisFilter_MASK.MaxLength = 8;
            this.txb_AnalysisFilter_MASK.Multiline = true;
            this.txb_AnalysisFilter_MASK.Name = "txb_AnalysisFilter_MASK";
            this.txb_AnalysisFilter_MASK.Size = new System.Drawing.Size(56, 18);
            this.txb_AnalysisFilter_MASK.TabIndex = 460;
            this.txb_AnalysisFilter_MASK.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.Cal_Aalysis_MASKValue;
            this.txb_AnalysisFilter_MASK.TextChanged += new System.EventHandler(this.AnalysisFilterFun);
            this.txb_AnalysisFilter_MASK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_AnalysisFilter_KeyPress);
            // 
            // txb_AnalysisFilter_AID
            // 
            this.txb_AnalysisFilter_AID.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "Cal_Aalysis_AIDValue", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txb_AnalysisFilter_AID.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txb_AnalysisFilter_AID.Location = new System.Drawing.Point(46, 14);
            this.txb_AnalysisFilter_AID.MaxLength = 8;
            this.txb_AnalysisFilter_AID.Multiline = true;
            this.txb_AnalysisFilter_AID.Name = "txb_AnalysisFilter_AID";
            this.txb_AnalysisFilter_AID.Size = new System.Drawing.Size(54, 18);
            this.txb_AnalysisFilter_AID.TabIndex = 459;
            this.txb_AnalysisFilter_AID.Text = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.Cal_Aalysis_AIDValue;
            this.txb_AnalysisFilter_AID.TextChanged += new System.EventHandler(this.AnalysisFilterFun);
            this.txb_AnalysisFilter_AID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txb_AnalysisFilter_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "MASK:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "AID:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox1);
            this.panel3.Controls.Add(this.gb_SetFilter);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(400, 265);
            this.panel3.TabIndex = 2;
            // 
            // rdb_StandardAndExtendFrame
            // 
            this.rdb_StandardAndExtendFrame.AutoSize = true;
            this.rdb_StandardAndExtendFrame.Checked = global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default.rdb_StandardAndExtendFrameCheckStatus;
            this.rdb_StandardAndExtendFrame.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::SuperPowerCAN_WeChat_TC4300_QQ_43058655.Properties.Settings.Default, "rdb_StandardAndExtendFrameCheckStatus", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rdb_StandardAndExtendFrame.Location = new System.Drawing.Point(131, 3);
            this.rdb_StandardAndExtendFrame.Name = "rdb_StandardAndExtendFrame";
            this.rdb_StandardAndExtendFrame.Size = new System.Drawing.Size(107, 16);
            this.rdb_StandardAndExtendFrame.TabIndex = 2;
            this.rdb_StandardAndExtendFrame.TabStop = true;
            this.rdb_StandardAndExtendFrame.Text = "标准帧和扩展帧";
            this.rdb_StandardAndExtendFrame.UseVisualStyleBackColor = true;
            this.rdb_StandardAndExtendFrame.CheckedChanged += new System.EventHandler(this.SetFilterFun);
            // 
            // FilterCal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 266);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterCal";
            this.Text = "滤波计算器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FilterCal_FormClosing);
            this.Load += new System.EventHandler(this.FilterCal_Load);
            this.gb_SetFilter.ResumeLayout(false);
            this.gb_SetFilter.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox gb_SetFilter;
        private System.Windows.Forms.Label lb_SetFilterExtendFrame;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txb_SetFilter_MASK;
        private System.Windows.Forms.Label lb_SetFilterStardardFrame;
        private System.Windows.Forms.TextBox txb_SetFilter_AID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rdb_DataFrame;
        private System.Windows.Forms.RadioButton rdb_DataAndRemoteFrame;
        private System.Windows.Forms.RadioButton rdb_RemoteFrame;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdb_StandardFrame;
        private System.Windows.Forms.RadioButton rdb_ExtendFrame;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lb_AnalysisFilter_ExtendID;
        private System.Windows.Forms.Label lb_AnalysisFilter_DisplayExtendID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_SetTopmost;
        private System.Windows.Forms.Label lb_AnalysisFilter_StandardID;
        private System.Windows.Forms.Label lb_AnalysisFilter_FrameType;
        private System.Windows.Forms.Label lb_AnalysisFilter_FrameFormat;
        private System.Windows.Forms.Label lb_AnalysisFilter_DisplayStandardID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txb_AnalysisFilter_MASK;
        private System.Windows.Forms.TextBox txb_AnalysisFilter_AID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txb_SetStandardID8;
        private System.Windows.Forms.TextBox txb_SetStandardID2;
        private System.Windows.Forms.TextBox txb_SetStandardID3;
        private System.Windows.Forms.TextBox txb_SetStandardID10;
        private System.Windows.Forms.TextBox txb_SetStandardID9;
        private System.Windows.Forms.TextBox txb_SetStandardID4;
        private System.Windows.Forms.TextBox txb_SetStandardID5;
        private System.Windows.Forms.TextBox txb_SetStandardID6;
        private System.Windows.Forms.TextBox txb_SetStandardID7;
        private System.Windows.Forms.TextBox txb_SetStandardID1;
        private System.Windows.Forms.TextBox txb_SetStandardID0;
        private System.Windows.Forms.TextBox txb_SetExtendID17;
        private System.Windows.Forms.TextBox txb_SetExtendID16;
        private System.Windows.Forms.TextBox txb_SetExtendID23;
        private System.Windows.Forms.TextBox txb_SetExtendID22;
        private System.Windows.Forms.TextBox txb_SetExtendID21;
        private System.Windows.Forms.TextBox txb_SetExtendID20;
        private System.Windows.Forms.TextBox txb_SetExtendID19;
        private System.Windows.Forms.TextBox txb_SetExtendID18;
        private System.Windows.Forms.TextBox txb_SetExtendID9;
        private System.Windows.Forms.TextBox txb_SetExtendID8;
        private System.Windows.Forms.TextBox txb_SetExtendID15;
        private System.Windows.Forms.TextBox txb_SetExtendID14;
        private System.Windows.Forms.TextBox txb_SetExtendID13;
        private System.Windows.Forms.TextBox txb_SetExtendID12;
        private System.Windows.Forms.TextBox txb_SetExtendID11;
        private System.Windows.Forms.TextBox txb_SetExtendID10;
        private System.Windows.Forms.TextBox txb_SetExtendID1;
        private System.Windows.Forms.TextBox txb_SetExtendID0;
        private System.Windows.Forms.TextBox txb_SetExtendID7;
        private System.Windows.Forms.TextBox txb_SetExtendID6;
        private System.Windows.Forms.TextBox txb_SetExtendID5;
        private System.Windows.Forms.TextBox txb_SetExtendID4;
        private System.Windows.Forms.TextBox txb_SetExtendID3;
        private System.Windows.Forms.TextBox txb_SetExtendID2;
        private System.Windows.Forms.TextBox txb_SetExtendID25;
        private System.Windows.Forms.TextBox txb_SetExtendID24;
        private System.Windows.Forms.TextBox txb_SetExtendID28;
        private System.Windows.Forms.TextBox txb_SetExtendID27;
        private System.Windows.Forms.TextBox txb_SetExtendID26;
        private System.Windows.Forms.RadioButton rdb_StandardAndExtendFrame;
    }
}